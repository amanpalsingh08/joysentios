//
//  AppEnums.swift
//  ZGuideZ
//
//  Created by Jitendra Kumar on 15/12/17.
//  Copyright © 2017 Mobilyte. All rights reserved.
//

import Foundation
import UIKit
enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

enum JSServiceType : Int{
    case normal = 0
    case searching = 2
    case pagging = 3
}

enum HTTPStatusCodes: Int {
    // 100 Informational
    case Continue = 100
    case SwitchingProtocols, Processing
    // 200 Success
    case OK = 200
    case Created, Accepted, NonAuthoritativeInformation, NoContent, ResetContent, PartialContent, MultiStatus, AlreadyReported
    case IMUsed = 226
    
    // 300 Redirection
    case MultipleChoices = 300
    case MovedPermanently, Found, SeeOther, NotModified, UseProxy,SwitchProxy, TemporaryRedirect, PermanentRedirect
    
    // 400 Client Error
    case BadRequest = 400
    case Unauthorized = 401
    case PaymentRequired = 402
    case Forbidden  = 403
    case NotFound = 404
    case MethodNotAllowed = 405
    case NotAcceptable, ProxyAuthenticationRequired,RequestTimeout, Conflict, Gone, LengthRequired, PreconditionFailed, PayloadTooLarge, URITooLong, UnsupportedMediaType, RangeNotSatisfiable, ExpectationFailed, ImATeapot
    case MisdirectedRequest = 421
    case UnprocessableEntity, Locked, FailedDependency
    case UpgradeRequired = 426
    case PreconditionRequired = 428
    case TooManyRequests
    case RequestHeaderFieldsTooLarge = 431
    case UnavailableForLegalReasons = 451
    // 500 Server Error
    case InternalServerError = 500
    case NotImplemented, BadGateway, ServiceUnavailable, GatewayTimeout, HTTPVersionNotSupported, VariantAlsoNegotiates, InsufficientStorage, LoopDetected
    case NotExtended = 510
    case NetworkAuthenticationRequired
    
    var description:String{
        switch self {
        case .BadRequest: return "BadRequest"
        case .Unauthorized:return "Unauthorized"
        case .PaymentRequired: return "Payment Required"
        case .Forbidden:return "Forbidden Request"
        case .NotFound:return "Request NotFound"
        case .MethodNotAllowed:return "HTTP Method Not Allowed"
        case .NotAcceptable:return "Request Not Acceptable"
        case .ProxyAuthenticationRequired:return "Proxy Authentication Required"
        case .RequestTimeout: return "Request Timeout"
        case .Conflict:return "Conflict"
        default:
            return kAppTitle
        }
    }
}
enum AppStoryboard:String {
    case Main
    case Home
    case Tabbar
    case Card
    case Gallery
    case Account
    case MyClient
    case Request
    case ClientDetail
    case Feedback
    case GiftStore
    
    var instance:UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    func viewController<T:UIViewController>(viewController classObject :T.Type)->T{
        let storyboardId = classObject.storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardId) as! T
    }
    func viewController<T:UIViewController>(withIdentifier identifier :String)->T{
        return instance.instantiateViewController(withIdentifier: identifier) as! T
    }
    func initalViewController<T:UIViewController>()->T?{
        return instance.instantiateInitialViewController() as? T
    }
}
enum AppPermission {
    case network
    case notification
    
    
    func show(){
        var title:String = kAppTitle
        var message:String?
        switch self {
        case .notification:
            title = "\"\(kAppTitle)\" Notification disabled"
            message = "This app unable access to push notification. Please check app setting and enable the push notification, tap \"Settings\" and turn on \"Allow Notifications.\""
            
        case .network:
            title = "\"\(kAppTitle)\" \(kConnectionError)"
            message = "The Internet connection appears to be offline.\(EmojiFont.sadEmoji)"
        }
        AppSettingAlert(title: title, message: message)
    }
    
}


