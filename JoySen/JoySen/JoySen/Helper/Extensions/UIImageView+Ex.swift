//
//  UIImageView+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 20/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit
import Kingfisher


//MARK: - UIImageView Extension

extension UIImageView{
    
    fileprivate func showLoader(isShow:Bool){
        if isShow {
            let loader = JKIndicatorView()
            loader.lineColor = JKColor.DarkBlue
            loader.lineWidth = 3
            loader.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(loader)
            loader.centerInSuperview(size: .init(width: 25, height: 25))
            loader.startAnimation()
        }else{
            guard let loader = self.subviews.first(where: {$0.isKind(of: JKIndicatorView.self)}) as? JKIndicatorView else{return}
            async {
                loader.stopAnimation()
                loader.removeFromSuperview()
            }
        }
    }
   public func cancelDownloadTask(){
        self.kf.cancelDownloadTask()
    }

    public func reloadImageSize(image:UIImage){
        let imageSize = image.size
        let aspect = imageSize.width/imageSize.height
        let size =  CGSize(width: self.frame.size.width, height: self.frame.size.width/aspect)
        self.frame.size.height = size.height
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
    }
    public func loadImage(filePath:String,placeholder: Placeholder? = #imageLiteral(resourceName: "ic_app_logo"),options:KingfisherOptionsInfo = [.transition(.fade(1))], onCompletion: ((Swift.Result<KFCrossPlatformImage,Error>)->Swift.Void)? = nil){
        guard NetworkState.state.isConnected else{
        
        self.cancelDownloadTask()
        self.showLoader(isShow: false)
        return
    }
    self.showLoader(isShow: true)
     
       
    
    self.kf.setImage(with: URL(string: filePath)!, placeholder: placeholder, options: options, completionHandler: { (result) in
        switch result{
        case .success(let value):
            self.showLoader(isShow: false)
            onCompletion?(.success(value.image))
        case .failure(let error):
            self.showLoader(isShow: false)
            onCompletion?(.failure(error))
            
            
            
        }
    })
     
 }
    
}
