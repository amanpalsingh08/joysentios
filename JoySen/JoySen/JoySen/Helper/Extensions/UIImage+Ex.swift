//
//  UIImage+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 20/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit
import Kingfisher
extension UIImage {
  
    #if swift(>=4.2)
    var png:                  Data      { return self.pngData()!        }
    var highestJPEG:          Data      { return self.jpegData(compressionQuality: 1.0)!  }
    var highJPEG:             Data      { return self.jpegData(compressionQuality: 0.75)! }
    var mediumJPEG:           Data      { return self.jpegData(compressionQuality: 0.5)!  }
    var lowQualityJPEG:       Data      { return self.jpegData(compressionQuality: 0.25)! }
    var lowestQualityJPEG:    Data      { return self.jpegData(compressionQuality: 0.0)!  }
    #else
    var png:                  Data      { return UIImagePNGRepresentation(self)!          }
    var highestJPEG:          Data      { return UIImageJPEGRepresentation(self,1.0)!     }
    var highJPEG:             Data      { return UIImageJPEGRepresentation(self,0.75)!    }
    var mediumJPEG:           Data      { return UIImageJPEGRepresentation(self,0.5)!     }
    var lowQualityJPEG:       Data      { return UIImageJPEGRepresentation(self,0.25)!    }
    var lowestQualityJPEG:    Data      { return UIImageJPEGRepresentation(self, 0.25 )!  }
    #endif
    
    
    public func scaleImage(to size: CGSize) -> KFCrossPlatformImage {
        guard let cgImage    = self.cgImage else {return self}
        let wRatio           = size.width/CGFloat(cgImage.width)
        let hRatio           = size.height/CGFloat(cgImage.height)
        let width            = size.width
        var height           = size.width
        if wRatio < hRatio { height =  CGFloat(cgImage.height) * CGFloat(wRatio)}
       return self.resize(to: .init(width: width, height: height))
       
    }
    
    
    // MARK: Round Corner
    /// Creates a round corner image from on `UIImage` image.
    ///
    /// - Parameters:
    ///   - radius: The round corner radius of creating image.
    ///   - size: The target size of creating image.
    ///   - corners: The target corners which will be applied rounding.
    ///   - backgroundColor: The background color for the output image
    /// - Returns: An image with round corner of `self`.
    ///
    /// - Note: This method only works for CG-based image. The current image scale is kept.
    ///         For any non-CG-based image, `UIImage` itself is returned.
    public func image(withRoundRadius radius: CGFloat,
                      fit size: CGSize,
                      roundingCorners corners: RectCorner = .all,
                      backgroundColor: KFCrossPlatformColor? = nil) -> KFCrossPlatformImage
    {
        guard let _ = cgImage else {
            assertionFailure("Round corner image only works for CG-based image.")
            return self
        }
         let wrapp = KingfisherWrapper(self)
       return wrapp.image(withRoundRadius: radius, fit: size, roundingCorners: corners, backgroundColor: backgroundColor)
       
     
    }
    
    
    // MARK: Resizing
    /// Resizes `UIImage` image to an image with new size.
    ///
    /// - Parameter size: The target size in point.
    /// - Returns: An image with new size.
    /// - Note: This method only works for CG-based image. The current image scale is kept.
    ///         For any non-CG-based image, `UIImage` itself is returned.
    public func resize(to size: CGSize) -> KFCrossPlatformImage {
        let wrapp = KingfisherWrapper(self)
        return wrapp.resize(to: size)
    }
   
    
    // MARK: Tint
    /// Creates an image from `UIImage` image with a color tint.
    ///
    /// - Parameter color: The color should be used to tint `UIImage`
    /// - Returns: An image with a color tint applied.
    public func tinted(with color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer { UIGraphicsEndImageContext() }
        color.set()
        withRenderingMode(.alwaysTemplate).draw(in: CGRect(origin: .zero, size: size))
        return UIGraphicsGetImageFromCurrentImageContext() ?? self
        
        
    }

    
}
