//
//  String+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 22/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit
struct Validator{
    enum Validate {
        case regularExp(SRegularExpression)
        case predicate(SPredicate)
        
        func formatter(in string: String)->Bool{
            switch self {
            case .regularExp(let field):
                return field.firstMatch(in: string)
            case .predicate(let field):
                return field.evaluate(with: string)
            }
        }
    }
    enum SRegularExpression {
        case alphaNumeric
        case alphaNumericWithSpace
        case email
        case specialChar
        case address
        case onlyNumbers
        case onlyNumbersWithPlus
        case onlyAlphabet
        var regularExp:String{
            switch self {
                
            case .alphaNumeric: return "[^a-zA-Z0-9 ]"
            case .alphaNumericWithSpace: return "[^A-Z0-9a-z ]"
            case .email:
                return"^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$"
            case .specialChar: return ".*[^A-Za-z0-9 ].*"
            case .address: return ".*[^A-Za-z0-9._@#/()-+*., ].*"
            case .onlyNumbers: return ".*[^A-Za-z ].*"
            case .onlyNumbersWithPlus: return ".*[^0-9+].*"
            case .onlyAlphabet: return ".*[^A-Za-z ].*"
                
            }
        }
        var expression:NSRegularExpression? {
            switch self {
            case .email:
                return try? NSRegularExpression(pattern: self.regularExp, options: .caseInsensitive)
            default :
                return try? NSRegularExpression(pattern: self.regularExp, options: [])
            }
        }
        
        func firstMatch(in string: String, options: NSRegularExpression.MatchingOptions = []) -> Bool{
            guard let expression = expression, !string.isEmpty else { return false}
            return expression.firstMatch(in: string, options: options, range: NSMakeRange(0, string.count)) == nil ? false : true
        }
    }
    enum SPredicate {
        case password
        case phoneNumber
        case validateUrl
        var regularExp:String{
            switch self {
            case .password: return "^(?=.*[A-Z])(?=.*[0-9])(?=.*[@$#]).{8,}$"
            case .phoneNumber: return "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"//"^\\d{3}-\\d{3}-\\d{4}$"
            case .validateUrl: return "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
         
            }
        }
        var predicate:NSPredicate{
            return NSPredicate(format: "SELF MATCHES %@", self.regularExp)
        }
        func evaluate(with object: Any?) -> Bool{
            return predicate.evaluate(with: object)
        }
        
        
    }
    
}
extension String{
    
    
    var binaryValue:    Data?           { return self.data(using: .utf8)     }
    var length:         Int             { return self.count                  }
    //MARK:- pairs
    var pairs: [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: count, by: 2).forEach {
            result.append(String(characters[$0..<min($0+2, count)]))
        }
        return result
    }
    
    //MARK:- String Validation
    
    //MARK:- validFormatter
    func validFormatter(_ formate:Validator.Validate)->Bool{
        guard !self.isEmpty else {return false}
        return formate.formatter(in: self)
    }
    //MARK:- isAlphanumericWithSpace
    var isAlphanumericWithSpace: Bool {
        return validFormatter(.regularExp(.alphaNumericWithSpace))
    }
    //MARK:- isAlphanumeric
    var isAlphanumeric: Bool {
        return validFormatter(.regularExp(.alphaNumeric))
    }
    //MARK:- isEmail
    var isEmail: Bool {
        return validFormatter(.regularExp(.email))
    }
    //MARK:- isSpecialChar
    var isSpecialChar: Bool {
        return validFormatter(.regularExp(.specialChar))
    }
    //MARK:- isAddress
    var isAddress: Bool {
        return validFormatter(.regularExp(.address))
    }
    
    //MARK:- onlyNumbers
    var isOnlyNumbers : Bool {
        return validFormatter(.regularExp(.onlyNumbers))
    }
    //MARK:- onlyNumbersWithPlus
    var isOnlyNumbersWithPlus: Bool {
        return validFormatter(.regularExp(.onlyNumbersWithPlus))
    }
    //MARK:- onlyAlphabet
    var isOnlyAlphabet:  Bool{
        return validFormatter(.regularExp(.onlyAlphabet))
    }
    //MARK:- isValidPassword
    var isPassword: Bool{
        return validFormatter(.predicate(.password))
    }
    
    //MARK:- isPhoneNumber
    var isPhoneNumber:Bool{
        return validFormatter(.predicate(.phoneNumber))
    }
    
    //MARK:- isValidateUrl
    var isValidateUrl : Bool {
        return validFormatter(.predicate(.validateUrl))
    }
    
    //MARK:- removeSpace
    var removeSpace:String{
        return self.components(separatedBy: .whitespaces).joined(separator: "")
    }
    
    //MARK:- Insert subString At Index
    func insert(_ string:String,ind:Int) -> String {
        return  String(self.prefix(ind)) + string + String(self.suffix(self.count-ind))
    }
    
    //MARK:- containsAlphabets
    var containsAlphabets: Bool {
        //Checks if all the characters inside the string are alphabets
        let set = CharacterSet.letters
        return self.utf16.contains( where: { return set.contains(UnicodeScalar($0)!)  } )
    }
    //MARK:- urlQueryEncoding
    /// Returns a new string made from the `String` by replacing all characters not in the unreserved
    /// character set (As defined by RFC3986) with percent encoded characters.
    
    var urlQueryEncoding: String? {
        let allowedCharacters = NSCharacterSet.urlQueryAllowed
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
        
    }
    //MARK:- safelyLimitedTo
    func safelyLimitedTo(length n: Int)->String {
        let c = String(self)
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
    //MARK:- height
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    //MARK:- width
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    //MARK:- substring
    func substring(to : Int) -> String? {
        if (to >= length) {
            return nil
        }
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[..<toIndex])
        
    }
    
    func substring(from : Int) -> String? {
        if (from >= length) {
            return nil
        }
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        return  String(self[Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))])
        
    }
    //MARK:- GetCharacter At Index
    func character(at offsetBy: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: offsetBy)]
    }
    
    
    //MARK:- subscript
    subscript(range: ClosedRange<Int>) -> String {
        let lowerIndex = index(startIndex, offsetBy: max(0,range.lowerBound), limitedBy: endIndex) ?? endIndex
        return  String(self[lowerIndex..<(index(lowerIndex, offsetBy: range.upperBound - range.lowerBound + 1, limitedBy: endIndex) ?? endIndex)])
        
    }
    subscript(at offsetBy: Int) -> String {
        guard offsetBy >= 0 && offsetBy < self.count else { return "" }
        return String(self[index(startIndex, offsetBy: offsetBy)])
    }
    subscript(r: Range<Int>) -> String? {
        get {
            let stringCount = self.count as Int
            guard stringCount < r.upperBound || stringCount < r.lowerBound else{return nil}
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound - r.lowerBound)
            return String(self[(startIndex ..< endIndex)])
        }
    }
    //MARK:- originalUrl
    var originalUrl:String?{
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    //MARK:- isEqualTo
    func isEqualTo(other:String)->Bool{
        return self.caseInsensitiveCompare(other) == .orderedSame ? true : false
    }
    //MARK:- MakeACall
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter { CharacterSet.decimalDigits.contains($0) }
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    func makeACall(){
        async {
         guard let controller  =  currentAlert else {return}
            if let alertController = controller as? UIAlertController{
                let messageFont  =  UIFont.systemFont(ofSize: 17)
                alertController.set(message: alertMessage, font: messageFont, color: .white)
            }else{
                controller.showAlertAction( message: "Do you want to \(("Call " + self + " number?"))", otherTitle: "Call") { (index) in
                    if index == 2{
                        guard self.isPhoneNumber, let url  = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) else {
                                 alertMessage = "Cannot place call"
                                 return
                                 
                             }
                             UIApplication.shared.open(url)
                    }
                }
            }
            
        }
       
        
    }
   
}

//MARK:- EXTENSION FOR NSAttributedString
extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.height
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        return boundingBox.width
    }
    
    convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true) throws {
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        let data = html.data(using: .utf8, allowLossyConversion: true)
        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            return
        }
        
        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        let range = NSRange(location: 0, length: attr.length)
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            if let htmlFont = attrib as? UIFont {
                let traits = htmlFont.fontDescriptor.symbolicTraits
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
                
                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitBold)!
                }
                
                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitItalic)!
                }
                
                attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
            }
        }
        
        self.init(attributedString: attr)
    }
}
extension UILabel{
    
    
}
