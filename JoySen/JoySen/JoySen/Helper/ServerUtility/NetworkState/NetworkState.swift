//
//  NetworkState.swift
//  WorldChatters
//
//  Created by Jitendra Kumar on 20/05/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import Alamofire
class NetworkState {
  
    class var state:NetworkState{
        struct  Singlton{
            static let instance = NetworkState()
        }
        return Singlton.instance
    }
    var isConnected:Bool {
        let isReachable = NetworkReachabilityManager()!.isReachable
        if !isReachable {
            DispatchQueue.main.async {
                SMUtility.shared.hideHud()
                AppPermission.network.show()
                
            }
        }
        return isReachable
    }
    
  
}
