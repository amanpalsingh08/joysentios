//
//  UIImageCropPickerKit.swift
//  B2BApp
//
//  Created by Jitendra Kumar on 29/03/18.
//  Copyright © 2018 Mobilyte. All rights reserved.
//

import UIKit
import TOCropViewController
class UIImageCropPickerKit :NSObject{
    
    
    class var shared:UIImageCropPickerKit{
        
        struct Singlton{
            
            static let instance = UIImageCropPickerKit()
        }
        return Singlton.instance
    }
    
    typealias UIImageCropPickerFinalizationBlock = (_ cropedimage: UIImage?,_ Cancelled:Bool)->Void
    
    fileprivate var CropPickerHanlder:UIImageCropPickerFinalizationBlock!
    fileprivate var source:UIView!
    fileprivate var controller :UIViewController!
    func imageCropPicker(from ViewController :UIViewController,source:UIView,croppingStyle:TOCropViewCroppingStyle,image:UIImage,OnCompletion:@escaping UIImageCropPickerFinalizationBlock){
        
        self.controller = ViewController
        self.source = source
        self.CropPickerHanlder = OnCompletion
        let cropController = TOCropViewController(croppingStyle: croppingStyle, image:  image)
        cropController.delegate = self
        cropController.aspectRatioPreset = .presetSquare
        cropController.aspectRatioLockEnabled = true
        cropController.resetAspectRatioEnabled = false
        cropController.aspectRatioPickerButtonHidden = true
        cropController.toolbarPosition = .top
        
        ViewController.present(cropController, animated: true, completion: nil)
    }
}
extension UIImageCropPickerKit:TOCropViewControllerDelegate{
    
    
    
    //MARK:-TOCropViewController Delegate-
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        let cropedimage:UIImage = image
        // let viewFrame: CGRect = controller.view.convert(source.frame, to: source) //CGRectZero
        
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(cropedimage,false)
            }
            
        }) {
            
        }
        
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int)
    {
        let cropedimage:UIImage = image
        //  let viewFrame: CGRect = controller.view.convert(source.frame, to: source) //CGRectZero
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(cropedimage,false)
            }
        }) {
            
        }
        
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        cropViewController.dismissAnimatedFrom(controller, toView: source, toFrame: source.frame, setup: {
            
            if (self.CropPickerHanlder != nil){
                
                self.CropPickerHanlder(nil,true)
            }
            
        }) {
            
        }
        //        cropViewController .dismiss(animated: true, completion: { () -> Void in
        //
        //        })
    }
    
}

