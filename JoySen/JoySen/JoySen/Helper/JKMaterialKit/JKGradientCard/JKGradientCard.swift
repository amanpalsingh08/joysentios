//
//  JKGradientCard.swift
//  JKMaterialKit
//
//  Created by Jitendra Kumar on 24/06/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit
@IBDesignable
class JKGradientCard:JKCardView{
    
    private var gradientLayer: CAGradientLayer!
    
    @IBInspectable var topColor: UIColor = .red {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = .yellow {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var startPoint: CGPoint = CGPoint(x: 0.0, y: 0.5) {
        didSet {
            setNeedsLayout()
        }
    }
        
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 1.0, y: 0.5) {
        didSet {
            setNeedsLayout()
        }
    }
    
    
    
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        self.gradientLayer = self.layer as? CAGradientLayer
        self.gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        self.gradientLayer.startPoint = startPoint
        self.gradientLayer.endPoint = endPoint
        super.layoutSubviews()
        animate(duration: 0.3, newTopColor: topColor, newBottomColor: bottomColor)

    }
    
    func animate(duration: TimeInterval, newTopColor: UIColor, newBottomColor: UIColor) {
        let fromColors = self.gradientLayer?.colors
        let toColors: [AnyObject] = [ newTopColor.cgColor, newBottomColor.cgColor]
        self.gradientLayer?.colors = toColors
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = fromColors
        animation.toValue = toColors
        animation.duration = duration
        animation.isRemovedOnCompletion = true
        animation.fillMode = .forwards
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        self.gradientLayer?.add(animation, forKey:"animateGradient")
    }
}
