//
//  AppStruct.swift
//  ZGuideZ
//
//  Created by Jitendra Kumar on 06/12/17.
//  Copyright © 2017 Mobilyte. All rights reserved.
//

import UIKit
import Foundation

struct EmojiFont {
    static let rightArraow = "→"
    static let leftArrow = "←"
    static  let bulletIcon = "•"
    static  let radioselect = "◉"
    static  let rediounSelect = "◎"
    static let downTrangle = "▾"
    static let rightTrangle = "‣"
    static let leftTrangle = "◀︎"
    static let sadEmoji = "☹"
    static let crossEmoji = "×"
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
    static var isPhone:Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true :false
    }
}




struct SegueIdentity {
    
    
    static let kLoginSegue                         = "LoginSegue"
    static let kSignUpSegue                        = "SignUpSegue"
    static let kForgotPasswordSegue                = "ForgotPasswordSegue"
    static let kTabBarSegue                        = "TabBarSegue"
    static let bkProfileDetailSegue                = "ProfileDetailSegue"
    static let kChangePasswordSegue                = "ChangePasswordSegue"
    static let kMyClientInfoSegue                  = "MyClientInfoSegue"
    static let kUpdateMyClientSegue                = "UpdateMyClientSegue"
    static let kAddMyClientSegue                   = "AddMyClientSegue"
    static let kRequestConnectionsSegue            = "RequestConnectionsSegue"
    static let kAddFeedbackSegue                   = "JSAddFeedbackSegue"
    static let kUpdateFeedbackSegue                = "JSUpdateFeedbackSegue"
    static let kClientNurtureGiftSubscriptionSegue = "ClientNurtureGiftSubscriptionSegue"
    static let kSearchConnectionSegue              = "JSSearchConnectionSegue"
    static let kGiftsSegue                         = "JSGiftsSegue"
    static let kGiftDetailSegue                    = "JSGiftDetailSegue"
    //Picker
    static let kStatePickerSegue                    = "JSStatePickerSegue"
    static let kConnection1PickerSegue              = "JSConnection1PickerSegue"
    static let kConnection2PickerSegue              = "JSConnection2PickerSegue"
    static let kPhoneTypePickerSegue                = "JSPhoneTypePickerSegue"
    static let kPhoneType2PickerSegue               = "JSPhoneType2PickerSegue"
    static let kShippingStatePickerSegue            = "JSShippingStatePickerSegue"
    static let kFrequencyPickerSegue                = "JSFrequencyPickerSegue"
    static let kGroupPickerSegue                    = "JSGroupPickerSegue"
    static let kLeadSourcePickerSegue               = "JSLeadSourcePickerSegue"
    static let kStagePickerSegue                    = "JSStagePickerSegue"
    static let kStatusPickerSegue                   = "JSStatusPickerSegue"
    static let kCardMonthsPickerSegue               = "JSCardMonthsPickerSegue"
    static let kCardYearsPickerSegue                = "JSCardYearsPickerSegue"
    static let kClientPickerSegue                   = "JSClientPickerSegue"
    static let kNumOfGitsPickerSegue                = "JSNumOfGitsPickerSegue"
    static let kSelfContributionPickerSegue         = "JSSelfContributionPickerSegue"
    static let kConnection1ContributionPickerSegue  = "JSConnection1ContributionPickerSegue"
    static let kGiftScheduleDatePickerSegue         = "JSGiftScheduleDatePickerSegue"
    static let kUserRoleSegue                       = "JSUserRoleSegue"
    static let kGiftNumberPickerSegue               = "JSGiftNumberPickerSegue"
    
   
}
struct StoryBoardIdentity {
    static let KLandingVC                           = "LandingVC"
    static let kForgotVC                            = "ForgotVC"
    static let kMyAccountVC                         = "JSMyAccountVC"
    static let KLandingNavigationVC                 = "JSLandingNavigationVC"
    static let kMyAccountNavigationVC               = "JSMyAccountNavigationVC"
    static let kSideMenuNavigationVC                = "JSRightSlideNavigationC"
    static let kGalleryNavigationVC                 = "JSGalleryNavigationVC"
    static let kCardDetailsNavigationVC             = "JSCardDetailsNavigationVC"
    static let kMyClientsNavigationVC               = "JSMyClientsNavigationVC"
    static let kChangePasswordNavigationVC          = "JSChangePasswordNavigationVC"
    static let kRequestNavigationVC                 = "JSRequestNavigationVC"
    static let kQueueClientsNavigationVC            = "JSQueueClientsNavigationVC"
    static let kFeedbackNavigationVC                = "JSFeedbackNavigationVC"
    static let kRecentDisabledClientsNavigationVC   = "JSRecentDisabledClientsNavigationVC"
    static let kRecentClientConnectionsNavigationVC = "JSRecentClientConnectionsNavigationVC"
    static let kClientConnectionsNavigationVC       = "JSClientConnectionsNavigationVC"
    static let kClientLastDeliveredNavigationVC     = "JSClientLastDeliveredNavigationVC"
    static let kImportClientsNavigationVC           = "JSImportClientsNavigationVC"
    static let kAddMyClientNavigationVC             = "JSAddMyClientNavigationVC"
    static let kGiftCategoryNavigationVC            = "JSGiftCategoryNavigationVC"
}


struct TBCellIdentity {
    
    static let kLoadMoreCell            = "LoadMoreCell"
    static let kRightMenuCell           = "JSRightMenuCell"
    static let kStateCell               = "JSStateCell"
    static let kMyClientCell            = "JSMyClientCell"
    static let kReceivedRequestCell     = "JSReceivedRequestCell"
    static let kSendRequestCell         = "JSSendRequestCell"
    static let kQueueClientCell         = "JSQueueClientCell"
    static let kFeedbackCell            = "JSFeedbackCell"
    static let kGalleryCell             = "JSGalleryCell"
    static let kResentClientConnectionCell = "JSResentClientConnectionCell"
    static let kClientConnectionCell    = "JSClientConnectionCell"
    static let kSearchConnectionCell    = "JSSearchConnectionCell"
    static let kClientLastDeliveredCell = "JSClientLastDeliveredCell"
    static let kGiftStoreCategoryCell   = "JSGiftStoreCategoryCell"
    static let kAnniversaryGiftCell      = "JSGiftCell"
}


struct FieldValidation {
    
    static let kFistNameEmpty        = "Please enter first name."
    static let kLastNameEmpty        = "Please enter last name."
    
    static let kEmailEmpty           = "Please enter email id."
    static let kValidEmail           = "Please enter the valid email."
    static let kcountryCode          = "Please select country Code."
    static let kPasswordEmpty        = "Please enter password."
    static let kOldPasswordEmpty     = "Please enter old password."
    static let kTempPasswordEmpty    = "Please enter temporary password."
    static let kAlreadyUsedPassword  = "You have already used this password, please try another password."
    static let kCurrentPasswordEmpty = "Please enter current password."
    static let kPassMinLimit         = "Your password should be minimum of 6 character."
    static let kConfirmPassEmpty     = "Please enter confirm password."
    static let kPassMissMatch        = "Password doesn't match."
    static let kAuthSessionExpire    = "Your login session got expired, Please try login again."
    static let kOTPCodeEmpty         = "Please enter verification code."
    static let kValidOTPCode         = "Verification code should contain only numbers."
    static let kSingUpSuccess        =  "\(kAppTitle) user account created successfully, Please check your registered email, verification code has been sent to the email. "
    static let kUserNotExist         = "User does not exsit. Please create new account."
    static let kAuthorizationcodeEmpty = "Please enter the authentication code you received by E-mail"
    static let kResetPasssword       = "Your password has been reset."
    static let kPhoneNumberLimit     = "Your phone number should be minimum of 10 digits."
    static let kCityEmpty            = "Please enter city name."
    static let kUserTypeEmpty        = "Please select userType."
    
}


struct CVCellIdentity {
    static let kAvailableTourGridCell = "AvailableTourGridCell"
    static let kTourListTableCell = "TourListTableCell"
    static let kGuideTableCell = "GuideTableCell"
    static let kPlaceAudioTableCell = "PlaceAudioTableCell"
    static let kfloatTableCell = "floatTableCell"
    static let kLoadMoreGridCell = "LoadMoreGridCell"
    static let kLoadMoreTableCell = "LoadMoreTableCell"
}



