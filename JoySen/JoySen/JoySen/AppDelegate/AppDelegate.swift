//
//  AppDelegate.swift
//  JoySen
//
//  Created by NAVINDER SINGH on 01/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
   var window: UIWindow?
   var tabBarController: JSTabBarController?
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       // Override point for customization after application launch.
    



       return true
   }

   // MARK: UISceneSession Lifecycle

   func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
       // Called when a new scene session is being created.
       // Use this method to select a configuration to create the new scene with.
    
 
       return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
   }

   func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
       // Called when the user discards a scene session.
       // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
       // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
   }

    
    


}



extension AppDelegate{

class var shared:AppDelegate{
    
    return UIApplication.shared.delegate as! AppDelegate
}
    func setNavigationBar(_ barTintColor:UIColor? = nil, tintColor:UIColor? ,isTransparent:Bool = true){
           let appearance  =  UINavigationBar.appearance()
        appearance.configBar(barTintColor, tintColor: tintColor, isTransparent: isTransparent)
           //self.showMainController()
           
       }
    func logoutUser(){
        //guard let user = userModel else{return}
        UserDefaults.removeObject(forKey: kAuthTokenKey)
        UserDefaults.removeObject(forKey: kUserDataKey)
        UserDefaults.removeObject(forKey: kUserIDKey)
        self.setupLandingScreen()
        
    }

    func setupLandingScreen(){
         UIApplication.shared.windows.first?.rootViewController = UINavigationController.instance(from: .Main, withIdentifier: StoryBoardIdentity.KLandingNavigationVC)
        
    }


     func setTabBarController(){
       // self.setNavigationBar()
        tabBarController = JSTabBarController.instance(from: .Tabbar)
        
         let rightSlideMenu = UINavigationController.instance(from: .Home, withIdentifier: StoryBoardIdentity.kSideMenuNavigationVC)
         //tabBarController!.selectedIndex = 2
        
         let slideMenuController = JKSlideMenuController(mainViewController: tabBarController!, leftMenuViewController: rightSlideMenu, enableLeftTapGeture: true)
          UIApplication.shared.windows.first?.rootViewController  = slideMenuController
         
         
     }
     
    
    func showMainController(){
        if (isLogin) {
            setTabBarController()
        }else{
            async {
                UserDefaults.removeObject(forKey: kAuthTokenKey)
                UserDefaults.removeObject(forKey: kUserDataKey)
                 UserDefaults.removeObject(forKey: kUserIDKey)
                self.setupLandingScreen()
            }
        }
    }
    var keyWindow:UIWindow?{
        return UIApplication.shared.keyWindow
    }
}
