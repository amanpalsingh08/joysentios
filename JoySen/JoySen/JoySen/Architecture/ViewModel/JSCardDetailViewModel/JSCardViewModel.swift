//
//  JSCardViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
final class JSCardViewModel{
    static let shared = JSCardViewModel()
    var objCard:JSCard?
    
    func getCard(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
                   SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
                   async {
                       SMUtility.shared.hideHud()
                       switch result{
                       case .success(let data, let code):
                           let response  = data.getValue(JSReponse<JSCard>.self)
                           if code == .OK{
                               guard let data = response?.data else { return  }
                            self.objCard =  data
                               onCompletion()
                           }else{
                               guard let message = response?.message else { return  }
                               alertMessage =  message
                           }
                       case .failure(let err):
                           alertMessage = err.localizedDescription
                       }
                   }
               }.request(JSEndpoint.Auth.Get.getCard(userId: userID).api)
    }
    func addEditCard(cardNumber:String, expiryMonth:String,expiryYear:String, cvc:String, onCompletion:@escaping()->Void){
        if cardNumber.isEmpty {
            alertMessage = "Please enter card number."
        }else if expiryMonth.isEmpty{
           alertMessage = "Please enter card expiry month."
        }else if expiryYear.isEmpty{
             alertMessage = "Please enter card expiry year."
        }else if cvc.isEmpty{
             alertMessage = "Please enter card cvc number."
        }else{
            guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
            SMUtility.shared.showHud()
            let params:[String:Any] = ["user_id":userID,"card_number":cardNumber,"expiry_month":expiryMonth,"expiry_year":expiryYear,"cvc":cvc]
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, let code):
                        let response  = data.getValue(JSReponse<JSMyClient>.self)
                        if code == .OK{
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onCompletion()
                        }else{
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.Auth.Post.addEditCard.api,params: params)
        }
        
    }
}
extension JSCardViewModel{
    var cardNumber:String?{
        return objCard?.cardNum
    }
    var expYear:String?{
        return objCard?.expYear
    }
    var expMonth:String?{
        return objCard?.expMonth
    }
}
