//
//  JSUserViewModel.swift
//  ZGuideZ
//
//  Created by Mandeep Kaur on 22/11/18.
//  Copyright © 2018 Mandeep Kaur. All rights reserved.
//

import UIKit


class JSUserViewModel: NSObject {
    
    static let shared = JSUserViewModel()
    // MARK: - Login
    func login(emailTF:JKTextField, passwordTF:JKTextField,onSuccess:@escaping()->Void){
        
        guard NetworkState.state.isConnected,let email = emailTF.text,let password = passwordTF.text  else{return}
        if email.isEmpty {
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kValidEmail
        }else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.length<6{
            alertMessage = FieldValidation.kPassMinLimit
        }else{
            SMUtility.shared.showHud()
            let params:[String:Any] = ["email":email,"password":password, "clientType":kClientType]
            Server.Request.dataTask(method: .post, completionHandler: { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSUserModel>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            guard let model = response?.data, let token  = model.accessToken else { return }
                            userModel = model
                            accessToken = token
                            
                            onSuccess()
                            
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            break
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                        
                    }
                }
                
            }).request(JSEndpoint.Auth.Post.login.api, params: params)
            
        }
        
    }
    
    // MARK: - Register
    func register(firstNameTF:JKTextField, lastNameTF:JKTextField, emailTF:JKTextField, passwordTF:JKTextField,confirmPasswordTF:JKTextField, userType:JSUserRole?, onSuccess:@escaping()->Void){
       
        guard NetworkState.state.isConnected,let firstName = firstNameTF.text,let lastName = lastNameTF.text,let email = emailTF.text,let password = passwordTF.text,let confirmPassword = confirmPasswordTF.text  else {
            return
        }
      
        if firstName.isEmpty {
            alertMessage = FieldValidation.kFistNameEmpty
        }else if lastName.isEmpty {
            alertMessage = FieldValidation.kLastNameEmpty
        }
        else if email.isEmpty {
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kValidEmail
        }else if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.length<6{
            alertMessage = FieldValidation.kPassMinLimit
        }else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPassEmpty
        }else if confirmPassword.length<6{
            alertMessage = FieldValidation.kPassMinLimit
        }
        else if password != confirmPassword{
            alertMessage = FieldValidation.kPassMissMatch
        }
        else if userType == nil {
                alertMessage = FieldValidation.kUserTypeEmpty
            }
        else{
          
            SMUtility.shared.showHud()
            let params:[String:Any] = ["firstname":firstName,"lastname":lastName,"email":email,"password":password,"role":userType!.value, "clientType":kClientType]
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSUserModel>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onSuccess()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.Auth.Post.register.api, params: params)
            
        }
        
    }
    
    
    
    // MARK: - Forgot Password
    func forgotPassword(emailTF:JKTextField,onSuccess:@escaping()->Void){
        
        if !NetworkState.state.isConnected {
            return
        }
        guard let email = emailTF.text else { return  }
        
        if email.isEmpty {
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kValidEmail
        }else{
            SMUtility.shared.showHud()
            let params:[String:Any] = ["email":email, "clientType":kClientType]
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSUserModel>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onSuccess()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                        
                        
                    case .failure(let error):
                        alertMessage = error.localizedDescription
                        
                    }
                }
            }.request(JSEndpoint.Auth.Post.forgotPassword.api, params: params)
            
        }
        
    }
    
    
    
    // MARK: - Change Password
    func changePassword(_ password:String,confirmPassword:String, onSuccess:@escaping()->Void){
        
        guard NetworkState.state.isConnected , let userId = self.userId else{return}
        if password.isEmpty {
            alertMessage = FieldValidation.kPasswordEmpty
        }else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPassEmpty
        }else if confirmPassword.length<6{
            alertMessage = FieldValidation.kPassMinLimit
        }else if password != confirmPassword{
            alertMessage = FieldValidation.kPassMissMatch
        }else{
            SMUtility.shared.showHud()
            let params:[String:Any] = ["password":password, "password_confirmation": confirmPassword, "user_id":userId]
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSUserModel>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onSuccess()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                    
                }
            }.request(JSEndpoint.Auth.Post.changePassword.api, params: params)
            
            
        }
        
    }
    
    // MARK: - Get User Profile
    func getUserProfile(onSuccess:@escaping()->Void){
        
        guard NetworkState.state.isConnected, let userId = self.userId else{
            AppDelegate.shared.logoutUser()
            return
            
        }
        
        SMUtility.shared.showHud()
        let header = Server.apiHeaders
        //Server.Request.
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _ ):
                    let response  = data.getValue(JSReponse<JSUserModel>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        
                        guard let model = response?.data else { return }
                        userModel = model
                        userModel?.accessToken = accessToken
                        
                        onSuccess()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Auth.Get.getUser(userId: userId).api,  headers: header, params: nil)
        
        
    }
    
    func updateProfile(firstname:String, lastname:String,email:String,userType:JSUserRole, phone:String,address:String,city:String,state:String,zipcode:String, image:UIImage,onSuccess:@escaping()->Void){
        
        guard NetworkState.state.isConnected, let userId = self.userId else{
            AppDelegate.shared.logoutUser()
            return
        }
        if firstName.isEmpty {
            alertMessage = FieldValidation.kFistNameEmpty
            return
        }
        else if email.isEmpty {
            alertMessage = FieldValidation.kEmailEmpty
            return
        }
        SMUtility.shared.showHud()
        let header = Server.apiHeaders
        
        let param:[String:Any] = ["user_id":userId,"firstname":firstname,"lastname":lastname,"email":email,"role":userType.value,"phone":phone,"address":address,"city":city,"state":state,"zipcode":zipcode]
        Server.Request.uploadTask(data: [.init(image: image, mediaKey: "image", formate: .jpeg(quality: .medium))], completionHandler: { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSUserModel>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let msg = response?.message  { alertMessage = msg  }
                        self.getUserProfile(onSuccess: onSuccess)
                    default:
                        guard let msg = response?.message else { return  }
                        alertMessage = msg
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
        }, progressHandler: nil).request(JSEndpoint.Auth.Post.updateUser.api, headers: header, params: param)
        
    }
    
    func logout(){
        guard NetworkState.state.isConnected,  !accessToken.isEmpty else{return}
        
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSUserModel>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        AppDelegate.shared.logoutUser()
                        
                    default:
                        guard let msg = response?.message else { return  }
                        alertMessage = msg
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Auth.Get.logout.api)
    }
}

extension JSUserViewModel{
    var userId:Int?{
        return userModel?.userID
    }
    var firstName:String{
        return userModel?.firstName ?? ""
    }
    var lastName:String{
        return userModel?.lastName ?? ""
    }
    var email:String{
        return userModel?.email ?? ""
    }
    var phone:String{
        return userModel?.phone ?? ""
    }
    var profileImage:String{
        return userModel?.profileImage ?? ""
    }
    var address:String{
        return userModel?.address ?? ""
    }
    var zipCode:String{
        return userModel?.zipCode ?? ""
    }
    var city:String{
        return userModel?.city ?? ""
    }
    var state:String{
        return userModel?.state ?? ""
    }
    var userType:JSUserRole?{
        guard let val  = userModel?.role, let userType = JSUserRole(rawValue: val)  else {return
            nil}
        return userType
    }
    
}
