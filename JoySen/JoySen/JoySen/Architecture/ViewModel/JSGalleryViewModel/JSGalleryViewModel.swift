//
//  JSGalleryViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

final class JSGalleryViewModel{
    static let shared = JSGalleryViewModel()
    fileprivate var galleryList:[JSGallery] = []
    
    func getGalleryList(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected else {
            return
        }
        
        Server.Request.dataTask(method: .get) { (result) in
            
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSGallery]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let list = response?.data else { return }
                        self.galleryList = list
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
        }.request(JSEndpoint.Gallery.Get.galleries.api)
    }
}
extension  JSGalleryViewModel{
    var count:Int{
        return self.galleryList.count
    }
    subscript(at index:Int)->JSGallery?{
        return self.galleryList[index]
    }
    func removeAll(){
        self.galleryList.removeAll()
    }
}
