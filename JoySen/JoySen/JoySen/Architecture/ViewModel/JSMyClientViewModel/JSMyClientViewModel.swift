//
//  JSMyClientViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 25/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

final class JSMyClientViewModel {
    static let shared = JSMyClientViewModel()
   fileprivate var myClients:[JSMyClient] = []
   fileprivate var searchClients:[JSMyClient] = []
   fileprivate var objClient:JSMyClient?
    var serviceType:JSServiceType = .normal
    func setObject(){
        if objClient != nil {
            objClient = nil
        }
        objClient = JSMyClient()
    }
    //MARK:- imageFormate
   fileprivate func imageMultiFormate(_ image:UIImage, fileName:String? = nil, uploadKey:String =  "image")->MultipartData{
        return DataFormate.multipart.result(dataType: .image(image: image, fileName: fileName, uploadKey: "image", formate: .jpeg(quality: .medium))) as! MultipartData
        
    }
    //MARK:- getMyClinets
    func getMyClinets( _ refreshControl:UIRefreshControl? = nil,onCompletion:@escaping()->Void){
        
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{
            refreshControl?.endRefreshing()
            return
            
        }
        if refreshControl == nil{
             SMUtility.shared.showHud()
        }
       
        
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                if let refreshControl = refreshControl, refreshControl.isRefreshing {
                     refreshControl.endRefreshing()
                }
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSMyClient]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data { self.myClients = list }
                        self.serviceType = .normal
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Clients.Get.clients(userId: userID).api)
    }
    
    //MARK:- getMyClient
    func getMyClient(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let clientId = objClient?.id else{ return }
        SMUtility.shared.showHud()
        
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSMyClient>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let obj = response?.data { self.objClient = obj}
                        
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Clients.Get.client(clientId: clientId).api)
    }
    
    //MARK:- addClient
    func addClient(
        profileImage:UIImage,
        fileName:String? = nil,
        onCompletion:@escaping()->Void){
        if  firstName == nil || firstName?.isEmpty  == true{
            alertMessage = FieldValidation.kFistNameEmpty
        }else if email == nil || email?.isEmpty == true {
            alertMessage = FieldValidation.kEmailEmpty
            
        }else if email?.isEmail == false{
            alertMessage = FieldValidation.kValidEmail
        }else if city == nil || city?.isEmpty == true{
            alertMessage = FieldValidation.kCityEmpty
            
        }else{
            guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
            SMUtility.shared.showHud()
            let multi = imageMultiFormate(profileImage, uploadKey: "image")
            
            
            let params:[String:Any] = [
                "user_id":userID,
                "firstname":firstName ?? "",
                "lastname":lastName ?? "",
                "stage":stage ?? "",
                "lead_source":leadSource ?? "",
                "email":email ?? "",
                "phone":phone ?? "",
                "phone_type":phoneType1 ?? "",
                "phone2":phone2 ?? "",
                "phone_type2":phoneType2 ?? "",
                //Address Information
                "address":address ?? "",
                "city":city ?? "",
                "state":state ?? "",
                "zipcode":zipCode ?? "",
                //Shipping Address
                "shipping_address":shippingAddress ?? "",
                "shipping_city":city ?? "",
                "shipping_state":shippingState ?? "",
                "shipping_zipcode":shippingZipCode ?? "",
                //Frequecy Information
                "frequency":frequency ?? "",
                "group":group ?? "",
                "status":status ?? ""
                
                
            ]
            Server.Request.uploadTask(data: [multi], completionHandler: { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<String>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            if let message = response?.message  { alertMessage =  message  }
                            
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }, progressHandler: nil).request(JSEndpoint.Clients.Post.addClient.api,params: params)
        }
        
        
        
    }
    
    //MARK:- updateClient
    func updateClient(
        profileImage:UIImage,
        onCompletion:@escaping()->Void){
        
        if firstName == nil || firstName?.isEmpty  == true{
            alertMessage = FieldValidation.kFistNameEmpty
            
        }else if  email == nil || email?.isEmpty == true {
            alertMessage = FieldValidation.kEmailEmpty
            return
        }else if email?.isEmail == false{
            alertMessage = FieldValidation.kValidEmail
            
        }else if city == nil || city?.isEmpty == true{
            alertMessage = FieldValidation.kCityEmpty
            
        }else{
            
            guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId, let clientID = objClient?.id else{ return }
            SMUtility.shared.showHud()
            let multi = imageMultiFormate(profileImage, uploadKey: "image")
            
            
            let params:[String:Any] = [
                "user_id":userID,
                "client_id":clientID,
                "firstname":firstName ?? "",
                "lastname":lastName ?? "",
                "stage":stage ?? "",
                "lead_source":leadSource ?? "",
                "email":email ?? "",
                //uploadKey:base64String,
                "phone":phone ?? "",
                "phone_type":phoneType1 ?? "",
                "phone2":phone2 ?? "",
                "phone_type2":phoneType2 ?? "",
                //Address Information
                "address":address ?? "",
                "city":city ?? "",
                "state":state ?? "",
                "zipcode":zipCode ?? "",
                //Shipping Address
                "shipping_address":shippingAddress ?? "",
                "shipping_city":city ?? "",
                "shipping_state":shippingState ?? "",
                "shipping_zipcode":shippingZipCode ?? "",
                //Frequecy Information
                "frequency":frequency ?? "",
                "group":group ?? "",
                "status":status ?? ""
                
                
            ]
            
            Server.Request.uploadTask(data: [multi], completionHandler: { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<String>.self)
                        
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            if let message = response?.message  { alertMessage =  message  }
                            
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }, progressHandler: nil).request(JSEndpoint.Clients.Post.updateClient.api,params: params)
            
        }
        
        
    }
    //MARK:- SearchClient
    
    func searchClient(_ query:String,onCompletion:@escaping()->Void){
        
        guard !query.isEmpty else {
            alertMessage = "Please Enter Keyword to search Client."
            return
        }
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
                   SMUtility.shared.showHud()
        let params:[String:Any] = ["user_id":userID,"keyword":query]
        Server.Request.dataTask(method: .post)  { (result) in
                      async {
                          SMUtility.shared.hideHud()
                          switch result{
                          case .success(let data, _):
                              let response  = data.getValue(JSReponse<[JSMyClient]>.self)
                              guard let statusType = response?.statusType else { return  }
                              switch statusType {
                              case .success:
                                  guard let list = response?.data else { return  }
                                  self.searchClients = list
                                  self.serviceType = .searching
                                  onCompletion()
                              default:
                                  guard let message = response?.message else { return  }
                                  alertMessage =  message
                              }
                          case .failure(let err):
                              alertMessage = err.localizedDescription
                          }
                      }
                  }.request(JSEndpoint.Clients.Post.searchClient.api,params: params)
        
    }
    
    //MARK:- CheckClient
    
    func checkClient(email:String, onCompletion:@escaping()->Void){      
        if email.isEmpty{
            alertMessage = FieldValidation.kEmailEmpty
        }else if !email.isEmail{
            alertMessage = FieldValidation.kValidEmail
        }else{
            guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
            SMUtility.shared.showHud()
            let params:[String:Any] = [
                "user_id":userID,
                "client_email":email
            ]
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSMyClient>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.Clients.Post.checkClient.api, params: params)
        }
        
    }
    

    //MARK:- didSet
    func didSet(atClient index:Int){
        self.objClient = self.myClients[index]
    }
}
extension JSMyClientViewModel{
    var myClientCount:Int{
        return  serviceType == .searching ?  searchClients.count : myClients.count
    }
    subscript(atClient index:Int)->JSMyClient?{
        return serviceType == .searching ? searchClients[index] : myClients[index]
    }
    func removeAll(){
        if  searchClients.count>0{
            searchClients.removeAll()
        }
        if myClients.count>0 {
            myClients.removeAll()
        }
    }
    var clientImage:String?{
        return objClient?.profileImage
    }
    var name:String?{
        set{
            objClient?.name = newValue
        }
        get{
            return objClient?.name
        }
    }
    var firstName:String?{
        set{
            objClient?.firstname = newValue
        }
        get{
            return objClient?.firstname
        }
        
    }
    var lastName:String?{
        set{
            objClient?.lastname = newValue
        }
        get{
            return objClient?.lastname
        }
        
    }
    var stage:String?{
        set{
            objClient?.stage = newValue
        }
        get{
            return objClient?.stage
        }
        
    }
    var leadSource:String?{
        set{
            objClient?.leadSource = newValue
        }
        get{
            return objClient?.leadSource
        }
        
    }
    var email:String?{
        set{
            objClient?.email = newValue
        }
        get{
            return objClient?.email
        }
        
    }
    
    var phone:String?{
        set{
            objClient?.phone = newValue
        }
        get{
            return objClient?.phone
        }
        
    }
    var phoneType1:String?{
        set{
            objClient?.phoneType = newValue
        }
        get{
            return objClient?.phoneType
        }
        
    }
    var phone2:String?{
        set{
            objClient?.phone2 = newValue
        }
        get{
            return objClient?.phone2
        }
        
    }
    var phoneType2:String?{
        set{
            objClient?.phoneType2 = newValue
        }
        get{
            return objClient?.phoneType2
        }
        
    }
    var address:String?{
        set{
            objClient?.address = newValue
        }
        get{
            return objClient?.address
        }
        
    }
    var city:String?{
        set{
            objClient?.city = newValue
        }
        get{
            return objClient?.city
        }
        
    }
    var state:String?{
        set{
            objClient?.state = newValue
        }
        get{
            return objClient?.state
        }
        
        
    }
    var zipCode:String?{
        set{
            objClient?.zipcode = newValue
        }
        get{
            return objClient?.zipcode
        }
        
    }
    var country:String?{
        set{
            objClient?.country = newValue
        }
        get{
            return objClient?.country
        }
    }
    var shippingAddress:String?{
        set{
            objClient?.shippingAddress = newValue
        }
        get{
            return objClient?.shippingAddress
        }
        
    }
    var shippingCity:String?{
        set{
            objClient?.shippingCity = newValue
        }
        get{
            return objClient?.shippingCity
        }
        
    }
    var shippingState:String?{
        set{
            objClient?.shippingState = newValue
        }
        get{
            return objClient?.shippingState
        }
        
    }
    var shippingZipCode:String?{
        set{
            objClient?.shippingZipcode = newValue
        }
        get{
            return objClient?.shippingZipcode
        }
        
    }
    var shippingCountry:String?{
        set{
            objClient?.shippingCountry = newValue
        }
        get{
            return objClient?.shippingCountry
        }
    }
    var frequency:String?{
        set{
            objClient?.frequency = newValue
        }
        get{
            return objClient?.frequency
        }
        
        
    }
    var frequencyType:JSFrequency?{
        guard let vl = frequency else { return nil }
        return JSFrequency(rawValue: vl)
    }
    var statusType:JSStatus?{
        guard let vl = status else { return nil }
        return JSStatus(rawValue: vl)
    }
    var phoneType1Key:JSPhoneType?{
        guard let vl = phoneType1 else { return nil }
        return JSPhoneType(rawValue: vl)
    }
    var phoneType2Key:JSPhoneType?{
        guard let vl = phoneType2 else { return nil }
        return JSPhoneType(rawValue: vl)
    }
    
    var group:String?{
        set{
            objClient?.group = newValue
        }
        get{
            return objClient?.group
        }
        
    }
    var status:String?{
        set{
            objClient?.status = newValue
        }
        get{
            return objClient?.status
        }
        
    }
    var assignedTo:String?{
        return objClient?.assignedTo
    }
    func didSetData( personal:JSPersonalInfoStack, addressInfo:JSAddressInfoStack, shipping:JSAddressInfoStack,frequency:JSFrequency?, status:JSStatus?,other:JSOtherInfoStack){
        self.firstName = personal.firstName
        self.lastName = personal.lastName
        self.email = personal.email
        self.phone = personal.phone
        self.phoneType1 = personal.phoneType?.lowercased()
        self.phone2 = personal.phone2
        self.phoneType2 = personal.phoneType2?.lowercased()
        self.address = addressInfo.address
        self.zipCode = addressInfo.zipCode
        self.city = addressInfo.city
        self.state = addressInfo.state
        self.country = addressInfo.country
        
        self.shippingAddress = shipping.address
        self.shippingCity = shipping.city
        self.shippingZipCode = shipping.zipCode
        self.shippingState = shipping.state
        self.shippingCountry = shipping.country
        self.frequency = frequency?.rawValue
        self.group = other.group
        self.status = status?.rawValue
        self.leadSource = other.leadSource
        self.stage = other.stage
    }
}
