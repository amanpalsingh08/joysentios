//
//  JSGiftDetailViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 02/05/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
final class JSGiftDetailViewModel{
   static var shared  = JSGiftDetailViewModel()
    fileprivate var giftDetailData:JSGiftItem?
    fileprivate var myClientId:JSMyClient?
    fileprivate var giftsQuantity:[Int] = []
    fileprivate var giftId:Int?
    
    //MARK:- getGiftData
    func getGiftData(onCompletion:@escaping()-> Void){
        
        guard NetworkState.state.isConnected, let giftId = self.giftId else { return  }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSGiftItem>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let vl = response?.data  {
                            self.giftDetailData = vl
                             self.giftsQuantity = self.getGiftsQunatity()
                        }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.GiftStore.Get.getGiftData(giftId: giftId).api)
        
    }
    //MARK:- getGiftData
    func getGiftShippingPrice(onCompletion:@escaping()-> Void){
        if clientId == nil {
            alertMessage = "Please select client"
        }else if giftQunatity == nil {
            alertMessage = "Please select number of gift"
        }else{
            guard NetworkState.state.isConnected,let giftId = self.giftId ,let clientId = self.clientId, let quantity = Int(giftQunatity ?? "1") else{ return }
            SMUtility.shared.showHud()
            Server.Request.dataTask( method: .get) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSGiftItem>.self)
                        guard let statusType = response?.httpStatus else { return  }
                        switch statusType {
                        case .OK:
                            if let vl = response?.data  { self.giftDetailData = vl}
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
                
            }.request(JSEndpoint.GiftStore.Get.getGiftShippingPrice(giftId: giftId, clientId: clientId, quantity: quantity).api)
        }
        
    }
    //MARK:- PurchaseGift
    func buyGift(message:String? = nil,onCompletion:@escaping()->Void){
        
        if clientId == nil {
            alertMessage = "Please select client"
        }else if giftQunatity == nil {
            alertMessage = "Please select number of gift"
        }else{
            
            guard NetworkState.state.isConnected,let giftId = self.giftId ,let clientId = self.clientId, let userId = JSUserViewModel.shared.userId, let quantity = giftQunatity else{ return }
            SMUtility.shared.showHud()
            Server.Request.dataTask(method: .get) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<String>.self)
                        guard let statusType = response?.httpStatus else { return  }
                        switch statusType {
                        case .OK:
                           
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.GiftStore.Get.purchaseGift(giftId: giftId, clientId: clientId, userId: userId, quantity: quantity, message: message).api)
        }
    }
    
}
extension JSGiftDetailViewModel{
    var clientName:String{
         return "\(myClientId?.firstname?.capitalized ?? "") \(myClientId?.lastname?.capitalized ?? "")"
     }
     
     var clientId:Int?{
         return myClientId?.id
     }
    var height:String{
        return self.giftDetailData?.height ?? ""
    }
    var width:String{
        return self.giftDetailData?.width ?? ""
    }
    var length:String{
        return self.giftDetailData?.length ?? ""
    }
    var gitImages:[String]{
        return self.giftDetailData?.images ?? []
    }
    var giftName:String{
        return self.giftDetailData?.giftName ?? ""
    }
    var giftDescription:String{
        return self.giftDetailData?.description ?? ""
    }
    var giftPrice:String?{
        return giftDetailData?.price
    }
    var giftShippingPrice:Double?{
        return giftDetailData?.shipping
    }
    var giftTotalPrice:Double?{
        return giftDetailData?.total
    }
    var giftQunatity:String?{
        set{
            giftDetailData?.quantity  = newValue
        }
        get{
            return giftDetailData?.quantity ?? "1"
        }
    }
    var giftTax:Double?{
        return giftDetailData?.tax
    }

    func didSetGiftId(at  identity:Int?){
        self.giftId = identity
    }

   func didSetClient(at  client:JSMyClient?){
       self.myClientId = client
   }

    
    
}
extension JSGiftDetailViewModel{
    
   fileprivate func getGiftsQunatity()->[Int]{
    guard let giftsAvailable = self.giftDetailData?.giftsAvailable, let intValue = Int(giftsAvailable), intValue>1 else { return [1] }
    
           let list = Array(stride(from:1, through: intValue, by: 1))
           return list
       }
    
    var giftsQuantityCount:Int{
        return giftsQuantity.count
    }
    subscript(atQuantity index:Int)->Int{
        guard index<giftsQuantityCount else{return 1}
        return giftsQuantity[index]
    }
    func didSetQuantity(at quantity:Int){
        self.giftQunatity = String(quantity)
    }
    
    func resetAll(){
        myClientId = nil
        giftsQuantity.removeAll()
        giftDetailData = nil
        
    }
    
}
