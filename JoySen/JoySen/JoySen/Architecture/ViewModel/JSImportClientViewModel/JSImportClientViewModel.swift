//
//  JSImportClientViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 14/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
final class JSImportClientViewModel{
    static let shared  = JSImportClientViewModel()
    
    
    func importClient(_ pickerData:UIDocumentPickerData,onCompletion:@escaping()->Void){
        if pickerData.fileUrl.pathExtension != "csv" {
            alertMessage = "Please Select only CSV File"
        }
        
        guard NetworkState.state.isConnected, let userId = JSUserViewModel.shared.userId else{
            AppDelegate.shared.logoutUser()
            return
        }
        
        SMUtility.shared.showHud()
        let param:[String:Any] = ["user_id":userId]
        Server.Request.uploadTask(data: [.init(file: pickerData.fileUrl, mediaKey: "file")], completionHandler: { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSUserModel>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let msg = response?.message  { alertMessage = msg  }
                        onCompletion()
                    default:
                        guard let msg = response?.message else { return  }
                        alertMessage = msg
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
        }, progressHandler: nil).request(JSEndpoint.Clients.Post.importClient.api, params: param)
        
    }
}
