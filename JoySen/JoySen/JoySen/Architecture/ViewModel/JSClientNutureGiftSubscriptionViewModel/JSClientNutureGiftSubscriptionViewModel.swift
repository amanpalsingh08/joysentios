//
//  JSClientNutureGiftSubscriptionViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 04/01/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
enum JSContributionType {
    case selfContribute
    case conn1Contribute
    
}
final class JSClientNutureGiftSubscriptionViewModel{
    
    
    static let shared = JSClientNutureGiftSubscriptionViewModel()
    //MARK:- Properties
    fileprivate var frequencyList:[JSFrequency]  = []
    fileprivate var numberOfGifts:[Int] = []
    fileprivate var monthsOfGiftList:[JSMonthsOfGift] = []
    fileprivate var giftTotal:JSGiftTotal?
    fileprivate var currentFrequcny:JSFrequency? = nil
    fileprivate var myclient:JSMyClient?
    fileprivate var selectedGifts:Int?
    fileprivate var contributes:[JSContribution] = []
    var conn1Id:Int?
    var conn2Id:Int?
    fileprivate var selfContribute:Double?
    fileprivate var conn1Contribute:Double?
    fileprivate var conn2Contribute:Double{
        return 100 - ((selfContribute ?? 0.0) + (conn1Contribute ?? 0.0))
    }
    var conn2ContributeString:String{
        return "\(conn2Contribute)%"
    }
    //MARK:- getMonthsOfGifts
    func getMonthsOfGifts(onCompletion:@escaping()-> Void){
        if self.clientId == nil {
            alertMessage = "Please Select client"
        }
        else{
            guard NetworkState.state.isConnected, isShowMonthOfGift == true  else{ return }
            SMUtility.shared.showHud()
            Server.Request.dataTask(method: .get) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<[String]>.self)
                        guard let statusType = response?.httpStatus else { return  }
                        switch statusType {
                        case .OK:
                            if let list = response?.data  { self.monthsOfGiftList = list.compactMap({JSMonthsOfGift($0)}) }
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.ClientNutureGiftSubscription.Get.getYears.api)
        }
    }
    //MARK:- getGitTotals
    func getGitTotals(onCompletion:@escaping()->Void){
        if self.clientId == nil {
            alertMessage = "Please Select client"
        }else if self.currentFrequcny == nil{
            alertMessage = "Please Select frequency"
        }else if selectedGifts == nil || selectedGifts == 0 {
            alertMessage = "Please Select  gifts"
            
        }else{
            if currentFrequcny == .custom ,let gifts = selectedGifts , gifts != selectedmonthsOfGift.count{
                alertMessage = "Please select months as much gift numbers are selected."
                return
            }
            guard NetworkState.state.isConnected , let userId = JSUserViewModel.shared.userId, let gifts = self.selectedGifts else{ return }
            SMUtility.shared.showHud()
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<JSGiftTotal>.self)
                        guard let statusType = response?.httpStatus else { return  }
                        switch statusType {
                        case .OK:
                            if let obj = response?.data  { self.giftTotal = obj }
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.ClientNutureGiftSubscription.Post.getTotal.api,params: ["user_id":userId,"gifts":gifts])
            
            
        }
        
        
    }
     //MARK:- requestConnection Type connection1, Connection2
    func requestConnection(_ compensationType:JSCompensationType, isRequestConnection1:Bool,onCompletion:@escaping()->Void){
        if self.clientId == nil {
            alertMessage = "Please Select client"
        }else if self.currentFrequcny == nil{
            alertMessage = "Please Select frequency"
        }else if selectedGifts == nil || selectedGifts == 0 {
            alertMessage = "Please Select  gifts"
            
        }
        
        guard NetworkState.state.isConnected , let userId = JSUserViewModel.shared.userId,  let clientId  = self.clientId else{ return }
        SMUtility.shared.showHud()
        var selfContri:String = ""
        var conn1Contri:String = ""
        var connContri:String = ""
        var months:[String] = []
        let frequency = currentFrequcny?.title ?? ""
        if currentFrequcny == .custom {
            months = selectedmonthsOfGift.compactMap({$0.title})
            //month = list.joined(separator: ",")
        }else{
            months = []
        }
        var params:[String : Any] = [
            "user_id":userId,
            "client_id":clientId,
            "frequency": frequency,
            "gifts":selectedGifts!,
            "total":totalGiftPayAmount,
            "months":months
        ]
        

        if compensationType == .custom {
             selfContri =  selfContribute != nil ? "\(selfContribute!)" : ""
            if isRequestConnection1 == false {
                conn1Contri =  self.conn1Contribute != nil ? "\(conn1Contribute!)" : ""
            }
            connContri = "\(self.conn2Contribute)"
        }else{
            //defult
            selfContribute = 33.33
            selfContri =  selfContribute != nil ? "\(selfContribute!)" : ""
            if isRequestConnection1 == false {
                self.conn1Contribute = 33.33
                conn1Contri =  self.conn1Contribute != nil ? "\(conn1Contribute!)" : ""
            }
            connContri = "\(self.conn2Contribute)"
            
        }
        if isRequestConnection1 {
            params["self_contri"] = selfContri
            params["conn_contri"] = connContri
            params["conn"] = conn1Id != nil ? conn1Id! : ""
            
        }else{
            params["self_contri"] = selfContri
            params["conn1_contri"] = conn1Contri
            params["conn2_contri"] = connContri
            params["conn1"] = conn1Id != nil ? conn1Id! : ""
            params["conn2"] = conn2Id != nil ? conn2Id! : ""
        }
        let api  = isRequestConnection1 ? JSEndpoint.ClientNutureGiftSubscription.Post.requestConnection1.api : JSEndpoint.ClientNutureGiftSubscription.Post.requestConnection2.api
        Server.Request.dataTask(method: .post) { (result) in
            
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<String>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if response?.statusType == .success {
                            alertMessage = """
                            Request has been successfully send to the Connections.
                            Please wait request to be Accepted.
                            Payment will get deducted from your card automatically when all connections accepts the request
                            """
                        }else{
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                        
                        
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
        }.request(api, params: params)
        
        
    }
    //MARK:- 100 % Payment
    //MARK:- PayNOW
    func payNow(onCompletion:@escaping()->Void){
        if self.clientId == nil {
            alertMessage = "Please Select client"
        }else if self.currentFrequcny == nil{
            alertMessage = "Please Select frequency"
        }else if selectedGifts == nil || selectedGifts == 0 {
            alertMessage = "Please Select  gifts"
            
        }
        
        guard NetworkState.state.isConnected , let userId = JSUserViewModel.shared.userId,  let clientId  = self.clientId else{ return }
        SMUtility.shared.showHud()
        var months:[String] = []
        let frequency = currentFrequcny?.title ?? ""
        if currentFrequcny == .custom {
            months = selectedmonthsOfGift.compactMap({$0.title})
            //month = list.joined(separator: ",")
        }else{
            months = []
        }
        let params:[String : Any] = [
            "user_id":userId,
            "client_id":clientId,
            "frequency": frequency,
            "gifts":selectedGifts!,
            "total":totalGiftPayAmount,
            "months":months
        ]
        Server.Request.dataTask(method: .post) { (result) in
            
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<String>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
        }.request(JSEndpoint.ClientNutureGiftSubscription.Post.payNow.api, params: params)
        
    }
    //MARK:- subscribePayment
    func subscribePayment(onCompletion:@escaping()->Void){
        if self.clientId == nil {
            alertMessage = "Please Select client"
        }
        
        guard NetworkState.state.isConnected , let userId = JSUserViewModel.shared.userId,  let clientId  = self.clientId else{ return }
        SMUtility.shared.showHud()
        
        let params:[String : Any] = [
            "user_id":userId,
            "client_id":clientId,
            
        ]
        Server.Request.dataTask(method: .post) { (result) in
            
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<String>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
        }.request(JSEndpoint.ClientNutureGiftSubscription.Post.subscribePayment.api, params: params)
        
        
    }
    //MARK:- getFrequencyData
    func getFrequencyData(onCompletion:@escaping()->Void){
        guard myclient != nil else {
            alertMessage = "Please Select Client first"
            return
        }
        self.frequencyList = [.monthly,.quaterly,.semiAnnualy,.annualy,.custom]
        onCompletion()
    }
    
    //MARK:- getNumberOfGifts
    func getNumberOfGifts(onCompletion:@escaping()->Void){
        
        guard let frequency = currentFrequcny else {
            alertMessage = "Please Select Frequency"
            return
        }
        self.numberOfGifts = frequency.numberOfGifts
        onCompletion()
        
    }
    //MARK:- getContributions
    func getContributions(onCompletion:@escaping()->Void){
        var thought:Int = 100
        if let selfContri = selfContribute {
            thought -= Int(selfContri)
        }
        let list  = Array(stride(from: 0, through: thought, by: 10))
        contributes = list.compactMap({JSContribution($0)})
        onCompletion()
    }
}
extension JSClientNutureGiftSubscriptionViewModel{
    
    
    var frequencyCount:Int{
        return self.frequencyList.count
    }
    subscript(atFrequency index:Int)->JSFrequency?{
        return frequencyList[index]
    }
    
    func didSetFrequency(at index:Int){
        self.currentFrequcny = self[atFrequency: index]
    }
    func didSetFrequency(at frequecny:JSFrequency?){
        self.currentFrequcny = frequecny
    }
    func removeAllFrequency(){
        if frequencyCount>0 {
            frequencyList.removeAll()
        }
        
    }
    
    var numberOfGiftsCount:Int{
        return self.numberOfGifts.count
    }
    subscript(atNumOfGit index:Int)->Int?{
        return numberOfGifts[index]
    }
    func didSetGifts(_ gifts:Int?){
        self.selectedGifts = gifts
    }
    func removeAllNumOfGit(){
        if numberOfGiftsCount>0 {
             numberOfGifts.removeAll()
        }
       
    }
    
    func didSetClient(at  item:JSMyClient?){
        self.myclient = item
    }
    
    var monthOfGiftCount:Int{
        return self.monthsOfGiftList.count
    }
    subscript(atMonthOfGift index:Int)->JSMonthsOfGift?{
        return self.monthsOfGiftList[index]
    }
   fileprivate var selectedmonthsOfGift:[JSMonthsOfGift]{
     return monthsOfGiftList.filter({$0.isSelected == true}).compactMap({$0})
    }
   var selectedmonthsOfGiftCount:Int{
        return selectedmonthsOfGift.count
    }
    func removeAllMonthsOfGift(){
       if self.monthOfGiftCount>0{
         self.monthsOfGiftList.removeAll()
        }
    }
    func didUpdateCheckAccessoryOfMonth(at index:Int){
        if isShowMonthOfGift{
            
            let isSelected = self[atMonthOfGift: index]?.isSelected ?? false
            if isSelected == false{
                if  let gift  = self.selectedGifts,selectedmonthsOfGiftCount<gift  {
                    self.monthsOfGiftList[index].isSelected = true
                }
            }else{
                self.monthsOfGiftList[index].isSelected = false
            }
            
        }
       
    }
    
    var contributionCount:Int{
        return self.contributes.count
    }
    subscript(atContribute index:Int)->JSContribution?{
        self.contributes[index]
    }
    func removeAllContribution(){
        if contributionCount>0 {
             self.contributes.removeAll()
        }
        
    }
    func didSetContribution(at index:Int, type:JSContributionType){
        let item  = self[atContribute: index]
        if type == .selfContribute {
            self.selfContribute = Double(item?.contribute ?? 0)
        }else if type == .conn1Contribute{
            self.conn1Contribute =  Double(item?.contribute ?? 0)
        }
        
        
    }
    func didSetContribution(at item:JSContribution?, type:JSContributionType){
        
        if type == .selfContribute {
            if  self.conn1Contribute != nil{
                self.conn1Contribute = nil
            }
            self.selfContribute =  Double(item?.contribute ?? 0)
        }else if type == .conn1Contribute{
            self.conn1Contribute =   Double(item?.contribute ?? 0)
        }
        
        
    }
   
    var isSelfContribute:Bool{
        return selfContribute == nil ? false : true
        
    }
    
    var gitTax:String{
        return giftTotal?.tax ?? ""
    }
    var pricePerGift:String{
        return giftTotal?.pricePerGift ?? ""
    }
    var totalGiftPayAmount:Double{
        return giftTotal?.total ?? 0.0
    }
    var isShowMonthOfGift:Bool{
        guard let frequency = currentFrequcny else{return false}
        return frequency == .custom ? true : false
    }
    var isShowNumOfGifts:Bool{
        return  currentFrequcny == nil ? false : true
    }
    var isShowFrequency:Bool{
        return  myclient == nil ? false : true
    }
    var clientName:String{
        return "\(myclient?.firstname?.capitalized ?? "") \(myclient?.lastname?.capitalized ?? "")"
    }
    var clientId:Int?{
        return myclient?.id
    }
    func resetAll(){
        myclient = nil
        selfContribute = nil
        conn1Contribute = nil
        currentFrequcny = nil
        giftTotal = nil
        selectedGifts = nil
        self.removeAllNumOfGit()
        self.removeAllFrequency()
        self.removeAllContribution()
        self.removeAllMonthsOfGift()
        
    }
    
}
