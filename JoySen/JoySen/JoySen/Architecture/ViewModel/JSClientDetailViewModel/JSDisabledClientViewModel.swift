//
//  JSDisabledClientViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
final class JSDisabledClientViewModel{
    static let shared = JSDisabledClientViewModel()
   fileprivate var disabledClients:[JSDisabledClient] = []
    
    //MARK- getDisabledClients
    func getDisabledClients(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
               SMUtility.shared.showHud()
               Server.Request.dataTask(method: .get) { (result) in
                   async {
                       SMUtility.shared.hideHud()
                       switch result{
                       case .success(let data, _):
                           let response  = data.getValue(JSReponse<[JSDisabledClient]>.self)
                           guard let statusType = response?.statusType else { return  }
                           switch statusType {
                           case .success:
                               if let list = response?.data { self.disabledClients = list }
                               onCompletion()
                           default:
                               guard let message = response?.message else { return  }
                               alertMessage =  message
                           }
                       case .failure(let err):
                           alertMessage = err.localizedDescription
                       }
                   }
               }.request(JSEndpoint.Clients.Get.disabledClient(userId: userID).api)
    }
    
    
}
extension JSDisabledClientViewModel{
    var count:Int{
        return disabledClients.count
    }
    subscript(at index:Int)->JSDisabledClient?{
        return self.disabledClients[index]
    }
    func removeAll(){
        self.disabledClients.removeAll()
    }
}
