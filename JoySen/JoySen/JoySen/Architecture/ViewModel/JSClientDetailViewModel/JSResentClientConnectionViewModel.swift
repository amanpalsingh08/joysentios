//
//  JSResentClientConnectionViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

final class JSResentClientConnectionViewModel{
    static let shared  = JSResentClientConnectionViewModel()
    fileprivate var connections:[JSResentClientConnection] = []
    
    
    //MARK- getDisabledClients
    func getResentClientConnections(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
               SMUtility.shared.showHud()
               Server.Request.dataTask(method: .get) { (result) in
                   async {
                       SMUtility.shared.hideHud()
                       switch result{
                       case .success(let data, _):
                           let response  = data.getValue(JSReponse<[JSResentClientConnection]>.self)
                           guard let statusType = response?.statusType else { return  }
                           switch statusType {
                           case .success:
                               if let list = response?.data { self.connections = list }
                               onCompletion()
                           default:
                               guard let message = response?.message else { return  }
                               alertMessage =  message
                           }
                       case .failure(let err):
                           alertMessage = err.localizedDescription
                       }
                   }
               }.request(JSEndpoint.Auth.Get.recentConnection(userId: userID).api)
 
    }
}

extension JSResentClientConnectionViewModel{
    var count:Int{
        return connections.count
    }
    subscript(at index:Int)->JSResentClientConnection?{
        return self.connections[index]
    }
    func removeAll(){
        self.connections.removeAll()
    }
}
