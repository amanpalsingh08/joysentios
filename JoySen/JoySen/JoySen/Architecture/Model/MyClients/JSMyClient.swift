//
//  JSMyClient.swift
//  JoySen
//
//  Created by Jitendra Kumar on 25/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
struct JSMyClient:Mappable {
    var id: Int?
    var userid:String?
    var firstname: String?
    var lastname: String?
    var name: String?
    var stage: String?
    var leadSource: String?
    var assignedTo: String?
    var email: String?
    var profileImage: String?
    var phone: String?
    var phoneType: String?
    var phone2: String?
    var phoneType2: String?
    var address: String?
    var city: String?
    var state: String?
    var zipcode: String?
    var country: String?
    var shippingAddress: String?
    var shippingCity: String?
    var shippingState: String?
    var shippingZipcode: String?
    var shippingCountry: String?
    var frequency: String?
    var group: String?
    var status: String?
    var createdAt: String?
    var updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, userid, firstname, lastname, name, stage
        case leadSource = "lead_source"
        case assignedTo = "assigned_to"
        case email
        case profileImage = "profile_image"
        case phone
        case phoneType = "phone_type"
        case phone2
        case phoneType2 = "phone_type2"
        case address, city, state, zipcode, country
        case shippingAddress = "shipping_address"
        case shippingCity = "shipping_city"
        case shippingState = "shipping_state"
        case shippingZipcode = "shipping_zipcode"
        case shippingCountry = "shipping_country"
        case frequency, group, status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
 
    fileprivate var jsFrequency:JSFrequency?{
        guard let val = self.frequency else { return nil }
        return JSFrequency(rawValue: val)
    }
   
    fileprivate var jsPhoneType:JSPhoneType?{
        guard let val = self.phoneType else { return nil }
        return JSPhoneType(rawValue: val)
    }
     var jsPhoneType2:JSPhoneType?{
         guard let val = self.phoneType2 else { return nil }
         return JSPhoneType(rawValue: val)
    }
     var jsStatus:JSStatus?{
         guard let val = self.status else { return nil }
        return JSStatus(rawValue: val)
    }
}


