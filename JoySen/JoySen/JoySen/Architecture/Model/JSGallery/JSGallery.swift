//
//  JSGallery.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit
struct JSGallery:Mappable {
    var imageFile, description: String?
    var imageSize:CGSize?
    enum CodingKeys: String, CodingKey {
        case imageFile = "img"
        case description = "description"
    }
}
extension JSGallery:Equatable{
    static func == (lhs:JSGallery, rhs:JSGallery)->Bool{
        return lhs.imageFile == rhs.imageFile && lhs.description == rhs.description
    }
}
