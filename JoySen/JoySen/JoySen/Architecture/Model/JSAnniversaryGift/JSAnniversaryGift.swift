//
//  JSAnniversaryGift.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation

struct JSGitCategory: Mappable,Hashable {
    var id: Int?
    var parentID: String?
    var typeName, typeSlug, createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case parentID = "parent_id"
        case typeName = "type_name"
        case typeSlug = "type_slug"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct JSGift:Mappable,Hashable {
    var id: Int?
    var giftName, description, price, height: String?
    var width, length, weight: String?
    var img: [String]?

    enum CodingKeys: String, CodingKey {
        case id
        case giftName = "gift_name"
        case description = "description"
        case price, height, width, length, weight, img
    }
}
struct JSAnniversaryGiftData:Mappable,Hashable {
     var giftID, giftName, description, weight: String?
     var length, width, height, giftsAvailable: String?
     var price: String?
     var images: [String]?
     var quantity: String?
     var tax, shipping, total: Double?

     enum CodingKeys: String, CodingKey {
         case giftID = "gift_id"
         case giftName = "gift_name"
         case description = "description"
         case weight, length, width, height
         case giftsAvailable = "gifts_available"
         case price, images, quantity, tax, shipping, total
     }
}
