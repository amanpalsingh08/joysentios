//
//  JSCard.swift
//  JoySen
//
//  Created by Jitendra Kumar on 03/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
struct JSCard:Mappable {
    var id: Int?
    var userid, cardNum, expMonth, expYear: String?
    var customerID, createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, userid
        case cardNum = "card_num"
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case customerID = "customer_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
extension JSCard:Equatable{
    static func == (lhs:JSCard, rhs:JSCard)->Bool{
        return lhs.id == rhs.id && lhs.userid == lhs.userid && lhs.cardNum == rhs.cardNum && lhs.expMonth == rhs.expMonth && lhs.expYear == rhs.expYear && lhs.customerID == rhs.customerID && lhs.createdAt == rhs.createdAt && lhs.updatedAt == rhs.updatedAt
    }
}
