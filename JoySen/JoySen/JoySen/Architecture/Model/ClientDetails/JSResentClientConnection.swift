//
//  JSResentClientConnection.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
struct JSResentClientConnection:Mappable {
    var id: Int?
    var clientID, clientName, clientFirstname, clientLastname: String?
    var clientStage, type, clientLeadSource, clientAssignedTo: String?
    var clientConnection1, clientConnection2: String?
    var clientReferer: String?
    var clientConn1Name, clientConn2Name, clientGroup: String?
    
    var trackingID: Int?
    var orderID: Int?
    var orderNumber:String?
    var labelID: Int?
    var status, createdAt, updatedAt, address: String?
    var city, state, email: String?
    var phone: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "client_id"
        case clientName = "client_name"
        case clientFirstname = "client_firstname"
        case clientLastname = "client_lastname"
        case clientStage = "client_stage"
        case type
        case clientLeadSource = "client_lead_source"
        case clientAssignedTo = "client_assigned_to"
        case clientConnection1 = "client_connection1"
        case clientConnection2 = "client_connection2"
        case clientReferer = "client_referer"
        case clientConn1Name = "client_conn1_name"
        case clientConn2Name = "client_conn2_name"
        case clientGroup = "client_group"
        case trackingID = "tracking_id"
        case orderID = "orderId"
        case orderNumber
        case labelID = "label_id"
        case status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case address, city, state, email, phone
    }
}
