//
//  JSQueueClient.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

// MARK: - JSQueueClient
struct JSQueueClient:Mappable {

    var id: Int?
    var clientID, clientName, clientFirstname, clientLastname: String?
    var clientStage, type, clientLeadSource, clientAssignedTo: String?
    var clientConnection1, clientConnection2: String?
    var clientReferer: Int?
    var clientConn1Name, clientConn2Name, clientGroup, trackingID: String?
    var orderID, orderNumber, labelID, status: String?
    var createdAt: String?
    var updatedAt: String?
    var address, city, state, email: String?
    var phone, group: String?

    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "client_id"
        case clientName = "client_name"
        case clientFirstname = "client_firstname"
        case clientLastname = "client_lastname"
        case clientStage = "client_stage"
        case type
        case clientLeadSource = "client_lead_source"
        case clientAssignedTo = "client_assigned_to"
        case clientConnection1 = "client_connection1"
        case clientConnection2 = "client_connection2"
        case clientReferer = "client_referer"
        case clientConn1Name = "client_conn1_name"
        case clientConn2Name = "client_conn2_name"
        case clientGroup = "client_group"
        case trackingID = "tracking_id"
        case orderID = "orderId"
        case orderNumber
        case labelID = "label_id"
        case status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case address, city, state, email, phone, group
    }
}
