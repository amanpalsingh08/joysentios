//
//  JSDisabledClient.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
struct JSDisabledClient:Mappable {
    var clientName, email: String?
    var phone: String?
    var address: String?
    var state: String?
    var city: String?
    var group: String?
    var lastDelivered, con1, con2: String?

    enum CodingKeys: String, CodingKey {
        case clientName = "client_name"
        case email, phone, address, state, city, group
        case lastDelivered = "last_delivered"
        case con1, con2
    }
}

