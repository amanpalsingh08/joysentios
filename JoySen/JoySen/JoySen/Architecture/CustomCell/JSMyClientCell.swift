//
//  JSMyClientCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 25/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSMyClientCell: UITableViewCell {
    @IBOutlet private weak var userView:JKImageView!
     @IBOutlet private weak var namelbl:UILabel!
     @IBOutlet private weak var emaillbl:UILabel!
    @IBOutlet private weak var groupBtn:JKButton!
    @IBOutlet private weak var requestConnBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var rowIndex:Int = 0{
        didSet{
            requestConnBtn.tag = rowIndex
        }
    }
    var client:JSMyClient?{
        didSet{
            namelbl.text = "\(client?.firstname?.capitalized ?? "") \(client?.lastname?.capitalized ?? "")"
            emaillbl.text = client?.email ?? ""
            groupBtn.normalTitle = client?.group?.capitalized ?? ""
            if let file = client?.profileImage {
                userView.loadImage(filePath: file)
            }
        }
    }
}
