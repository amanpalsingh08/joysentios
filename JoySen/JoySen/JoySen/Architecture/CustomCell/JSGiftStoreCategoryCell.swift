//
//  JSGiftStoreCategoryCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 28/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSGiftStoreCategoryCell: UITableViewCell {
    @IBOutlet private weak var titleLabel:JKLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var cat:JSGitCategory?{
        didSet{
            titleLabel.text = cat?.typeName ?? ""
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class JSGiftSelctMonthCell:UICollectionViewCell{
    @IBOutlet private weak var titleLabel:UILabel!
    @IBOutlet private weak var card:JKCardView!
     
    var month:JSMonthsOfGift?{
        didSet{
            titleLabel.text = month?.title ?? ""
            card.backgroundColor = month?.isSelected == true ? #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1) : .white
            titleLabel.textColor = month?.isSelected == true ? .white : .black
        }
    }
    
}
