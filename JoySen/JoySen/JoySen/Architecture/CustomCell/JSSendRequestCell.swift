//
//  JSSendRequestCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSSendRequestCell: UITableViewCell {
    @IBOutlet fileprivate weak var namelbl:UILabel!
    @IBOutlet fileprivate weak var emaillbl:UILabel!
    @IBOutlet fileprivate weak var connection1lbl:UILabel!
    @IBOutlet fileprivate weak var connection2lbl:UILabel!
    @IBOutlet fileprivate weak var requestStatusBtn:JKButton!
    fileprivate var viewModel = JSRequestsViewModel.shared
    var refreshData:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        requestStatusBtn.addTarget(self, action: #selector(self.onClickRequest(_:)), for: .touchUpInside)
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var rowIndex:Int = 0{
        didSet{
            requestStatusBtn.tag = rowIndex
        }
    }
    var sendItem:JSRequestsSent?{
        didSet{
            namelbl.text = sendItem?.clientName ?? ""
            emaillbl.text = sendItem?.clientEmail ?? ""
            connection1lbl.text = sendItem?.conn1 ?? ""
            connection2lbl.text = sendItem?.conn2 ?? ""
            requestStatusBtn.normalTitle = sendItem?.requestStatusType?.title ?? ""
            if let type = sendItem?.requestStatusType{
                requestStatusBtn.isEnabled = type == .pending ? true : false
                requestStatusBtn.backgroundColor = type.color
            }
        }
    }
    @objc
    fileprivate func onClickRequest(_ sender:JKButton){
       guard let type = self.sendItem?.requestStatusType else { return }
               if type == .pending {
                currentController?.showTitleActionSheet(message: "Do you really want to Cancel this request", cancelTitle: "Dismiss",otherTitles: ["Destroy(Remove Request)","OK(Pay 100% of Amount and remove request)"], onCompletion: { (index) in
                    if index == 1{
                        self.viewModel.distroyRequest(at: self.rowIndex) {
                            self.refreshData?()
                        }
                    }else if index == 2{
                        self.viewModel.payAll(at: self.rowIndex) {
                            self.refreshData?()
                        }
                    }
                })
                
               }
    }
}
