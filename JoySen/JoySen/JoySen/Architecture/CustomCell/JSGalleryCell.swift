//
//  JSGalleryCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSGalleryCell: UITableViewCell {
    @IBOutlet fileprivate weak var imageBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   var item:JSGallery?{
        didSet{
            if let file = item?.imageFile?.originalUrl {
                imageBtn.loadImage(filePath: file, for: .normal)
            }
        }
  
    }
}
