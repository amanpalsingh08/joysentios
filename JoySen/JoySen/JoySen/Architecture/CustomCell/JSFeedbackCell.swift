//
//  JSFeedbackCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSFeedbackCell: UITableViewCell {
    @IBOutlet fileprivate weak var namelbl:UILabel!
    @IBOutlet fileprivate weak var feedbacklbl:UILabel!
    @IBOutlet fileprivate weak var emaillbl:UILabel!
    @IBOutlet fileprivate weak var editBtn:JKButton!
    @IBOutlet fileprivate weak var deleteBtn:JKButton!
    var refresh:(()->Void)?
    fileprivate var vieWModel = JSFeedbackViewModel.shared
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        deleteBtn.addTarget(self, action: #selector(self.onDelete(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var rowIndex:Int = 0{
        didSet{
            deleteBtn.tag = rowIndex
            editBtn.tag = rowIndex
        }
    }
    var feedback:JSFeedback?{
        didSet{
            namelbl.text = "\(feedback?.clientFirstname?.capitalized ?? "") \(feedback?.clientLastname?.capitalized ?? "")"
            emaillbl.text = feedback?.clientEmail ?? ""
            feedbacklbl.text = feedback?.feedback ?? ""
        }
    }
    @objc
    fileprivate func onDelete(_ sender:JKButton){
        guard let id = feedback?.feedbackID else { return }
        vieWModel.deleteFeedback(feedbackId: id) {
            async {
                self.refresh?()
            }
        }
        
    }
}
