//
//  JSGiftCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSGiftCell: UITableViewCell {
    @IBOutlet private var giftCarouselView:UICarouselView!
    @IBOutlet private var titlelbl:UILabel!
    @IBOutlet private var detaillbl:UILabel!
    @IBOutlet private var pricelbl:UILabel!
    @IBOutlet private var detailBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var rowIndex:Int = 0 {
        didSet{
            self.detailBtn.tag = rowIndex
        }
    }
    var gift:JSGifts?{
        didSet{
            if let images = gift?.img {
                 giftCarouselView.setImages(urls: images)
            }
           
            titlelbl.text = gift?.giftName ?? ""
            detaillbl.text = gift?.description ?? ""
             let price = gift?.price ?? ""
             pricelbl.text = price.isEmpty == false ? "$\(price)" : ""
        
            
        }
    }
    func stopCarousel(){
        giftCarouselView.stop()
    }

}
