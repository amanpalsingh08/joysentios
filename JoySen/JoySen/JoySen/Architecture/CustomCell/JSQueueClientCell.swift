//
//  JSQueueClientCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSQueueClientCell: UITableViewCell {
    @IBOutlet private weak var namelbl:UILabel!
    @IBOutlet private weak var phonelbl:UILabel!
    @IBOutlet private weak var emaillbl:UILabel!
    @IBOutlet private weak var addresslbl:UILabel!
    @IBOutlet private weak var citylbl:UILabel!
    @IBOutlet private weak var statelbl:UILabel!
    @IBOutlet private weak var shippingMonthlbl:UILabel!
    @IBOutlet private weak var grouplbl: UILabel!
    @IBOutlet private weak var callBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var queueItem:JSQueueClient?{
        didSet{
            namelbl.text = queueItem?.clientName == nil ? "\(queueItem?.clientFirstname?.capitalized ?? "") \(queueItem?.clientLastname?.capitalized ?? "")" : "\(queueItem?.clientName?.capitalized ?? "")"
            phonelbl.text = queueItem?.phone ?? ""
            emaillbl.text = queueItem?.email ?? ""
            addresslbl.text = queueItem?.address ?? ""
            citylbl.text = queueItem?.city ?? ""
            statelbl.text = queueItem?.state ?? ""
            shippingMonthlbl.text = ""
            grouplbl.text = queueItem?.group?.capitalized ?? ""
             callBtn.isHidden = phonelbl.text?.isEmpty == true
        }
    }
    var disabledClient:JSDisabledClient?{
        didSet{
            namelbl.text = disabledClient?.clientName ?? ""
            phonelbl.text = disabledClient?.phone ?? ""
            emaillbl.text = disabledClient?.email ?? ""
            addresslbl.text = disabledClient?.address ?? ""
            citylbl.text = disabledClient?.city ?? ""
            statelbl.text = disabledClient?.state ?? ""
            grouplbl.text = disabledClient?.group?.capitalized ?? ""
             callBtn.isHidden = phonelbl.text?.isEmpty == true
        }
    }
    @IBAction private func onCall(_ sender: Any) {
        guard let phoneLabel = self.phonelbl , let number = phoneLabel.text else {return}
        number.makeACall()
    }
}
class JSResentClientConnectionCell: UITableViewCell {
    @IBOutlet private weak var namelbl:UILabel!
    @IBOutlet private weak var phonelbl:UILabel!
    @IBOutlet private weak var emaillbl:UILabel!
    @IBOutlet private weak var addresslbl:UILabel!
    @IBOutlet private weak var citylbl:UILabel!
    @IBOutlet private weak var statelbl:UILabel!
    @IBOutlet private weak var dateAddedlbl:UILabel!
    @IBOutlet private weak var conn1lbl:UILabel!
    @IBOutlet private weak var conn2lbl:UILabel!
    @IBOutlet private weak var callBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var resentConnection:JSResentClientConnection?{
        didSet{
            namelbl.text =  resentConnection?.clientName == nil ? "\(resentConnection?.clientFirstname?.capitalized ?? "") \(resentConnection?.clientLastname?.capitalized ?? "")" : "\(resentConnection?.clientName?.capitalized ?? "")"
            phonelbl.text = resentConnection?.phone ?? ""
            emaillbl.text = resentConnection?.email ?? ""
            addresslbl.text = resentConnection?.address ?? ""
            citylbl.text = resentConnection?.city ?? ""
            statelbl.text = resentConnection?.state ?? ""
            dateAddedlbl.text =  resentConnection?.createdAt ?? ""
            conn1lbl.text = resentConnection?.clientConn1Name ?? ""
            conn2lbl.text = resentConnection?.clientConn2Name ?? ""
             callBtn.isHidden = phonelbl.text?.isEmpty == true
        }
    }
    @IBAction private func onCall(_ sender: Any) {
        guard let phoneLabel = self.phonelbl , let number = phoneLabel.text else {return}
              number.makeACall()
    }
    
    
}


class JSClientLastDeliveredCell: UITableViewCell {
    @IBOutlet private weak var fnamelbl:UILabel!
    @IBOutlet private weak var lnamelbl:UILabel!
    @IBOutlet private weak var emaillbl:UILabel!
    @IBOutlet private weak var grouplbl:UILabel!
    @IBOutlet private weak var phonelbl:UILabel!
    @IBOutlet private weak var addresslbl:UILabel!
    @IBOutlet private weak var citylbl:UILabel!
    @IBOutlet private weak var statelbl:UILabel!
    @IBOutlet private weak var dateShippinglbl:UILabel!
    @IBOutlet private weak var trakinglbl:UILabel!
    @IBOutlet private weak var callBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var model:JSDeliveredClient?{
        didSet{
          
            fnamelbl.text = model?.clientFirstname?.capitalized ?? ""
            lnamelbl.text = model?.clientLastname?.capitalized ?? ""
            phonelbl.text = model?.phone ?? ""
            emaillbl.text = model?.email ?? ""
            addresslbl.text = model?.address ?? ""
            citylbl.text = model?.city ?? ""
            statelbl.text = model?.state ?? ""
            grouplbl.text = model?.clientGroup?.capitalized ?? ""
            dateShippinglbl.text =  model?.createdAt ?? ""
            let traking = model?.trackingID ?? 0
            trakinglbl.text =  traking == 0 ? "" :"\(traking)"
            callBtn.isHidden = phonelbl.text?.isEmpty == true
          
        }
    }
    @IBAction private func onCall(_ sender: Any) {
        guard let phoneLabel = self.phonelbl , let number = phoneLabel.text else {return}
        number.makeACall()
    }
    
    
}


