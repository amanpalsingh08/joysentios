//
//  JSMyClientsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 23/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSMyClientsVC: UIViewController {
    @IBOutlet fileprivate var tableView:UITableView!
    fileprivate var vm = JSMyClientViewModel.shared
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl!.addTarget(self, action: #selector(self.onRefresh(_:)), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getClients()
    }
   
     @objc fileprivate func onRefresh(_ sender:UIRefreshControl){
        self.getClients(sender)
    }
    private func getClients(_ refreshControl:UIRefreshControl? = nil){
        vm.removeAll()
        self.tableView.reloadData()
        vm.getMyClinets(refreshControl, onCompletion: {
            async {
                
                self.tableView.reloadData()
            }
        })
       
    }
    
    // MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kAddMyClientSegue {
            guard let   navController  = segue.destination as? UINavigationController , let controller = navController.topViewController as? JSAddMyClientVC else { return  }
            controller.isAddNewClient = true
            controller.AddClientCompletion = {
                self.getClients()
               
            }
        }else if segue.identifier == SegueIdentity.kMyClientInfoSegue, let indexPath  = self.tableView.indexPathForSelectedRow {
            vm.didSet(atClient: indexPath.row)
            
        }else if segue.identifier == SegueIdentity.kRequestConnectionsSegue, let requestBtn  = sender as? JKButton  {
            let controller  = segue.destination as! JSRequestConnectionsVC
            controller.clientID = vm[atClient: requestBtn.tag]?.id
            
            
        }
    }
    
    
    
}
extension JSMyClientsVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.myClientCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kMyClientCell, for: indexPath) as! JSMyClientCell
        cell.rowIndex = indexPath.row
        cell.client = vm[atClient: indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    
}
extension JSMyClientsVC:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
         vm.removeAll()
        self.getClients()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard let keyword  = searchBar.text else {
            return
        }
       // vm.removeAll()
       // tableView.reloadData()
        vm.searchClient(keyword) {
            async {
                self.tableView.reloadData()
            }
        }
    }
}
