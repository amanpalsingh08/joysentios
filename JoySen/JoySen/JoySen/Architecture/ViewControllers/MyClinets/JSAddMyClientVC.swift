//
//  JSAddMyClientVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 25/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSAddMyClientVC: UIViewController {
    fileprivate var vm = JSMyClientViewModel.shared
    private var uploadImage:UIImage?
    var isAddNewClient:Bool = true
    @IBOutlet fileprivate weak var clientEmailTF: JKTextField!
    @IBOutlet fileprivate weak var emailClientStack: UIStackView!
    @IBOutlet fileprivate weak var checkClinetlbl: UILabel!
    //Personal Information
    @IBOutlet weak var profileBtn: JKButton!
    @IBOutlet fileprivate weak var personalInfo: JSPersonalInfoStack!
    //Address Information
    @IBOutlet fileprivate weak var addressInfo: JSAddressInfoStack!
    //Shipping Address Information
    @IBOutlet  fileprivate weak var shippingAddressInfo: JSAddressInfoStack!
    //Other Information
    @IBOutlet fileprivate weak var otherInfo: JSOtherInfoStack!
    @IBOutlet weak var submitBtn: JKButton!
    
    var AddClientCompletion:(()->Void)?
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    
    fileprivate var frequency:JSFrequency?{
        didSet{
            otherInfo.frequency = frequency?.title
        }
    }
    
    fileprivate var phoneType:JSPhoneType?{
        didSet{
            personalInfo.phoneType = phoneType?.title
        }
    }
    fileprivate var phoneType2:JSPhoneType?{
        didSet{
            personalInfo.phoneType2 = phoneType2?.title
        }
    }
    fileprivate var status:JSStatus?{
        didSet{
            otherInfo.status = status?.title
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuBtn.image = isFromMenu ? #imageLiteral(resourceName: "ic_menu"): UIImage(systemName: "multiply")
        if isAddNewClient {
            emailClientStack.isHidden = false
            checkClinetlbl.isHidden = false
        }else{
            emailClientStack.isHidden = true
            checkClinetlbl.isHidden = true
        }
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isAddNewClient ? "Add Client" : "Update Client"
        submitBtn.normalTitle = isAddNewClient ? "Submit" : "Update"
        if isAddNewClient {
            vm.setObject()
        }else{
            getClient()
        }
        
    }
    //MARK:- getClient
    private func getClient(){
        vm.getMyClient {
            async {
                self.loadClientData()
            }
        }
    }
     //MARK:- loadClientData
    private func loadClientData(){
        personalInfo.firstName = vm.firstName?.capitalized ?? ""
        personalInfo.lastName = vm.lastName?.capitalized ?? ""
        personalInfo.email = vm.email
        personalInfo.phone = vm.phone
        self.phoneType = vm.phoneType1Key
        personalInfo.phone2 = vm.phone2
        self.phoneType2 = vm.phoneType2Key
        
        addressInfo.address = vm.address
        addressInfo.state = vm.state
        addressInfo.zipCode = vm.zipCode
        addressInfo.country = vm.country
        addressInfo.city    = vm.city
        
        shippingAddressInfo.address = vm.shippingAddress
        shippingAddressInfo.state = vm.shippingState
        shippingAddressInfo.zipCode = vm.shippingZipCode
        shippingAddressInfo.country = vm.shippingCountry
        shippingAddressInfo.city    = vm.shippingCity
        
        self.frequency = vm.frequencyType
        otherInfo.group = vm.group
        otherInfo.stage = vm.stage
        otherInfo.leadSource = vm.leadSource
        otherInfo.assignedTo = vm.assignedTo
        self.status = vm.statusType
        
        if let file = vm.clientImage {
            profileBtn.loadImage(filePath: file, for: .normal) { (result) in
                async {
                    switch result{
                    case .success(let val):
                        self.uploadImage = val
                    case .failure(_):break
                    }
                }
            }
        }
        
    }
    //MARK:- onProfileImage
    @IBAction private func onProfileImage(_ sender:JKButton){
        
        UIBottomSheet.shared.showPicker(pickerOptions: [.photoLibrary,.photoCamera], message: "Please choose any one option for \(isAddNewClient ? "Add" : "Update") client profile image.", source: sender, allowsEditing: true) { (result) in
            switch result{
            case .image(let image, _):
                async {
                    self.profileBtn.normalImage = image
                    self.uploadImage = image
                    
                }
            default:break
            }
        }
        
        
    }
    //MARK:- onCheckCientEmail
    @IBAction private func onCheckCientEmail(_ sender: Any) {
        self.view.endEditing(true)
        guard let email = clientEmailTF.text else { return  }
        vm.checkClient(email: email) {
            async {
                self.dismiss()
                
            }
        }
        
    }
    
     //MARK:- onSubmit
    @IBAction private func onSubmit(_ sender: Any) {
        self.view.endEditing(true)
        guard let image = uploadImage else {
            alertMessage = "Please select profile image"
            return  }
        
        vm.didSetData(personal: personalInfo, addressInfo: addressInfo, shipping: shippingAddressInfo, frequency: frequency,status: status, other: otherInfo)
        if isAddNewClient{
            vm.addClient(profileImage: image, onCompletion: {
                async {
                    self.dismiss()
                }
            })
        }else{
            vm.updateClient(profileImage: image) {
                async {
                    self.dismiss()
                }
            }
            
        }
    }
     //MARK:- dismiss
    private func dismiss(){
        if !isFromMenu{
            self.dismiss(animated: true) {
                self.AddClientCompletion?()
            }
        }else{
           isFromMenu = false
            self.toggleLeft()
        }
    }
     //MARK:- onMenu
    @IBAction func onMenu(_ sender: Any) {
        dismiss()
    }
     //MARK:- Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == SegueIdentity.kGroupPickerSegue {
            return self.frequency == nil ? false : true
        }else{
            return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kPhoneTypePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .phoneType1(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kPhoneType2PickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .phoneType2(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kStatePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .state(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kShippingStatePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .shippingState(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kFrequencyPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .frequency(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kGroupPickerSegue {
            guard let controller = segue.destination as? JSPickerVC ,let fcy  = self.frequency else{return}
            self.showPicker(controller, pickerType: .group(frequencyType: fcy, ""), sender: sender)
        }else  if segue.identifier == SegueIdentity.kLeadSourcePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .leadSource(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kStagePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .stage(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kStatusPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .status(nil), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
        }
        
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .state(let val):
                self.addressInfo.state = val?.name
            case .stage(let val):
                self.otherInfo.stage = val?.name
            case .leadSource(let val):
                self.otherInfo.leadSource = val?.name
            case .phoneType1(let val):
                self.phoneType = val
            case .phoneType2(let val):
                self.phoneType2 = val
            case .shippingState(let val):
                self.shippingAddressInfo.state = val?.name
            case .frequency(let val):
                self.frequency = val
            case .group(_, let val):
                self.otherInfo.group = val
            case .status(let val):
                self.status = val
            default:break
                
            }
        }
        
    }
    
}
extension JSAddMyClientVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
