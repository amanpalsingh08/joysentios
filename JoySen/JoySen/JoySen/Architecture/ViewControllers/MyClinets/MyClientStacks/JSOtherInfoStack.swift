//
//  JSOtherInfoStack.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit


class JSOtherInfoStack: UIStackView {
    
    //show Client
    @IBOutlet private weak var frequencylbl: UILabel!
    @IBOutlet private weak var grouplbl: UILabel!
    @IBOutlet private weak var leadSourcelbl: UILabel!
    @IBOutlet private weak var stagelbl: UILabel!
    @IBOutlet private weak var assignedTolbl: UILabel!
    @IBOutlet private weak var statuslbl: UILabel!
    
    //Add Client / Update Client
    @IBOutlet private weak var frequencyTF: JKTextField!
    @IBOutlet private weak var groupTF: JKTextField!
    @IBOutlet private weak var leadSourceTF: JKTextField!
    @IBOutlet private weak var stageTF: JKTextField!
    @IBOutlet private weak var statusTF: JKTextField!
    
    
    var frequency:String?{
        set{
            if let label = self.frequencylbl {
                label.text = newValue ?? ""
            }
            if let field = self.frequencyTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.frequencylbl {
                return label.text
            }
            if let field = self.frequencyTF {
                return field.text
            }
            return nil
        }
    }
    var group:String?{
        set{
            if let label = self.grouplbl {
                label.text = newValue ?? ""
            }
            if let field = self.groupTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.grouplbl {
                return label.text
            }
            if let field = self.groupTF {
                return field.text
            }
            return nil
        }
    }
    var leadSource:String?{
        set{
            if let label = self.leadSourcelbl {
                label.text = newValue ?? ""
            }
            if let field = self.leadSourceTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.leadSourcelbl {
                return label.text
            }
            if let field = self.leadSourceTF {
                return field.text
            }
            return nil
        }
    }
    
    var stage:String?{
        set{
            if let label = self.stagelbl {
                label.text = newValue ?? ""
            }
            if let field = self.stageTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.stagelbl {
                return label.text
            }
            if let field = self.stageTF {
                return field.text
            }
            return nil
        }
    }
    
    var status:String?{
        set{
            if let label = self.statuslbl {
                label.text = newValue ?? ""
            }
            if let field = self.statusTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.statuslbl {
                return label.text
            }
            if let field = self.statusTF {
                return field.text
            }
            return nil
        }
    }
    var assignedTo:String?{
        set{
            if let label = self.assignedTolbl {
                label.text = newValue ?? ""
            }
            
            
        }
        get{
            if let label = self.assignedTolbl {
                return label.text
            }
            
            return nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
