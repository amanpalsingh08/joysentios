//
//  JSPersonalInfoStack.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSPersonalInfoStack: UIStackView {
    //show Client
    @IBOutlet private weak var namelbl: UILabel!
    @IBOutlet private weak var emaillbl: UILabel!
    @IBOutlet private weak var phone1lbl: UILabel!
    @IBOutlet private weak var phoneType1lbl: UILabel!
    @IBOutlet private weak var phone2lbl: UILabel!
    @IBOutlet private weak var phoneType2lbl: UILabel!
    
    //Add Client / Update Client
    @IBOutlet private weak var firstNameTF: JKTextField!
    @IBOutlet private weak var lastNameTF: JKTextField!
    @IBOutlet private weak var emailTF: JKTextField!
    @IBOutlet private weak var phone1TF: JKTextField!
    @IBOutlet private weak var phoneType1TF: JKTextField!
    @IBOutlet private weak var phone2TF: JKTextField!
    @IBOutlet private weak var phoneType2TF: JKTextField!
    @IBOutlet private weak var callBtn1: JKButton!
    @IBOutlet private weak var callBtn2: JKButton!
    
    var firstName:String?{
        set{
            if let field = self.firstNameTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let field = self.firstNameTF {
                return field.text
            }
            
            return nil
        }
    }
    var lastName:String?{
        set{
            if let field = self.lastNameTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let field = self.lastNameTF {
                return field.text
            }
            
            return nil
        }
    }
    var name:String?{
        set{
            if let label = self.namelbl {
                label.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.namelbl {
                return label.text
            }
            
            return nil
        }
    }
    
    var email:String?{
        set{
            if let label = self.emaillbl {
                label.text = newValue ?? ""
            }
            if let field = self.emailTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.emaillbl {
                return label.text
            }
            if let field = self.emailTF {
                return field.text
            }
            return nil
        }
    }
    
    var phone:String?{
        set{
            if let label = self.phone1lbl {
                label.text = newValue ?? ""
                callBtn1.isHidden = label.text?.isEmpty == true
            }
            if let field = self.phone1TF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.phone1lbl {
                return label.text
                
            }
            if let field = self.phone1TF {
                return field.text
            }
            return nil
        }
    }
    var phoneType:String?{
        set{
            if let label = self.phoneType1lbl {
                label.text = newValue ?? ""
            }
            if let field = self.phoneType1TF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.phoneType1lbl {
                return label.text
            }
            if let field = self.phoneType1TF {
                return field.text
            }
            return nil
        }
    }
    var phone2:String?{
        set{
            if let label = self.phone2lbl {
                label.text = newValue ?? ""
                callBtn2.isHidden = label.text?.isEmpty == true
            }
            if let field = self.phone2TF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.phone2lbl {
                return label.text
            }
            if let field = self.phone2TF {
                return field.text
            }
            return nil
        }
    }
    var phoneType2:String?{
        set{
            if let label = self.phoneType2lbl {
                label.text = newValue ?? ""
            }
            if let field = self.phoneType2TF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.phoneType2lbl {
                return label.text
            }
            if let field = self.phoneType2TF {
                return field.text
            }
            return nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let phone1TF = self.phone1TF {
            phone1TF.delegate = self
        }
        if let phone2TF = self.phone2TF {
            phone2TF.delegate = self
        }
        
    }
    
    @IBAction private func onCall1(_ sender:Any){
        if let phone  = phone {
            phone.makeACall()
        }
        
    }
    @IBAction private func onCall2(_ sender:Any){
        if let phone  = phone2 {
            phone.makeACall()
        }
    }
    
}




extension JSPersonalInfoStack:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phone1TF || textField == phone2TF  {
            var fullString = textField.text ?? ""
            fullString.append(string)
            if fullString.count>textField.maxLength {
                return false
            }
            let shouldRemoveLastDigit = range.length == 1 ? true : false
            textField.text = fullString.formattedNumber(shouldRemoveLastDigit: shouldRemoveLastDigit)
            
            return false
        }else{
            return true
        }
        
    }
}
