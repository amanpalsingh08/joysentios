//
//  JSMyClientDetailVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 25/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSMyClientDetailVC: UIViewController {
    
    fileprivate var vm = JSMyClientViewModel.shared
    //Personal Information
    @IBOutlet weak var profileBtn: JKButton!
    @IBOutlet fileprivate weak var personalInfo: JSPersonalInfoStack!
    //Address Information
    @IBOutlet fileprivate weak var addressInfo: JSAddressInfoStack!
    //Shipping Address Information
    @IBOutlet  fileprivate weak var shippingAddressInfo: JSAddressInfoStack!
    //Other Information
    @IBOutlet fileprivate weak var otherInfo: JSOtherInfoStack!
    //Feedback
    
    @IBOutlet fileprivate weak var feedbacklbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getClient()
    }
    private func getClient(){
        vm.getMyClient {
            async {
                self.loadClientData()
            }
        }
    }
    
    private func loadClientData(){
        personalInfo.name = "\(vm.firstName?.capitalized ?? "") \(vm.lastName?.capitalized ?? "")"
        personalInfo.email = vm.email
        personalInfo.phone = vm.phone
        personalInfo.phoneType = vm.phoneType1Key?.title
        personalInfo.phone2 = vm.phone2
        personalInfo.phoneType2 = vm.phoneType2Key?.title
        
        addressInfo.address = vm.address
        addressInfo.state = vm.state
        addressInfo.zipCode = vm.zipCode
        addressInfo.country = vm.country
        addressInfo.city = vm.city
        shippingAddressInfo.address = vm.shippingAddress
        shippingAddressInfo.state = vm.shippingState
        shippingAddressInfo.zipCode = vm.shippingZipCode
        shippingAddressInfo.country = vm.shippingCountry
        shippingAddressInfo.city    = vm.shippingCity
        
        otherInfo.frequency = vm.frequencyType?.title
        otherInfo.group = vm.group
        otherInfo.stage = vm.stage
        otherInfo.leadSource = vm.leadSource
        otherInfo.assignedTo = vm.assignedTo
        otherInfo.status = vm.statusType?.title
        if let file = vm.clientImage {
            profileBtn.loadImage(filePath: file, for: .normal)
        }
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kUpdateMyClientSegue {
            guard let   navController  = segue.destination as? UINavigationController , let controller = navController.topViewController as? JSAddMyClientVC else { return  }
            controller.isAddNewClient = false
            controller.AddClientCompletion = {
                self.getClient()
               
            }
            
        }
    }
    
    
}
