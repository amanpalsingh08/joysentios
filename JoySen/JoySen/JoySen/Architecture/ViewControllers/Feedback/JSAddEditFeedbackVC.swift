//
//  JSAddEditFeedbackVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSAddEditFeedbackVC: UIViewController {

    @IBOutlet weak var clientTF: JKTextField!
    @IBOutlet weak var clientPickerView: UIView!
    @IBOutlet weak var feedbackTV: JKTextView!
    var isAddFeedback:Bool = true
    var viewModel:JSFeedbackViewModel = JSFeedbackViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        showClientDropdown()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func showClientDropdown(){
        self.title = isAddFeedback ? "Client Feedback": "Edit Feedback"
        self.clientPickerView.isHidden = isAddFeedback ? false : true
        if !isAddFeedback {
            clientTF.text = viewModel.clientName ?? ""
            feedbackTV.text = viewModel.feedback ?? ""
        }
    }
    @IBAction fileprivate func onSubmit(_ sender:Any){
        self.view.endEditing(true)
        guard let feedback = feedbackTV.text else { return }
        viewModel.feedback = feedback
        viewModel.addEditFeedback(!isAddFeedback) {
            async {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == SegueIdentity.kClientPickerSegue {
                   guard let controller = segue.destination as? JSPickerVC else{return}
                   controller.pickerType = .client(nil)
                   if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
                       popoverController.sourceView = sd
                       popoverController.sourceRect = sd.bounds
                       popoverController.delegate = self
                       controller.preferredContentSize.width = sd.bounds.width
                       
                   }
                   controller.didSelectPickerItem = {(item) in
                       switch item {
                       case .client(let vsl):
                        self.clientTF.text = "\(vsl?.firstname?.capitalized ?? "") \(vsl?.lastname?.capitalized ?? "")"
                        if let clientId = vsl?.id {
                             self.viewModel.clientID = "\(clientId)"
                        }
                       
                           
                       default:
                           break
                       }
                   }
                   
               }
    }
    

}
extension JSAddEditFeedbackVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
