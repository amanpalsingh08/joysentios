//
//  LoginVC.swift
//  ZGuideZ
//
//  Created by Mandeep Kaur on 20/11/18.
//  Copyright © 2018 Mandeep Kaur. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak fileprivate var emailTF: JKTextField!
    @IBOutlet weak fileprivate var passwordTF: JKTextField!
    @IBOutlet weak fileprivate var objUserVM:JSUserViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        passwordTF.right { (accesoryBtn) in
            self.passwordTF.isSecureTextEntry = accesoryBtn.isSelected == true ? false : true
        }
            
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction fileprivate func onLogin(_ sender: Any) {
        self.view.endEditing(true)
        objUserVM.login(emailTF: emailTF, passwordTF: passwordTF, onSuccess: {
            async {
              AppDelegate.shared.showMainController()
            }
        }
            )
    }
    
    
    @IBAction func backBtnClick(_ sender: Any) {
        self.view.endEditing(true)
        _ =  self.navigationController?.popViewController(animated:false)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == SegueIdentity.kTabBarSegue {
//            let controller = segue.destination as! JSTabBarController
//        }
//        else if segue.identifier == SegueIdentity.kSignUpSegue {
//            let controller = segue.destination as! SignupVC
//            controller.email = emailTF.text!
//        }
//    }
 

}
