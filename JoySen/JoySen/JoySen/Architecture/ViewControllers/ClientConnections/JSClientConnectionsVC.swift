//
//  JSClientConnectionsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 03/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSClientConnectionsVC: UITableViewController {
    fileprivate var viewModel = JSClientConnectionViewModel.shared
    @IBOutlet fileprivate weak var searchBar:UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        getClientConnection()
    }
    
    
    fileprivate func getClientConnection(){
        viewModel.removeAll()
        self.tableView.reloadData()
        viewModel.getClientConnections {
            async {
                self.tableView.reloadData()
                
            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.serviceType == .searching ? viewModel.searchConnectionCount : viewModel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.serviceType == .searching {
            let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kSearchConnectionCell, for: indexPath) as! JSSearchConnectionCell
            cell.searchConnection = viewModel[atSearch: indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kClientConnectionCell, for: indexPath) as! JSClientConnectionCell
            cell.rowIndex = indexPath.row
            cell.item = viewModel[at: indexPath.row]
            cell.refreshData = {
                self.getClientConnection()
            }
            return cell
        }
    }
    
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 274
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension JSClientConnectionsVC:UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
          self.view.endEditing(true)
        searchBar.showsCancelButton = false
        if viewModel.searchConnectionCount>0 {
            viewModel.removelAllSearchData()
            if viewModel.count>0 {
                viewModel.serviceType = .normal
                self.tableView.reloadData()
            }else{
                
                self.getClientConnection()
            }
            
        }
        
        
    }
  
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if  let keyword = searchBar.text, !keyword.isEmpty {
                   search(keyword: keyword)
               }else{
                   if viewModel.count>0 {
                       viewModel.serviceType = .normal
                       self.tableView.reloadData()
                   }else{
                       self.getClientConnection()
                   }
               }
        
    }
    fileprivate func search(keyword:String){
        viewModel.serviceType = .searching
        viewModel.searchClientConnection(keyword) {(success) in
            async {
               self.tableView.reloadData()
                
            }
        }
    }
}
