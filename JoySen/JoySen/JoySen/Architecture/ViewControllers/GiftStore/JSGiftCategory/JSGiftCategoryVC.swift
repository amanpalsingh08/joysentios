//
//  JSGiftCategoryVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit


class JSGiftCategoryVC: UITableViewController {
    
    fileprivate let giftStoreVM = JSGiftStoreViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGiftTypes()
        
    }
    fileprivate func getGiftTypes(){
        if giftStoreVM.giftTypeCount>0 {
            giftStoreVM.removeAllGiftTypes()
            self.tableView.reloadData()
        }
        giftStoreVM.getGiftTypes{
            async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    // MARK: - Navigation
   /* override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == SegueIdentity.kGiftsSegue {
            guard let indexPath = tableView.indexPathForSelectedRow, let item  = giftStoreVM[atGiftType:indexPath.row], let type = item.catType else { return false }
            return type == .HomeDecor ? true:false
        }else{
            return true
        }
    }*/
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kClientNurtureGiftSubscriptionSegue {
            
        }else if segue.identifier == SegueIdentity.kGiftsSegue {
            guard let controller = segue.destination as? JSGiftsVC ,let indexPath = tableView.indexPathForSelectedRow, let item  = giftStoreVM[atGiftType:indexPath.row] else { return  }
            controller.title = item.typeName ?? ""
            controller.viewModel.didSetGiftType(identity: item.id)
            
        }
    }
    
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftStoreVM.giftTypeCount
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(JSGiftStoreCategoryCell.self, for: indexPath)
        cell.cat = giftStoreVM[atGiftType:indexPath.row]
        return cell
        
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
