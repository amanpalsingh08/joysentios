//
//  JSAnviersaryGiftsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSGiftsVC: UITableViewController {
    var viewModel = JSGiftsViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGifts()
    }
    private func getGifts(){
        if viewModel.giftCount>0 {
            viewModel.removeAllGifts()
            self.tableView.reloadData()
        }
        viewModel.getGifts {
            async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.giftCount
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(JSGiftCell.self, for: indexPath)
        cell.rowIndex = indexPath.row
        cell.gift = viewModel[atGift: indexPath.row]

        return cell
    }
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? JSGiftCell {
            cell.stopCarousel()
        }
    }

   private func allStop(){
        guard let cells = tableView.visibleCells as?[JSGiftCell] else { return }
        cells.forEach({$0.stopCarousel()})
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kGiftDetailSegue {
            allStop()
            guard let controller = segue.destination as? JSGiftDetailVC,let btn = sender as? JKButton, let item = viewModel[atGift: btn.tag] else { return  }
            controller.viewModel.didSetGiftId(at: item.id)
            
        }
    }
    

}
