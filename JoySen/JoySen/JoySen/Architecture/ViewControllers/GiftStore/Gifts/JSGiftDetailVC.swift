//
//  JSAnviersaryGiftDetailVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

internal class JSGiftDetailInfoStack: UIStackView {
    @IBOutlet weak private var giftNamelbl: UILabel!
    @IBOutlet weak private var descriptionlbl: UILabel!
    @IBOutlet weak private var widthlbl: UILabel!
    @IBOutlet weak private var heightlbl: UILabel!
    @IBOutlet weak private var lengthlbl: UILabel!
    @IBOutlet weak private var pricelbl: UILabel!
    @IBOutlet weak private var clientTF: JKTextField!
    @IBOutlet weak private var numberOfGiftTF: JKTextField!
    @IBOutlet weak private var messageTV: JKTextView!
    var didTapPurchaseNow:(()->Void)?
    var viewModel:JSGiftDetailViewModel?{
        didSet{
            self.giftNamelbl.text = viewModel?.giftName
            self.descriptionlbl.text = viewModel?.giftDescription
            self.widthlbl.text = viewModel?.width
            self.heightlbl.text = viewModel?.height
            self.lengthlbl.text = viewModel?.length
            let giftPrice = viewModel?.giftPrice
            self.pricelbl.text = giftPrice != nil ? "$\(giftPrice!)" : ""
            self.numberOfGift = viewModel?.giftQunatity
        }
    }
    var clientName:String?{
        didSet{
            clientTF.text = clientName
        }
        
    }
    var numberOfGift:String?{
        didSet{
            numberOfGiftTF.text = numberOfGift
        }
        
    }
    @IBAction private func onPurchaseNow(_ sender: JKButton) {
        
        didTapPurchaseNow?()
    }

    
}
internal class JSGiftPaymentInfoStack: UIStackView {
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var quantitylbl: UILabel!
    @IBOutlet weak var taxlbl: UILabel!
    @IBOutlet weak var shippinglbl: UILabel!
    @IBOutlet weak var totalPricelbl: UILabel!
    var didOnTapBuy:((Bool)->Void)?
    var viewModel:JSGiftDetailViewModel?{
        didSet{
            let giftPrice = viewModel?.giftPrice
            self.pricelbl.text = giftPrice != nil ? "$\(giftPrice!)" : ""
            self.quantitylbl.text = viewModel?.giftQunatity
            let tax = viewModel?.giftTax
            self.taxlbl.text = tax != nil ? "$\(tax!)" : ""
            self.shippinglbl.text = viewModel?.height
            let giftTotalPrice = viewModel?.giftTotalPrice
            self.totalPricelbl.text = giftTotalPrice != nil ? "$\(giftTotalPrice!)" : ""
        }
    }
    
    
    @IBAction private func onBuy(_ sender: JKButton) {
        self.didOnTapBuy?(true)
    }
    
    @IBAction  private func onCancel(_ sender: JKButton) {
        self.didOnTapBuy?(false)
    }
}
class JSGiftDetailVC: UIViewController {
    
    @IBOutlet weak var giftCarouselView: UICarouselView!
    @IBOutlet weak var giftInfoStack: JSGiftDetailInfoStack!
    @IBOutlet weak var giftPaymentInfoStack: JSGiftPaymentInfoStack!
    var viewModel = JSGiftDetailViewModel.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        giftInfoStack.didTapPurchaseNow = {
            self.viewModel.getGiftShippingPrice{
                async{
                    self.giftInfoStack.isHidden  = true
                    self.giftPaymentInfoStack.isHidden  = false
                    self.loadData()
                }
            }
        }
        giftPaymentInfoStack.didOnTapBuy = { isBuy in
            if isBuy{
                self.viewModel.buyGift {
                    async{
                        self.giftInfoStack.isHidden  = false
                        self.giftPaymentInfoStack.isHidden  = true
                    }
                }
            }else{
                async{
                    self.giftInfoStack.isHidden  = false
                    self.giftPaymentInfoStack.isHidden  = true
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGiftDetail()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        giftCarouselView.stop()
        viewModel.resetAll()
    }
    private func getGiftDetail(){
        viewModel.getGiftData {
            async {
                self.loadData()
            }
        }
    }
    fileprivate func loadData(){
        self.giftCarouselView.setImages(urls:viewModel.gitImages)
        self.giftPaymentInfoStack.viewModel = viewModel
        self.giftInfoStack.viewModel = viewModel
        
        
    }
    
    //MARK:- onShare
    @IBAction private func onShare(_ sender: UIBarButtonItem) {

       // guard let appLink  =  URL(string: "https://apps.apple.com/in/app/joysent/id1489100919") else{return}
        if let name = URL(string: "https://itunes.apple.com/us/app/joysent/id1489100919?ls=1&mt=8"), !name.absoluteString.isEmpty, UIApplication.shared.canOpenURL(name) {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            if !Platform.isPhone {
                 activityVC.popoverPresentationController?.barButtonItem = sender
                 activityVC.popoverPresentationController?.permittedArrowDirections = .any
            }
            activityVC.completionWithItemsHandler = {(activityType, completed, items, error) in
                        if completed{
                           if let err = error {
                               print("Sharoing Failed = \(err.localizedDescription)")
                           }else{
                               if let activity  = activityType {
                                   print("Activity: \(activity)")
                               }
                               if let list = items {
                                    print("items: \(list)")
                               }
                           }
                           
                       }

                   }
            self.present(activityVC, animated: true, completion: nil)
        }else  {
            // show alert for not available
            
        }
    
        
    }
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kClientPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .client(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kGiftNumberPickerSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .giftQuantity(1), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .client(let client):
                self.viewModel.didSetClient(at: client)
                self.giftInfoStack.clientName = self.viewModel.clientName
                self.loadData()
            case .giftQuantity(let quantity):
                self.viewModel.didSetQuantity(at:quantity)
                self.giftInfoStack.numberOfGift = self.viewModel.giftQunatity
                async {
                    self.loadData()
                }
                
                
            default:
                break
            }
        }
        
    }
    
}
extension JSGiftDetailVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
