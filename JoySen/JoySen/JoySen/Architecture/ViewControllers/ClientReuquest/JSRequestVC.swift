//
//  JSRequestVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSRequestVC: UIViewController {
    @IBOutlet fileprivate weak var segment:UISegmentedControl!
    @IBOutlet fileprivate weak var tableView:UITableView!
    fileprivate var viewModel = JSRequestsViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getRequestData()
    }
    //MARK:- OnChangeRequestType
    @IBAction private func OnChangeRequestType(_ sender:UISegmentedControl){
        getRequestData()
    }
    fileprivate func getRequestData(){
        if segment.selectedSegmentIndex == 0 {
            viewModel.removeAllReceivedRequests()
            self.tableView.reloadData()
            viewModel.getRecievedRequestList {
                async {
                    self.tableView.reloadData()
                }
            }
        }else if segment.selectedSegmentIndex == 1{
            viewModel.removeAllSendRequests()
            self.tableView.reloadData()
            viewModel.getSendRequestList {
                async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension JSRequestVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segment.selectedSegmentIndex == 0 ? viewModel.receivedRequestCount : viewModel.sendRequestCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segment.selectedSegmentIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kSendRequestCell, for: indexPath) as! JSSendRequestCell
            cell.rowIndex = indexPath.row
            cell.sendItem = viewModel[atSend: indexPath.row]
            cell.refreshData = {
                self.getRequestData()
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kReceivedRequestCell, for: indexPath) as! JSReceivedRequestCell
            cell.rowIndex = indexPath.row
            cell.receivedItem = viewModel[atReceived: indexPath.row]
            cell.refreshData = {
                self.getRequestData()
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return segment.selectedSegmentIndex == 1 ? 156 : 226
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
}


