//
//  JSRightSlideMenuVC.swift
//
//
//
//  Copyright © 2018 YOGESH. All rights reserved.
//

import UIKit
enum JSRightMenu:Int {
    case giftStore
    case myAccount
    case changePassword
    case myClients
    case importClients
    case clientConnection
    
    case recentClientConnection
    case queueClient
    case clientLastDelivered
    case recentDisabledClients
    
    case requestNotification
    case cardDetails
    case clientFeedback
    case gallery
    case logout
    var title:String{
        switch self {
        case .giftStore:             return "Gift Store"
        case .myAccount :            return "My Account"
        case .changePassword :       return "Change Password"
        case .myClients:             return "My Clients"
        case .importClients:         return "Import Clients"
        case .clientConnection:      return "Client Connections"
            
        case .requestNotification:   return "Request Notification"
        //Client Detail
        case .recentClientConnection:return "Recent  Client Connections"
        case .queueClient:           return "Queue Clients"
        case .clientLastDelivered:   return "Client Last Delivered"
        case .recentDisabledClients: return "Recent Disabled Clients"
        // end//
        case .cardDetails:           return "Card Details"
        case .clientFeedback:        return "Client Feedback"
        case .gallery:               return "Gallery"
        case .logout:                return "Logout"
        }
        
    }
    var icon:UIImage{
        switch self {
        case .giftStore:             return #imageLiteral(resourceName: "ic_app_logo")
        case .myAccount :            return #imageLiteral(resourceName: "ic_white_account")
        case .changePassword :       return #imageLiteral(resourceName: "ic_white_change_password")
        case .myClients:             return #imageLiteral(resourceName: "ic_white_my_clients")
        case .importClients:         return #imageLiteral(resourceName: "ic_white_import_clients")
        case .clientConnection:      return #imageLiteral(resourceName: "ic_white_clinet_connection")
        case .recentClientConnection:return #imageLiteral(resourceName: "ic_white_recent_client_connections")
        case .queueClient:           return #imageLiteral(resourceName: "ic_white_queue_clients")
        case .clientLastDelivered:   return #imageLiteral(resourceName: "ic_white_clients_last_delivered")
        case .recentDisabledClients: return #imageLiteral(resourceName: "ic_white_recently_disabled_clients")
        case .requestNotification:   return #imageLiteral(resourceName: "ic_white_request_notifications")
        case .cardDetails:           return #imageLiteral(resourceName: "ic_white_card")
        case .clientFeedback:        return #imageLiteral(resourceName: "ic_white_feedbacks_by_You")
        case .gallery:               return #imageLiteral(resourceName: "ic_white_gallery")
        case .logout:                return #imageLiteral(resourceName: "ic_flower")
            
        }
    }
    var navigationController:UINavigationController?{
        switch self {
        case .giftStore:
             return UINavigationController.instance(from: .GiftStore, withIdentifier: StoryBoardIdentity.kGiftCategoryNavigationVC)
        case .myAccount:
            return UINavigationController.instance(from: .Account, withIdentifier: StoryBoardIdentity.kMyAccountNavigationVC)
        case .changePassword:
            return UINavigationController.instance(from: .Account, withIdentifier: StoryBoardIdentity.kChangePasswordNavigationVC)
        case .myClients:
            return UINavigationController.instance(from:.MyClient, withIdentifier: StoryBoardIdentity.kMyClientsNavigationVC)
        case .importClients:
            return UINavigationController.instance(from:.MyClient, withIdentifier: StoryBoardIdentity.kImportClientsNavigationVC)
        case .clientConnection:
            return UINavigationController.instance(from:.MyClient, withIdentifier: StoryBoardIdentity.kClientConnectionsNavigationVC)
        case .requestNotification:
            return UINavigationController.instance(from:.Request, withIdentifier: StoryBoardIdentity.kRequestNavigationVC)
        case .recentClientConnection:
            return UINavigationController.instance(from:.ClientDetail, withIdentifier: StoryBoardIdentity.kRecentClientConnectionsNavigationVC)
        case .queueClient:
            return UINavigationController.instance(from:.ClientDetail, withIdentifier: StoryBoardIdentity.kQueueClientsNavigationVC)
        case .clientLastDelivered:
            return UINavigationController.instance(from:.ClientDetail, withIdentifier: StoryBoardIdentity.kClientLastDeliveredNavigationVC)
        case .recentDisabledClients:
            return UINavigationController.instance(from:.ClientDetail, withIdentifier: StoryBoardIdentity.kRecentDisabledClientsNavigationVC)
        case .cardDetails:
            return UINavigationController.instance(from:.Card, withIdentifier: StoryBoardIdentity.kCardDetailsNavigationVC)
        case .clientFeedback:
            return UINavigationController.instance(from:.Feedback, withIdentifier: StoryBoardIdentity.kFeedbackNavigationVC)
        case .gallery:
            return UINavigationController.instance(from:.Gallery, withIdentifier: StoryBoardIdentity.kGalleryNavigationVC)
        default: return  nil
            
        }
    }
}
class JSRightSlideVC: UIViewController {
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    fileprivate var menulist:[JSRightMenu] = [
        .giftStore,
        .myClients,
        .queueClient,
        .clientLastDelivered,
        .recentDisabledClients,
        .clientConnection,
        .recentClientConnection,
        .requestNotification,
        .importClients,
        .clientFeedback,
        .gallery,
        .cardDetails,
        .myAccount,
        .changePassword,
        .logout
        
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
}
extension JSRightSlideVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menulist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kRightMenuCell, for: indexPath) as! JSRightMenuCell
        cell.item = menulist[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
}

extension JSRightSlideVC:UITableViewDelegate{
    var tabVontroller:JSTabBarController?{
        return  AppDelegate.shared.tabBarController
    }
    fileprivate func  didChange(tabbar item :JSRightMenu){
        var selectedIndex:Int = 2 //queueClient
        isFromMenu = true
        if item == .requestNotification {
            selectedIndex = 1
        }
        guard let controller = tabVontroller else { return  }
        controller.selectedIndex = selectedIndex
        slideMenuController()?.changeMainViewController(mainViewController: controller, close: true)
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let type  = menulist[indexPath.row]
        switch type {
        case .queueClient,.requestNotification:
            self.didChange(tabbar: type)
        case .logout:
            self.toggleLeft()
            self.showLogoutAlert {
                AppDelegate.shared.logoutUser()
                //JSUserViewModel.shared.logout()
            }
            
        default:
            guard let navigation = type.navigationController else{return}
            slideMenuController()?.changeMainViewController(mainViewController: navigation, close: true)
        }
        
        
    }
}

