//
//  JSClientLastDeliveredVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 02/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSClientLastDeliveredVC: UITableViewController {
    fileprivate var viewModel = JSClientLastDeliveredViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        getClientLastDelivered()
    }
    
    //MARK:- getClientLastDelivered
    fileprivate func getClientLastDelivered(){
        viewModel.removeAll()
        self.tableView.reloadData()
        
        viewModel.getDeliveredclients  {
            async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kClientLastDeliveredCell, for: indexPath) as! JSClientLastDeliveredCell
        cell.model = viewModel[at: indexPath.row]
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
