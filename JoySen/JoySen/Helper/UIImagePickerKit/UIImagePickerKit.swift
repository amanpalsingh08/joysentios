//
//  UIImagePickerKit.swift
//  JoySen
//
//  Created by Jitendra Kumar on 23/12/16.
//  Copyright © 2016 Jitendra. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import CoreImage

struct UIImagePickerSource{
    enum UIPickerResult {
          case image(image:UIImage?,filename:String?)
          case file(url:URL?,filename:String?)
          case pickerInfo(info:[UIImagePickerController.InfoKey : Any])
          case cancelled
      }
    
     
    enum Media {
        case photo(SourceType)
        case video(SourceType)
        
        var isPhoto:Bool{
            switch self {
            case .photo:return true
            default: return false
                
            }
        }
        var isSourceTypeCamera:Bool{
            return sourceType == .camera ? true:false
        }
        var sourceType:UIImagePickerController.SourceType{
            switch self {
            case .photo(let vl): return vl.sourceType
            case .video(let vl):return vl.sourceType
            }
        }
        var isSourceTypeAvailable:Bool{
            switch self {
            case .photo(let vl): return vl.isSourceType
            case .video(let vl):return  vl.isSourceType
            }
        }
        var cameraDevice:UIImagePickerController.CameraDevice?{
            switch self {
            case .photo(let vl):
                switch vl {
                case .camera(let device): return (device == .front && device.isCameraAvailable == true) ? CameraDevice.front.cameraDevice : CameraDevice.rear.cameraDevice
                default: return nil
                    
                }
            case .video(let vl):
                switch vl {
                case .camera(let device):return (device == .front && device.isCameraAvailable == true) ? CameraDevice.front.cameraDevice : CameraDevice.rear.cameraDevice
                default:return nil
                    
                }
            }
        }
        
    }
    enum SourceType {
        case photoLibrary
        case camera(CameraDevice)
        case savedPhotosAlbum
        
        var sourceType:UIImagePickerController.SourceType{
            switch self{
            case .photoLibrary: return .photoLibrary
            case .camera:       return .camera
            case .savedPhotosAlbum: return .savedPhotosAlbum
            }
        }
        var isSourceType:Bool{
            return UIImagePickerController.isSourceTypeAvailable(sourceType)
        }
        
    }
    enum CameraDevice : Int {
        case rear
        case front
        var cameraDevice:UIImagePickerController.CameraDevice{
            switch self {
            case .rear: return  .rear
            case .front: return .front
                
            }
        }
        var isCameraAvailable:Bool{
            return UIImagePickerController.isCameraDeviceAvailable(cameraDevice)
        }
        
    }
}




//MARK:-UIImagePickerController-

typealias UIImagePickerControllerBlock = (_ controller : UIImagePickerController?,_ result:UIImagePickerSource.UIPickerResult)->Void
typealias UIImagePickerMeidaBlock = (_ controller : UIImagePickerController?,_ result:UIImagePickerSource.UIPickerResult)->Void
class UIImagePickerKit: NSObject {
    fileprivate var pickerControllerHandler : UIImagePickerControllerBlock?
    class var shared:UIImagePickerKit{
        struct Singlton{
            static let instance = UIImagePickerKit()
        }
        return Singlton.instance
    }
    
    
    //MARK:- authorisationStatus
    fileprivate func authorisationStatus(sourceType:UIImagePickerController.SourceType,onCompletion:@escaping (_ authorrized:Bool)->Void){
        switch sourceType {
        case .camera:
            let cameraMediaType = AVMediaType.video
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
            switch cameraAuthorizationStatus {
            case .denied,.restricted:
                onCompletion(false)
            case .authorized:
                onCompletion(true)
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                    if granted {
                        onCompletion(true)
                        print("Granted access to \(cameraMediaType)")
                    } else {
                        onCompletion(false)
                        print("Denied access to \(cameraMediaType)")
                    }
                }
                
            @unknown default:
                fatalError()
            }
        default:
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                print("permission authorized")
                onCompletion(true)
            case .denied,.restricted:
                print("permission denied/restricted")
                onCompletion(false)
            case .notDetermined:
                print("Permission Not Determined")
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == PHAuthorizationStatus.authorized{
                        // photo library access given
                        onCompletion(true)
                    }else{
                        print("restriced manually")
                        onCompletion(false)
                    }
                })
                
            @unknown default:
                fatalError()
                
            }
        }
        
        
    }
    //MARK:- setPicker Info
    fileprivate func set(picker:UIImagePickerController,mediaType:UIImagePickerSource.Media = UIImagePickerSource.Media.photo(.photoLibrary)){
        if mediaType.isSourceTypeAvailable {
            picker.sourceType = mediaType.sourceType
            switch mediaType{
            case .photo(let source):
                if source.sourceType == .camera, let cameraDevice = mediaType.cameraDevice{
                    picker.cameraDevice = cameraDevice
                }
            case .video(let source):
                picker.mediaTypes = [kUTTypeMovie ,kUTTypeVideo].compactMap({$0 as String})
                if source.sourceType == .camera, let cameraDevice = mediaType.cameraDevice{
                    picker.cameraDevice = cameraDevice
                    picker.videoMaximumDuration = 45
                }
            }
        }
        
        
    }
    //MARK:- showImagePickerController
    func showImagePickerController(from viewController:UIViewController,mediaType:UIImagePickerSource.Media = UIImagePickerSource.Media.photo(.photoLibrary),allowsEditing:Bool = false, source:Any?,completionHandler handler:UIImagePickerControllerBlock?){
        self.pickerControllerHandler = handler
        authorisationStatus(sourceType: mediaType.sourceType) { (isAuthorized) in
            
            if !isAuthorized{
                let val = mediaType.isPhoto ?  "photos" : "videos"
                let message =  mediaType.isSourceTypeCamera ? "App does not have access to your \(val). To enable access, tap settings and turn on  \(val) Library Access." :"App does not have access to your camera. To enable access, tap settings and turn on Camera."
                viewController.showAlertAction( message: message, cancelTitle: "OK", otherTitle: "Setting", onCompletion: { (index) in
                    if index == 2{
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                                UIApplication.shared.openURL(settingsUrl)
                            }
                        }
                    }
                })
            }else{
                let picker : UIImagePickerController = UIImagePickerController()
                picker.allowsEditing = allowsEditing
                picker.delegate = self
                self.set(picker: picker, mediaType: mediaType)
                DispatchQueue.main.async {
                    if UIDevice.current.userInterfaceIdiom  == .pad {
                        if  let source = source {
                            picker.modalPresentationStyle = .popover
                            if let barButtonItem = source as? UIBarButtonItem {
                                
                                if let popoverController = picker.popoverPresentationController {
                                    popoverController.barButtonItem = barButtonItem
                                    let size  = viewController.view.bounds.size
                                    let ratio = size.width / size.height
                                    if size.width > size.height {
                                        let newHeight = size.width / ratio
                                        picker.preferredContentSize = CGSize(width: size.width, height: newHeight)
                                    }
                                    else{
                                        let newWidth = size.height * ratio
                                        picker.preferredContentSize = CGSize(width: newWidth, height: size.height)
                                    }
                                   // picker.preferredContentSize = CGSize(width: size.width-150, height: size.height - 190)
                                }
                                
                            }else{
                                if let popoverController = picker.popoverPresentationController {
                                    if let sourceView = source as? UIView{
                                        popoverController.sourceView = sourceView
                                        popoverController.sourceRect = sourceView.bounds
                                        let size  = viewController.view.bounds.size
                                        let ratio = size.width / size.height
                                        if size.width > size.height {
                                            let newHeight = size.width / ratio
                                            picker.preferredContentSize = CGSize(width: size.width, height: newHeight)
                                        }
                                        else{
                                            let newWidth = size.height * ratio
                                            picker.preferredContentSize = CGSize(width: newWidth, height: size.height)
                                        }
                                        //picker.preferredContentSize = CGSize(width: size.width - 150, height: size.height - 190)
                                    }
                                    
                                    
                                }
                                
                            }
                        }else{
                            picker.modalFromSheet()
                        }
                        
                    }
                    
                    
                    viewController.present(picker, animated: true, completion: nil)
                    
                    
                }
            }
        }
        
    }
    
    
    
    
    //MARK:- showPhotoPickedController
    func showPhotoPickedController(from viewController:UIViewController,sourceType:UIImagePickerSource.SourceType = .photoLibrary,allowsEditing:Bool,source:UIView? = nil,onCompletion:UIImagePickerMeidaBlock?){
        
        let mediaSource  = UIImagePickerSource.Media.photo(sourceType)
        self.showImagePickerController(from: viewController, mediaType: mediaSource, allowsEditing: allowsEditing, source: source, completionHandler: { (picker, result) in
            switch result{
            case .pickerInfo(let info):
                let key = allowsEditing ? UIImagePickerController.InfoKey.editedImage : UIImagePickerController.InfoKey.originalImage
                let image  = info[key] as? UIImage
                
                var asset: PHAsset?
                if #available(iOS 11.0, *) {
                    asset = info[.phAsset] as? PHAsset
                } else {
                    if let imageURL = info[.referenceURL] as? URL {
                        let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                        asset = result.firstObject
                    }
                }
                
                if let asset = asset{
                    guard let name = asset.value(forKey: "filename") else{return}
                    onCompletion?(picker, UIImagePickerSource.UIPickerResult.image(image: image, filename: "\(name)"))
                }else{
                    onCompletion?(picker, UIImagePickerSource.UIPickerResult.image(image: image, filename: nil))
                }
            case .cancelled:
                 onCompletion?(picker, result)
            default:break
                
                
            }
            
            
            
        })
        
        
    }
    //MARK:- showFilePickedController
    func showFilePickedController(from viewController:UIViewController,sourceType:UIImagePickerSource.SourceType = .photoLibrary,allowsEditing:Bool,source:UIView? = nil,onCompletion:UIImagePickerMeidaBlock?){
        let mediaSource  = UIImagePickerSource.Media.video(sourceType)
        self.showImagePickerController(from: viewController, mediaType: mediaSource, allowsEditing: allowsEditing, source: source, completionHandler: { (picker, result) in
            switch result{
            case .pickerInfo(let info):
                let type = info[UIImagePickerController.InfoKey.mediaType] as? String
                if type == kUTTypeVideo as String || type! == kUTTypeMovie as String{
                    if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL{
                        onCompletion?(picker, .file(url: videoURL, filename: videoURL.lastPathComponent))
                    }else{
                        onCompletion?(picker, .cancelled)
                    }
                }
            case .cancelled:
                onCompletion?(picker, result)
            default:break
                
            }
            
            
        })
    }
    
    
}

 extension UIImagePickerKit:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerControllerHandler?(picker,.cancelled)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pickerControllerHandler?(picker,.pickerInfo(info: info))
    }
    
    
}


class UIBottomSheet{
    enum BottomSheetOption:Int {
        case none
        case photoLibrary
        case photoCamera
        case videoLibrary
        case videoCamera
        
        var title:String{
            switch self {
            case .photoLibrary: return "Photo Gallery"
            case .photoCamera:  return "Photo Camera"
            case .videoLibrary: return "Video Library"
            case .videoCamera:  return "Video Camera"
            default:            return ""
            }
        }
        var icon:UIImage?{
            
            switch self {
            case .photoLibrary: return  #imageLiteral(resourceName: "ic_blue_gallery")
            case .photoCamera:  return  #imageLiteral(resourceName: "ic_photoCam")
                
            case .videoLibrary: return  nil
            case .videoCamera:  return  nil
            default:            return  nil
            }
        }
        
    }
    class var shared:UIBottomSheet{
        
        struct Singlton{
            
            static let instance = UIBottomSheet()
        }
        return Singlton.instance
    }
    //MARK: - showActionSheet
    fileprivate func showActionSheet(pickerOptions options:[BottomSheetOption] = [.photoLibrary,.photoCamera,.videoLibrary,.videoCamera],message:String,source:UIView? = nil,onCompletion:@escaping (Int,BottomSheetOption)->Void){
        guard let controller = currentController else {return}
        let titleFont  = UIFont.systemFont(ofSize:17, weight: .bold)
        let messageFont = UIFont.systemFont(ofSize: 14)
        let alertModel = AlertControllerModel(contentViewController: nil, title: kAppTitle, message: message, titleFont:titleFont , messageFont: messageFont, titleColor: JKColor.DeepIndigo, messageColor:.white, tintColor:  JKColor.DeepIndigo)
        let imageSize = CGSize(width: 25, height: 25)
        let cancel = AlertActionModel(actionIcon: nil, actionTitle: AlertActionTitle(title: "Cancel", titleColor: JKColor.DeepIndigo), style: .cancel)
        var actions = [AlertActionModel]()
        actions.append(cancel)
        
        for item in options {
            let icon  = item.icon
            let title = item.title
            let model = icon == nil ? AlertActionModel(actionTitle: AlertActionTitle(title: title, titleColor: JKColor.DeepIndigo), style: .default) : AlertActionModel(actionIcon:AlertActionIcon(image: icon!, imageColor: JKColor.DeepIndigo, imageSize: imageSize), actionTitle:AlertActionTitle(title: title, titleColor: JKColor.DeepIndigo), style: .default,alignmentMode:.left)
            actions.append(model)
        }
        
        _ = UIAlertController.showActionSheet(from: controller, controlModel: alertModel, actions: actions, source: source, alertActionHandler: { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            
            if index>1,let type = BottomSheetOption(rawValue: index-1){
                
                onCompletion(index,type)
            }else{
                onCompletion(index,.none)
            }
            
            
        })
        
        
    }
    
    //MARK:- presentImagePicker
    fileprivate func presentImagePicker(sourceType:UIImagePickerSource.SourceType = .photoLibrary,source:UIView? = nil,allowsEditing:Bool = true,OnPickerHandler:@escaping(_ result:UIImagePickerSource.UIPickerResult)->Void){
        guard let controller = currentController else {return}
        UIImagePickerKit.shared.showPhotoPickedController(from: controller, sourceType: sourceType, allowsEditing: allowsEditing,source:source, onCompletion: { (picker, result) in
            DispatchQueue.main.async {
                picker?.dismiss(animated: true, completion: {
                    OnPickerHandler(result)
                })
             
            }
            
        })
    }
    //MARK:- presentVideoPicker
    fileprivate func presentVideoPicker(sourceType:UIImagePickerSource.SourceType = .photoLibrary,source:UIView? = nil,allowsEditing:Bool = true,OnPickerHandler:@escaping(_ result:UIImagePickerSource.UIPickerResult)->Void){
        guard let controller = currentController else {return}
        UIImagePickerKit.shared.showFilePickedController(from: controller, sourceType: sourceType, allowsEditing: allowsEditing,source:source, onCompletion: { (picker, result) in
            DispatchQueue.main.async {
                picker?.dismiss(animated: true, completion: {
                    OnPickerHandler(result)
                })
                
            }
        })
        
    }
    //MARK:- showPicker
    func showPicker(pickerOptions options:[BottomSheetOption] = [.photoLibrary,.photoCamera,.videoLibrary,.videoCamera],message:String = "",source:UIView? = nil,allowsEditing:Bool = true,OnPickerHandler:@escaping(_ result:UIImagePickerSource.UIPickerResult)->Void){
        
        self.showActionSheet(pickerOptions: options,message: message,source:source) { (index,type) in
            if type == .photoLibrary || type == .photoCamera{
                let sourceType:UIImagePickerSource.SourceType = type == .photoCamera ? .camera(UIImagePickerSource.CameraDevice.front.isCameraAvailable ? .front:.rear) :  UIImagePickerSource.SourceType.savedPhotosAlbum.isSourceType ?  .savedPhotosAlbum : .photoLibrary
                self.presentImagePicker(sourceType: sourceType, source: source, allowsEditing: allowsEditing, OnPickerHandler: OnPickerHandler)
                
            }else if type == .videoLibrary || type == .videoCamera{
                let sourceType:UIImagePickerSource.SourceType = type == .videoCamera ?
                    .camera(UIImagePickerSource.CameraDevice.front.isCameraAvailable ? .front:.rear) : UIImagePickerSource.SourceType.savedPhotosAlbum.isSourceType ?  .savedPhotosAlbum : .photoLibrary
                self.presentVideoPicker(sourceType: sourceType, source: source,allowsEditing: allowsEditing, OnPickerHandler:OnPickerHandler)
            }
            
        }
    }
    
}
