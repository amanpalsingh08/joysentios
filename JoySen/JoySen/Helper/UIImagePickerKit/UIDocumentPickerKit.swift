//
//  UIDocumentPickerKit.swift
//  MOM
//
//  Created by Jitendra Kumar on 05/12/18.
//  Copyright © 2018 360itpro. All rights reserved.
//

import UIKit
//MARK:-UIImagePickerController-
typealias UIDocumentPickerData = (fileData:Data,fileUrl:URL,fileSize:UInt64)
typealias UIDocumentPickerViewControllerCancellationBlock = ()->Void
typealias UIDocumentPickerViewControllerFinalizationBlock = (_ result:Result<UIDocumentPickerData, Error>)->Void
enum UIDocumentPickerError:Error {
    case cancelled
    
}
class UIDocumentPickerKit: NSObject {

   fileprivate var pickerCancellationBlock :  UIDocumentPickerViewControllerCancellationBlock?
   fileprivate var pickerFinalizationBlock : UIDocumentPickerViewControllerFinalizationBlock?
   fileprivate (set) var controller :UIViewController!
  
    
    class var shared:UIDocumentPickerKit{
        
        struct Singlton{
            static let instance = UIDocumentPickerKit()
        }
        return Singlton.instance
    }
    
    func showDocumentPicker(from viewController:UIViewController, source:UIView?,OnFinalizationBlock finishBlock:UIDocumentPickerViewControllerFinalizationBlock?,OnCancellationBlock CancelledBlock:UIDocumentPickerViewControllerCancellationBlock?){
        
       // AppDelegate.shared.setNavigationBar(#colorLiteral(red: 0, green: 0.7093494534, blue: 0.884814918, alpha: 1), tintColor: .white)
        controller = viewController
        pickerCancellationBlock = CancelledBlock
        pickerFinalizationBlock = finishBlock
        let documentTypes:[String] = [UTI.commaSeparatedText].compactMap({$0.rawValue})
        let picker = UIDocumentPickerViewController(documentTypes: documentTypes, in: .import)
        picker.delegate = self
    
        
        DispatchQueue.main.async {
            if UIDevice.current.userInterfaceIdiom  == .pad {
                if  let source = source {
                    picker.modalPresentationStyle = .popover
                    if let barButtonItem = source as? UIBarButtonItem {
                        
                        if let popoverController = picker.popoverPresentationController {
                            popoverController.barButtonItem = barButtonItem
                            popoverController.sourceRect = source.bounds
                            let size  = viewController.view.bounds.size
                            picker.preferredContentSize = CGSize(width: size.width - 150, height: size.height - 190)
                        }
                        
                    }else{
                        if let popoverController = picker.popoverPresentationController {
                            
                            popoverController.sourceView = source
                            popoverController.sourceRect = source.bounds
                            let size  = viewController.view.bounds.size
                            picker.preferredContentSize = CGSize(width: size.width - 150, height: size.height - 190)
                            
                        }
                        
                    }
                }else{
                    picker.modalFromSheet()
                }
                
            }
            
            viewController.present(picker, animated: true, completion: {
            
            })
            
            
        }
    }
    
}
//MARK:-UIDocumentPickerDelegate-
extension UIDocumentPickerKit:UIDocumentPickerDelegate,UINavigationControllerDelegate{
    
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
            
        }
        
        let cico = url as URL
        print("The Url is : \(cico)")
        //    downloadeDataFromeURL(url: cico)
        let coordinator = NSFileCoordinator(filePresenter: nil)
        let error: Error? = nil
        coordinator.coordinate(readingItemAt: url, options: NSFileCoordinator.ReadingOptions(rawValue: 0), error: error as! NSErrorPointer, byAccessor: {(_ newURL: URL) -> Void in
            //   var data = Data(contentsOf: newURL)
            
            do {
                let data = try Data(contentsOf: newURL)
                let fileSize: UInt64 = UInt64(data.count)
                    DispatchQueue.main.async {
                        //self.composeMessage(type: .document, content: url)
                        print("file saved")
                        if let handler = self.pickerFinalizationBlock {
                            //handler(.success(data,newURL,fileSize))
                           let pickerData  = UIDocumentPickerData(fileData: data, fileUrl: newURL, fileSize: fileSize)
                            handler(.success(pickerData))
                        }
                        
                    }
               
                
            } catch let error as NSError {
                print(error.localizedDescription)
                if let handler = self.pickerFinalizationBlock {
                    handler(.failure(error))
                }
            }
            // Do something
        })
        if error != nil {
            // Do something else
            print("file ERROR")
            if let handler = pickerFinalizationBlock {
                handler(.failure(error!))
            }
            
        }
        //optional, case PDF -> render
        //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
        
        
        
        
    }
       func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        print("we cancelled")
        // self.showAlert(alertMessage: "Either File size too large. or ")
        controller.dismiss(animated: true, completion: nil)
        if let handler = pickerCancellationBlock {
            handler()
        }
        
    }
    
    
}
