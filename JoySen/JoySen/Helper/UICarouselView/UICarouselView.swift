//
//  UICarouselView.swift
//  JoySen
//
//  Created by Jitendra Kumar on 02/05/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class UICarouselView: UIView {
    fileprivate var currentPage:Int = 0
    fileprivate var timer:Timer? = nil
    fileprivate var imageUrls:[String] = []
    fileprivate var pageCount:Int{
        return imageUrls.count
    }
    var isAllMoved:Bool = false
    lazy var flowlayout:UICollectionViewFlowLayout = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        flowlayout.minimumLineSpacing = 0.0
        flowlayout.minimumInteritemSpacing = 0.0
        return flowlayout
    }()
    fileprivate lazy var collectionView:UICollectionView = {
        
        let collection  = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        collection.isPagingEnabled = true
        collection.showsHorizontalScrollIndicator = false
        collection.showsVerticalScrollIndicator = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = .clear
        
        return collection
    }()
    private lazy var previousBtn:JKButton = {
        let button = JKButton()
        button.normalImage = UIImage(systemName: "chevron.left")
        button.setTitleColor(.white, for: .normal)
        button.tintColor = .white
        button.isHidden = false
        button.isEnabled = false
        button.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var nextBtn:JKButton = {
        let button = JKButton()
        button.normalImage = UIImage(systemName: "chevron.right")
        button.setTitleColor(.white, for: .normal)
        button.tintColor = .white
        button.isHidden = false
        button.isEnabled = false
        button.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubViews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        addSubViews()
    }
    //MARK:- Private Methods
    private func addSubViews(){
        self.removeAll()
        flowlayout.itemSize = self.bounds.size
        
        collectionView.dataSource = self
        collectionView.delegate = self
        nextBtn.addTarget(self, action: #selector(self.didChangeItem), for: .touchUpInside)
        previousBtn.addTarget(self, action: #selector(self.didChangeItem), for: .touchUpInside)
        add(collectionView,previousBtn,nextBtn)
        collectionView.fillSuperview()
        
        previousBtn.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        previousBtn.centerYToSuperview()
        previousBtn.constrainHeight(35)
        previousBtn.constrainWidth(35)
        
        nextBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        nextBtn.centerYToSuperview()
        nextBtn.constrainHeight(35)
        nextBtn.constrainWidth(35)
        collectionView.register(UICarouselItemCell.self)
        
        
    }
    
    private func buttonEanbled(){
        self.previousBtn.isEnabled = self.currentPage>0 ? true : false
        self.nextBtn.isEnabled = self.currentPage<self.pageCount-1 ? true:false
    }
    private func addTimer(){
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 8, repeats: true, block: { (timer) in
                if timer.isValid{
                    self.didChangeItem()
                }
            })
        }
    }
    
    
    @objc private func didChangeItem(){
        guard pageCount>1 else {return}
        if isAllMoved == false,currentPage<pageCount-1{
            currentPage+=1
            let indexPath = IndexPath(row: currentPage, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            if currentPage == pageCount-1{
                isAllMoved = true
            }
        }else if isAllMoved, currentPage>0{
            currentPage -= 1
            let indexPath = IndexPath(row: currentPage, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            if currentPage == 0 {
                isAllMoved = false
            }
        }
        buttonEanbled()
    }
    //MARK:- Public Methods
    
    func setImages(urls:[String]){
        async {
            self.imageUrls.removeAll()
            self.imageUrls = urls
            self.currentPage = 0
            self.stop()
            self.collectionView.reloadData()
            if self.pageCount>1 {
                self.previousBtn.isHidden = false
                self.nextBtn.isHidden = false
                self.buttonEanbled()
                self.addTimer()
            }
            
        }
    }
    func stop(){
        if timer != nil{
            timer?.invalidate()
            timer = nil
            currentPage = 0
            self.previousBtn.isHidden = true
            self.nextBtn.isHidden = true
        }
    }
    
}
extension UICarouselView:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pageCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeue(UICarouselItemCell.self, for: indexPath)
        cell.imageUrl = imageUrls[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size//UICollectionViewFlowLayout.automaticSize
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
class UICarouselItemCell: UICollectionViewCell {
    private lazy var imageView:JKImageView = {
        let view  = JKImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBound = true
        view.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addSubViews()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubViews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        addSubViews()
        
    }
    
    func addSubViews(){
        contentView.removeAll()
        contentView.addSubview(imageView)
        imageView.fillSuperview()
      
    }
    var imageUrl:String?{
        didSet{
            if let file = imageUrl {
                imageView.loadImage(filePath: file)
            }
        }
    }
    
    
}
