//
//  JKSegmentedControl.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/08/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit
@IBDesignable
class JKSegmentedControl: UISegmentedControl {
    
    @IBInspectable
    var textColor:UIColor = .black{
        didSet{
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var selectedTextColor:UIColor = .black{
        didSet{
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var fontSize:CGFloat = 14{
        didSet{
            setNeedsDisplay()
            
        }
        
    }
    open override func layoutSubviews() {
        super.layoutSubviews()
        let selected:[NSAttributedString.Key:Any] = [.foregroundColor : selectedTextColor,.font:UIFont.systemFont(ofSize: fontSize)]
        let normal:[NSAttributedString.Key:Any] = [.foregroundColor : textColor,.font:UIFont.systemFont(ofSize: fontSize)]
        
        self.setTitleTextAttributes(selected, for: .selected)
        self.setTitleTextAttributes(normal, for: .normal)
      
        
        
    }
}
