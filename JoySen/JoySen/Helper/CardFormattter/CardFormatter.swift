//
//  CardFormatter.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit
//Detecting card type
enum CardType: String {
    case Unknown, Amex, Visa, MasterCard,MestroCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, MestroCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MestroCard:
            return "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
    var icon:UIImage?{
        switch self {
        case .Amex:
            return #imageLiteral(resourceName: "ic_amex")
        case .Visa:
            return #imageLiteral(resourceName: "ic_visa")
        case .MasterCard:
            return #imageLiteral(resourceName: "ic_masterCard")
        case .MestroCard:
            return #imageLiteral(resourceName: "ic_mestro")
        case .Diners:
            return #imageLiteral(resourceName: "ic_diners")
        case .Discover:
            return #imageLiteral(resourceName: "ic_discover")
        case .JCB:
            return #imageLiteral(resourceName: "ic_jcb")
        case .UnionPay:
            return #imageLiteral(resourceName: "ic_unionPay")
        case .Hipercard:
            return #imageLiteral(resourceName: "ic_hipercard")
        case .Elo:
            return #imageLiteral(resourceName: "ic_elo")
        default:
            return #imageLiteral(resourceName: "ic_crdit_card")
        }
    }
}
extension String{
    func checkCardNumber() -> (type: CardType, formatted: String, valid: Bool) {
        // Get only numbers from the input string
        let input = self
        guard !input.isEmpty  else {return(.Unknown, "", false)}
               let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)//input.stringByReplacingOccurrencesOfString("[^0-9]", withString: "", options: .RegularExpressionSearch)
               
        
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        let characters = numberOnly.compactMap({$0})
        // format
        var formatted4 = ""
        for character in characters {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, formatted, valid)
    }

    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
   // Validating a card
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        
        let digitStrings = number.reversed().map({String($0)})
        
        for (index,element) in digitStrings.enumerated() {
            guard let digit = Int(element) else { return false }
            let odd = index % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
}
