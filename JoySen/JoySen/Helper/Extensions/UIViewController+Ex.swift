//
//  UIViewController.swift
//  Pivot
//
//  Created by Jitendra Kumar on 20/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit


//MARK: - UIViewController Extension -
extension UIViewController{
    static var storyboardID:String{
        return String(describing: self)
    }
    static func instance(from storyboard:AppStoryboard)->Self{
        return storyboard.viewController(viewController: self)
    }
    static func instance(from storyboard:AppStoryboard,withIdentifier identifier :String)->Self{
        return storyboard.viewController(withIdentifier: identifier)
    }
    func modalTransitionStyle(_ style:UIModalTransitionStyle){
        self.modalTransitionStyle = style
    }
    func modalPresentationStyle(_ style:UIModalPresentationStyle){
        self.modalPresentationStyle = style
    }
    //MARK: - modalPresentation
    func modalPresentation(){
        self.modalTransitionStyle(.crossDissolve)
        self.modalPresentationStyle(.overCurrentContext)
    }
    //MARK: - modalPresentation
    func popoverPresentation(){
        self.modalTransitionStyle(.crossDissolve)
        self.modalPresentationStyle(.popover)
    }
    //MARK: - modalFromSheet
    func modalFromSheet(){
        self.modalTransitionStyle(.crossDissolve)
        self.modalPresentationStyle(.formSheet)
    }
    
    
    //MARK: - zoomBounceAnimation
    func zoomBounceAnimation(containtView popUp:UIView){
        popUp.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.3/1.5, animations: {
            popUp.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (finished) in
            UIView.animate(withDuration: 0.3/2, animations: {
                popUp.transform = CGAffineTransform(scaleX:  0.9, y:  0.9)
            }) { (finished) in
                UIView.animate(withDuration: 0.3/2, animations: {
                    popUp.transform = .identity
                })
            }
        }
        
    }
    //MARK: - fadeAnimationController -
    func fadeAnimationController(duration:TimeInterval = 0.5,completion: ((Bool) -> Swift.Void)? = nil ){
        self.view.alpha = 0
        UIView.transition(with: self.view, duration: duration, options: .transitionCrossDissolve, animations: {
            
            self.view.alpha = 1.0
            if (completion != nil) {
                completion!(true)
            }
        }) { (finished:Bool) in
            
        }
    }
    
    //MARK: - showLogoutAlert -
    func showLogoutAlert(title:String = kAppTitle,message:String = "Are you sure you want to logout?", completion: (() -> Swift.Void)? = nil){
        
        self.showAlertAction(title: title, message: message, cancelTitle: "NO", otherTitle: "YES") { (buttonIndex) in
            switch buttonIndex {
            case 2:
                
                if (completion != nil){
                    completion!()
                }
                break
                
            default:
                break
            }
        }
        
    }
    //MARK:- showAlert-
    func showAlertAction(title:String = kAppTitle,message:String?,cancelTitle:String = "Cancel",otherTitle:String = "OK",onCompletion:@escaping (_ didSelectIndex:Int)->Void){
        self.alertControl(title: title, message: message, cancelTitle: cancelTitle, otherTitle: otherTitle, onCompletion: onCompletion)
    }
    
    //MARK:- showAlert-
    func showAlert(title:String = kAppTitle,message:String?,completion:((_ didSelectIndex:Int)->Swift.Void)? = nil){
        
        self.alertControl(title: title, message: message, cancelTitle: "OK", otherTitle: nil, onCompletion: completion)
    }
    
    fileprivate func alertControl(title:String = kAppTitle,message:String?,cancelTitle:String = "OK",otherTitle:String?,onCompletion:((_ didSelectIndex:Int)->Swift.Void)? = nil){
        let titleFont  = UIFont.systemFont(ofSize: 20, weight: .semibold)
        let messageFont  = UIFont.systemFont(ofSize:  17)
        let msge  = (message != nil && !message!.isEmpty) ? "\n\(message!)" : nil
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: msge, titleFont: titleFont, messageFont: messageFont, titleColor: JKColor.DarkBlue, messageColor: .darkGray, tintColor: JKColor.DarkBlue)
        var actions:[AlertActionModel] = [AlertActionModel]()
        
        let alertActionTitle = AlertActionTitle(title: cancelTitle, titleColor: JKColor.DarkBlue)
        let cancel = AlertActionModel(actionIcon: nil, actionTitle: alertActionTitle, style: .cancel)
        actions.append(cancel)
        if let otherTitle = otherTitle {
            let alertActionTitle = AlertActionTitle(title: otherTitle, titleColor: JKColor.DarkBlue)
            let other = AlertActionModel(actionIcon: nil, actionTitle:alertActionTitle, style: .default)
            actions.append(other)
        }
        
        _ = UIAlertController.showAlert(from: self, controlModel: alertModel, actions: actions) { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            if let handler = onCompletion {
                handler(index)
            }
            
        }
    }
    func showTitleActionSheet(message:String?,cancelTitle:String = "OK",otherTitles:[String] ,source:UIView? = nil,onCompletion:@escaping (Int)->Void){
        var actions:[AlertActionModel] = []
        for  otherTitle in otherTitles {
            let alertActionTitle = AlertActionTitle(title: otherTitle, titleColor: JKColor.DarkBlue)
            let other = AlertActionModel(actionIcon: nil, actionTitle:alertActionTitle, style: .default)
            actions.append(other)
        }
        self.showActionSheet(message: message,cancelTitle:cancelTitle, buttons: actions, source: source, onCompletion: onCompletion)
    }
    
    func showActionSheet(title:String = kAppTitle,message:String?,cancelTitle:String = "Cancel",buttons:[AlertActionModel] ,source:UIView? = nil,onCompletion:@escaping (Int)->Void){
        guard let controller = currentController else {return}
        let titleFont  = UIFont.systemFont(ofSize:20, weight: .semibold)
        let messageFont = UIFont.systemFont(ofSize: 17)
        let msge  = (message != nil && !message!.isEmpty) ? "\n\(message!)" : nil
        
        let alertModel = AlertControllerModel(contentViewController: nil, title: title, message: msge, titleFont:titleFont , messageFont: messageFont, titleColor: JKColor.DarkBlue, messageColor:.darkGray, tintColor:  JKColor.DarkBlue)
        let cancel = AlertActionModel(actionIcon: nil, actionTitle: AlertActionTitle(title: cancelTitle, titleColor: JKColor.DarkBlue), style: .cancel)
        var actions : [AlertActionModel] = [cancel]
        actions.append(contentsOf: buttons)
        
        _ = UIAlertController.showActionSheet(from: controller, controlModel: alertModel, actions: actions, source: source, alertActionHandler: { (alert:UIAlertController, action:UIAlertAction, index:Int) in
            onCompletion(index)
            
            
            
        })
        
        
    }

    @IBAction func onClickMenu(_ sender:Any){
       
        self.toggleLeft()
    }
    
    func multiLineTitle(){
        guard let navigationController = navigationController else { return  }
        for navItem in navigationController.navigationBar.subviews {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    largeLabel.text = self.title
                    largeLabel.numberOfLines = 0
                    largeLabel.lineBreakMode = .byWordWrapping
                }
            }
        }
        navigationController.navigationBar.layoutSubviews()
        navigationController.navigationBar.layoutIfNeeded()
        
    }
    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }
}
