//
//  Date+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 20/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import Foundation


//MARK:- EXTENSION FOR DATE
extension Date {
    
    func displayStyle(dateFormatterStyle style:DateFormatterStyle)->String{
        return style.result(date: self)
    }
    
    var timeDisplay:String{
        get{
            let secondAngle = Int(Date().timeIntervalSince(self))
            let minute = 60
            let hour = 60*minute
            let day  = 24*hour
            let week = 7*day
            let month = 4*week
            let year = 12*month
            let quatient:Int
            let unit:String
            if secondAngle<minute {
                quatient = secondAngle
                unit = "second"
            }else if secondAngle<hour {
                quatient = secondAngle/minute
                unit = "min"
            }else if secondAngle<day {
                quatient = secondAngle/hour
                unit = "hour"
            }else if secondAngle<week {
                quatient = secondAngle/day
                unit = "day"
            }else if secondAngle<month {
                quatient = secondAngle/week
                unit = "week"
            }else  if secondAngle<year {
                quatient = secondAngle/month
                unit = "month"
            }else{
                quatient = secondAngle/year
                unit = "year"
            }
            return "\(quatient) \(unit)\(quatient == 1 ? "" :"s") ago"
        }
    }
    
    
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
    var next30days:Date{
        let today = Date()
        return Calendar.current.date(byAdding: .day, value: 30, to: today)!
    }
    
    // convert Date to string date
    func dateToString(timeZoneType type:TimeZoneType = .utc,formater:String = "yyyy-MM-dd HH:mm:ss") -> String{
        return DateFormatterType.string(timeZoneType: type, formater: formater).result(date: self) as? String ?? ""
        
    }
    
    func dateToDate(timeZoneType type:TimeZoneType = .utc,formater:String = "yyyy-MM-dd HH:mm:ss") -> Date?{
        return DateFormatterType.date(timeZoneType: type, formater: formater).result(date: self) as? Date
        
    }
    
    func addDays(_ daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd * 60 * 60 * 24)
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(_ hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd * 60 * 60)
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        //Return Result
        return dateWithHoursAdded
    }
    
}
enum TimeZoneType:CustomStringConvertible {
    var description: String{
        switch self {
        case .current:
            return "phone_local TimeZone"
        case .utc:
            return "UTC TimeZone"
        case .gmt:
            return "GMT TimeZone"
        default:
            return "Custom TimeZone"
        }
    }
    
    case current
    case utc
    case gmt
    case custom(String)
    var timeZone:TimeZone?{
        switch self {
        case .current:
            return TimeZone.current
        case .utc:
            return TimeZone(abbreviation: "UTC")
        case .gmt:
            return TimeZone(abbreviation: "GMT")
        case .custom(let abbreviation):
            return TimeZone(abbreviation: abbreviation)
            
        }
        
    }
}
enum LocaleType:CustomStringConvertible {
    case current
    case autoupdatingCurrent
    case en_US
    case en_US_POSIX
    case custom(String)
    var locale:Locale{
        switch self {
        case .current: return Locale.current
        case .autoupdatingCurrent:return Locale.autoupdatingCurrent
        case .en_US: return Locale(identifier: "en_US")
        case .en_US_POSIX: return Locale(identifier: "en_US_POSIX")
        case .custom(let abbreviation):return Locale(identifier: abbreviation)
        }
    }
    var description: String{
        switch self {
        case .current:
            return "current Local"
        case .autoupdatingCurrent:
            return "autoupdatingCurrent Local"
        case .en_US:
            return "en_US Local"
        case .en_US_POSIX:
            return "en_US_POSIX Local"
        default:
            return "Custom Local"
        }
    }
}

enum TimeZoneDateFormatter {
    struct PTDateFormatter {
        var timeZoneType :TimeZoneType?
        var formater:String
        var locateType:LocaleType?
        
        init(timeZoneType :TimeZoneType? = nil,formater:String, locateType:LocaleType? = nil) {
            self.timeZoneType = timeZoneType
            self.formater = formater
            self.locateType = locateType
        }
    }
    
    case dateFormatter(serverFormat:PTDateFormatter,localFormat:PTDateFormatter)
    func result(dateString:String)->(date:Date?,dateTimeStr:String?){
        if dateString.isEmpty {
            return (nil,nil)
        }
        switch self {
        case .dateFormatter(let serverFormat, let localFormat):
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = serverFormat.formater
            if let timeZoneType = serverFormat.timeZoneType{
                 dateFormatter.timeZone = timeZoneType.timeZone
            }
            if let locateType = serverFormat.locateType{
                dateFormatter.locale = locateType.locale
            }

            if let date = dateFormatter.date(from: dateString){ // create date from string
                // change to a readable time format and change to local time zone
                dateFormatter.dateFormat = localFormat.formater
                if let timeZoneType = localFormat.timeZoneType{
                    dateFormatter.timeZone = timeZoneType.timeZone
                }
                if let locateType = localFormat.locateType{
                    dateFormatter.locale = locateType.locale
                }
                let dateStamp = dateFormatter.string(from: date)
                return (date,dateStamp)
            }else{
                return (nil,nil)
            }
        
            
        }
        
        
    }
}
enum DateFormatterType{
    
    case string(timeZoneType :TimeZoneType,formater:String)
    case date(timeZoneType :TimeZoneType,formater:String)
    func result(date:Date)->Any?{
        switch self {
        case .string(let timeZoneType,let formater):
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = formater
            dateFormatter.timeZone = timeZoneType.timeZone
            let dateStamp = dateFormatter.string(from: date)
            return dateStamp
        case .date(let timeZoneType,let formater):
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = formater
            dateFormatter.timeZone = timeZoneType.timeZone
            let dateStamp = dateFormatter.string(from: date)
            let newdate = dateFormatter.date(from: dateStamp)
            return newdate
        }
    }
}
enum DateFormatterStyle {
    case timeDisplay(timeStyle:DateFormatter.Style)
    case dateDisplay(dateStyle:DateFormatter.Style)
    case dateTimeDisplay(timeStyle :DateFormatter.Style,dateStyle:DateFormatter.Style)
    
    static var formatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = LocaleType.en_US.locale
        //formatter.setLocalizedDateFormatFromTemplate(T##dateFormatTemplate: String##String)
        return formatter
    }()
    func result(date:Date)->String{
        switch self {
        case .timeDisplay(let timeStyle):
            if timeStyle == .none  {
                return ""
            }
            DateFormatterStyle.formatter.dateStyle = .none
            DateFormatterStyle.formatter.timeStyle = timeStyle
            return DateFormatterStyle.formatter.string(from: date)
        case .dateDisplay(let dateStyle):
            if dateStyle == .none  {
                return ""
            }
            DateFormatterStyle.formatter.timeStyle = .none
            DateFormatterStyle.formatter.dateStyle = dateStyle
            return DateFormatterStyle.formatter.string(from: date)
        case .dateTimeDisplay(let timeStyle, let dateStyle):
            if timeStyle == .none || dateStyle == .none {
                return ""
            }
            DateFormatterStyle.formatter.timeStyle = timeStyle
            DateFormatterStyle.formatter.dateStyle = dateStyle
            return DateFormatterStyle.formatter.string(from: date)
            
        }
        
    }
    
}

extension Date{
        /// Returns a Boolean value indicating whether the value of the first
        /// argument is equal to that of the second argument.
        ///
        /// - Parameters:
        ///   - lhs: A value to compare.
        ///   - rhs: Another value to compare.
       static func ==(lhs:Date, rhs: Date) -> Bool {
        return lhs.compare(rhs) == .orderedSame ? true : false
        }
        /// Returns a Boolean value indicating whether the value of the first
        /// argument is greater than to that of the second argument.
        ///
        /// - Parameters:
        ///   - lhs: A value to compare.
        ///   - rhs: Another value to compare.
       static func >(lhs:Date, rhs: Date) -> Bool {
            return lhs.compare(rhs) == .orderedDescending
        }
        /// Returns a Boolean value indicating whether the value of the first
        /// argument is greater than or equal to that of the second argument.
        ///
        /// - Parameters:
        ///   - lhs: A value to compare.
        ///   - rhs: Another value to compare.
        static func >=(lhs:Date,rhs: Date) -> Bool {
            return (lhs.compare(rhs) == .orderedDescending || lhs.compare(rhs) == .orderedSame)
        }
        /// Returns a Boolean value indicating whether the value of the first
        /// argument is less than to that of the second argument.
        ///
        /// - Parameters:
        ///   - lhs: A value to compare.
        ///   - rhs: Another value to compare.
       static func <(lhs:Date, rhs: Date) -> Bool {
            return lhs.compare(rhs) == .orderedAscending
        }
        /// Returns a Boolean value indicating whether the value of the first
        /// argument is less than or equal to that of the second argument.
        ///
        /// - Parameters:
        ///   - lhs: A value to compare.
        ///   - rhs: Another value to compare.
       static func <=(lhs:Date, rhs: Date) -> Bool {
            return (lhs.compare(rhs) == .orderedAscending || lhs.compare(rhs) == .orderedSame)
        }
    
    

  
}


enum Compare<Value> where Value:Comparable {
    case equal
    case less
    case lessOrEqual
    case greater
    case greaterOrEqual
    func result(lhs:Value,rhs:Value)->Bool{
        switch self {
        case .equal: return lhs == rhs
        case .less: return lhs<rhs
        case .greater: return lhs>rhs
        case .lessOrEqual: return lhs<=rhs
        case .greaterOrEqual: return lhs>=rhs
            
        }
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

extension Formatter {
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return formatter
    }()
    static let iso8601NoSecond: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.formatOptions = [.withInternetDateTime]
        return formatter
    }()
}
