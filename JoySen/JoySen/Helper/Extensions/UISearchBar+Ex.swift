//
//  UISearchBar+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 22/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import UIKit
//MARK: - UISearchBar Extension
@IBDesignable
extension UISearchBar{
   
    @IBInspectable var textColor:UIColor?{
        set{
            searchTextField?.textColor = (newValue == nil) ? .black : newValue
            self.setNeedsDisplay()
        }
        get{
            return searchTextField?.textColor
        }
    }
    @IBInspectable var searchIcon:UIImage?{
        set{
            if let icon = newValue {
                setImage(icon, for: .search, state: .normal)
            }
            self.setNeedsDisplay()
        }
        get{
            return image(for: .search, state: .normal)
        }
    }
    @IBInspectable var clearIcon:UIImage?{
        set{
            if let icon = newValue {
                setImage(icon, for: .clear, state: .normal)
            }
            self.setNeedsDisplay()
        }
        get{
            return image(for: .clear, state:.normal)
        }
    }

    
    @IBInspectable var bookmarkIcon:UIImage?{
        set{
            if let icon = newValue {
                setImage(icon, for: .bookmark, state: .normal)
            }
            self.setNeedsDisplay()
        }
        get{
            return image(for: .bookmark, state:.normal)
        }
    }
    @IBInspectable var resultsIcon:UIImage?{
        set{
            if let icon = newValue {
                setImage(icon, for: .resultsList, state: .normal)
            }
            self.setNeedsDisplay()
        }
        get{
            return image(for: .resultsList, state:.normal)
        }
    }
    @IBInspectable var placeholderColor:UIColor?{
        set{
            if let color = newValue {
               placeholderLabel?.textColor = color
            }
            self.setNeedsDisplay()
        }
        get{
            return placeholderLabel?.textColor
        }
    }
   
    func setTextFieldColor(color: UIColor) {
        
        if let textField = searchTextField {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
            default:
                textField.backgroundColor = color
                
                
            }
            self.setNeedsDisplay()
        }
    }
    func setTextColor(color: UIColor) {
        if let textField = searchTextField {
            textField.textColor = color
            self.setNeedsDisplay()
        }
    }
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    fileprivate var searchTextField:UITextField?{
        return getViewElement(type: UITextField.self) ?? (value(forKey: "searchField") as? UITextField)
    }
    fileprivate var placeholderLabel:UILabel?{
        return searchTextField?.value(forKey: "placeholderLabel") as? UILabel
    }
     // UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    
}


