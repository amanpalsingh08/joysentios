//
//  UINavigationBar+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 01/11/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationController{
    
    func setBarColor(_ barTintColor:UIColor?, tintColor:UIColor? = nil,barTitleColor titleColor:UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),isTransparent:Bool = false){
        self.navigationBar.configBar(barTintColor,tintColor: tintColor,barTitleColor:titleColor, isTransparent: isTransparent)
        
    }
    
}
extension UINavigationBar {
    
    func configBar(_ barTintColor:UIColor? = nil, tintColor:UIColor? ,barTitleColor titleColor:UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),isTransparent:Bool = false){
        if #available(iOS 13.0, *) {
            self.setBarAppearance(color: barTintColor, isTransparent: isTransparent)
            
        } else {
            
            
            if self.prefersLargeTitles {
                self.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            }else{
                self.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            }
            self.backIndicatorImage = #imageLiteral(resourceName: "ic_back")
            self.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "ic_back")
            
        }
        self.isTranslucent = isTransparent
        if let tintColor = tintColor {
            self.tintColor = tintColor
        }
        if barTintColor != nil {
            self.barTintColor = barTintColor
        }
        
        if isTransparent {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
        }
        
    }
    func setBarAppearance(color:UIColor? = nil,barTitleColor titleColor:UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),isTransparent:Bool = false){
        if #available(iOS 13.0, *) {
            let barAppearance = UINavigationBarAppearance()
            barAppearance.configureWithOpaqueBackground()
            
            if self.prefersLargeTitles {
                barAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : titleColor]
            }else{
                barAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor : titleColor]
            }
            
            barAppearance.backgroundColor = color
            self.standardAppearance = barAppearance
            self.compactAppearance = barAppearance
            self.scrollEdgeAppearance = barAppearance
            barAppearance.setBackIndicatorImage(#imageLiteral(resourceName: "ic_back"), transitionMaskImage: #imageLiteral(resourceName: "ic_back"))
            self.backgroundColor = color
            
            
        }
    }
}
