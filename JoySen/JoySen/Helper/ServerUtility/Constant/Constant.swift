//
//  Constant.swift
//  StableGuard
//
//  Created by Jitendra Kumar on 24/08/18.
//  Copyright © 2018 Jitendra Kumar. All rights reserved.

import Foundation
import UIKit

func async(onCompletion:@escaping()->Void){
    DispatchQueue.main.async {
        onCompletion()
    }
    
}
func asyncExecute(onCompletion:@escaping()->Void){
    DispatchQueue.main.async(execute: {
        onCompletion()
    })
}
var sceneDelegate:SceneDelegate?{
    guard let window = currentController?.view.window else { return nil }
    return window.windowScene?.delegate as? SceneDelegate
}
var rootController:UIViewController?{
    if #available(iOS 13, *) {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    return AppDelegate.shared.window?.rootViewController
}
var currentController:UIViewController?{
    
    if let navController  =  rootController as? UINavigationController {
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return navController
        }
    }else if let sideController  =  rootController as? JKSlideMenuController{
        
        if  let navController = sideController.mainViewController as? UINavigationController{
            if  let visibleViewController = navController.visibleViewController{
                return visibleViewController
            }else{
                return sideController
            }
        }else if let  tabBarController = sideController.mainViewController as? UITabBarController,let navController = tabBarController.selectedViewController as? UINavigationController{
            if  let visibleViewController = navController.visibleViewController{
                return visibleViewController
            }else{
                return sideController
            }
        }
        
    }else if let tabBarController  =  rootController as? UITabBarController, let navController = tabBarController.selectedViewController as? UINavigationController{
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return tabBarController
        }
    }
    return nil
}

var currentAlert:UIViewController?{
    
    if let controller  =  currentController as? UINavigationController{
        if  let visibleViewController = controller.visibleViewController{
            if let currentAlert = visibleViewController.presentedViewController as? UIAlertController{
                return currentAlert
            }else if let currentAlert = visibleViewController as? UIAlertController {
                return currentAlert
            }else{
                return visibleViewController
            }
            
        }else{
            if let currentAlert = controller.presentedViewController as? UIAlertController{
                return currentAlert
                
            }else{
                return controller
            }
        }
    }else if let controller  = currentController  {
        if let currentAlert = controller.presentedViewController as? UIAlertController{
            return currentAlert
            
        }else{
            return controller
        }
    }else{
        return nil
    }
}
var alertMessage: String? {
    didSet{
        async {
            guard let controller  =  currentAlert else {return}
            
            if let alertController = controller as? UIAlertController{
                let messageFont  =  UIFont.systemFont(ofSize: 17)
                alertController.set(message: alertMessage, font: messageFont, color: .white)
            }else{
                controller.showAlert(message: alertMessage)
            }
            
        }
    }
}
func AppSettingAlert(title:String,message:String?){
    async {
        guard let controller  =  currentAlert else {return}
        if let alertController = controller as? UIAlertController{
            let messageFont  =  UIFont.systemFont(ofSize: 17)
            alertController.set(message: alertMessage, font: messageFont, color: .white)
        }else{
            controller.showAlertAction(title: title, message: message, cancelTitle: "OK", otherTitle: "Settings") { (index) in
                if index == 2{
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        
                    }
                    else{
                        UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
                    }
                }
            }
        }
        
    }
}
var authID:Int?{
    set{
        guard let val = newValue else { return  }
        UserDefaults.standard[kUserIDKey] = val
    }
    get{
        return UserDefaults.standard[kUserIDKey]
    }
}

var userModel:JSUserModel?{
    set{
        guard let  model = newValue else { return }
        authID = model.userID
        UserDefaults.set(encoder: model, forKey: kUserDataKey)
        // UserDefaults.set(archivedObject: model, forKey: kUserDataKey)
    }
    get{
        
        guard let model =  UserDefaults.get(decoder: JSUserModel.self, forKey: kUserDataKey) else { return nil}
        return model
    }
    
}
var isLogin:Bool {
    if !accessToken.isEmpty {
        return true
    }else{
        return false
    }
    
    
}
var isCustomer:Bool{
    guard let  model = userModel,let userRole = model.role else { return false}
    return userRole == .customer
    
}
var accessToken:String{
    set{
        UserDefaults.set(object: "\(newValue)", forKey: kAuthTokenKey)
    }
    get{
        guard let accessToken = UserDefaults.getObject(forKey: kAuthTokenKey) as? String else { return "" }
        return accessToken
    }
}


var appID:String{
    set{
        UserDefaults.set(object:"\(newValue)", forKey: kAppIDKey)
    }
    get{
        guard let accessToken = UserDefaults.getObject(forKey: kAppIDKey) as? String else { return "" }
        return accessToken
    }
}

let kNotificationCenter             =   NotificationCenter.default



let kTokenExpired = "The incoming token has expired"
let kUnauthorized = "Unauthorized"
var kAppImage:UIImage?          {get{ return Bundle.kAppIcon }}
var kAppTitle :String           {get{return Bundle.kAppTitle}}

let kConnectionError        = "No Internet Connection!☹"
let kUserDataKey            = "UserData"
let kAuthTokenKey           = "AuthToken"
let kNotAvaialable          = "Not Available"
let kAppIDKey               = "AppID"
let kUserIDKey               = "UserID"
//Default for API

let kClientType         = "iOS"
var pageNumber:Int = 1
var offsetLimit:Int = 10
var isFromMenu:Bool = false

