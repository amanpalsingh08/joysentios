//
//  CREndpoint.swift
//  JoySen
//
//  Created by Jitendra Kumar on 20/07/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//

import Foundation

//Web Service Constant String-



let kAPIVersion = "v1/"
//User API


private let kBaseUrl = "https://joysent.com/api/"

struct JSEndpoint {
    struct Auth {
        enum Post:String {
            case login = "login"
            case register = "register"
            case updateUser = "update-user"
            case forgotPassword = "forgot-password"
            case changePassword = "update-password"
            case addEditCard = "add-edit-card"
            
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
            
        }
        enum Get {
            case getUser(userId:Int)
            case getUsers(userId:Int)
            case recentConnection(userId:Int)
            case getCard(userId:Int)
            case logout
            
            
            var api:String{
                switch self {
                case .getUsers(let userId):return kBaseUrl + kAPIVersion + "get-users?id=\(userId)"
                case .getUser(let userId):return kBaseUrl + kAPIVersion + "get-user?id=\(userId)"
                case .getCard(let userId): return kBaseUrl + kAPIVersion + "get-card?user_id=\(userId)"
                case .logout :  return kBaseUrl + kAPIVersion + "logout?api_token=\(accessToken)"
                case .recentConnection(let userId): return kBaseUrl + kAPIVersion + "recent-connections?user_id=\(userId)"
                    
                    
                    
                }
            }
        }
        
        
        
    }
    
    struct Clients {
        enum Post:String {
            case addClient = "add-client"
            case updateClient = "update-client"
            case searchConnection = "search-connection"
            case searchClient = "search-client"
            case importClient = "import-client"
            case checkClient = "check-client"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
            
        }
        enum Get {
            case clients(userId:Int)
            case leadSource
            case stage
            case state
            case deliveredClient(userId:Int)
            case queueClient(userId:Int)
            case client(clientId:Int)
            case disabledClient(userId:Int)
            case clientConnections(userId:Int)
            
            var api:String{
                switch self {
                    
                case .clients(let userId):return kBaseUrl + kAPIVersion + "get-clients?user_id=\(userId)"
                case .leadSource :  return kBaseUrl + kAPIVersion + "get-lead-source"
                case .stage: return kBaseUrl + kAPIVersion + "get-stage"
                case .state: return kBaseUrl + kAPIVersion + "get-state"
                case .deliveredClient(let userId):return kBaseUrl + kAPIVersion + "delivered-client?user_id=\(userId)"
                case .queueClient(let userId):return kBaseUrl + kAPIVersion + "queue-client?user_id=\(userId)"
                case .client(let clientId):return kBaseUrl + kAPIVersion + "get-client?client_id=\(clientId)"
                case .disabledClient(let userId): return kBaseUrl + kAPIVersion + "disabled-clients?user_id=\(userId)"
                case .clientConnections(let userId): return kBaseUrl + kAPIVersion + "client-connections?user_id=\(userId)"
                }
            }
        }
    }
    
    struct Request {
        enum Get {
            case acceptRequest(userId:Int,requestId:Int)
            case cancelRequest(userId:Int,requestId:Int)
            case requestsSend(userId:Int)
            case requestsReceived(userId:Int)
            case sendRequest(senderId:Int,receiverId:Int,clientId:Int)
            case payAll(userId:Int,requestId:Int)
            case destroyRequest(requestId:Int)
            var api:String{
                switch self {
                    
                case .acceptRequest(let userId,let requestId):return kBaseUrl + kAPIVersion + "accept-request?user_id=\(userId)&request_id=\(requestId)"
                case .cancelRequest(let userId,let requestId):return kBaseUrl + kAPIVersion + "cancel-request?user_id=\(userId)&request_id=\(requestId)"
                case .requestsSend(let userId):return kBaseUrl + kAPIVersion+"requests-send?user_id=\(userId)"
                case .requestsReceived(let userId):return kBaseUrl + kAPIVersion+"requests-received?user_id=\(userId)"
                case .sendRequest(let senderId, let receiverId, let clientId):
                    return kBaseUrl + kAPIVersion+"send-request?sender_id=\(senderId)&eceiver_id=\(receiverId)&client_id=\(clientId)"
                case .payAll(let userId, let requestId):
                    return kBaseUrl + kAPIVersion + "pay-all?user_id=\(userId)&request_id=\(requestId)"
                case .destroyRequest(let requestId):
                    return kBaseUrl + kAPIVersion + "destroy-request?request_id=\(requestId)"
                }
            }
        }
        enum Post:String {
            case requestConnections = "request-connections"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
            
        }
    }
    struct Feedback {
        enum Get {
            case feedbacks(userId:Int)
            case delete(feedbackId:Int)
            
            var api:String{
                switch self {
                case .feedbacks(let userId):
                    return kBaseUrl + kAPIVersion + "feedback-list?user_id=\(userId)"
                case .delete(let feedbackId):
                    return kBaseUrl + kAPIVersion + "delete-feedbac?feedback_id=\(feedbackId)"
                }
            }
        }
        enum Post:String {
            case addEdit = "add-edit-feedback"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
        }
    }
    struct Gallery {
        enum Get :String{
            case galleries = "get-gallery"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
        }
    }
    struct ClientNutureGiftSubscription {
        enum Get {
            case subscribedClients(userId:Int)
            case onetimePaid(userId:Int)
            case getYears
            
            var api:String{
                switch self {
                case .subscribedClients(let userId):
                    return kBaseUrl + kAPIVersion + "subscribed-clients?user_id=\(userId)"
                case .onetimePaid(let userId):
                    return kBaseUrl + kAPIVersion + "onetime-paid?user_id=\(userId)"
                case .getYears:
                    return kBaseUrl + kAPIVersion + "get-years"
                    
                }
            }
        }
        enum Post:String {
            case getTotal = "get-total"
            case payNow   = "pay-now"
            case subscribePayment = "subscribe-payment"
            case requestConnection1 = "request-connection1"
            case requestConnection2 = "request-connection2"
            case acceptSubRequest = "accept-sub-request"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
        }
    }
    struct GiftStore {
        enum Get {
            case getGiftTypes
            case getDates
            case getGifts(giftTypeId:Int)
            case getGiftData(giftId:Int)
            case getGiftShippingPrice(giftId:Int,clientId:Int,quantity:Int)
            case purchaseGift(giftId:Int,clientId:Int,userId:Int,quantity:String,message:String?)
             case getCustomerGiftShippingPrice(giftId:Int,zipCode:String)
            var api:String{
                switch self {
                case .getGiftTypes:
                    return kBaseUrl + kAPIVersion + "get-gift-types"
                case .getGiftData(let giftId):
                    return kBaseUrl + kAPIVersion + "get-gift-data?gift_id=\(giftId)"
                case .getDates:
                    return kBaseUrl + kAPIVersion + "get-dates"
                case .getGifts(let giftTypeId):
                    return kBaseUrl + kAPIVersion + "get-gifts?gift_type_id=\(giftTypeId)"
                case .getGiftShippingPrice(let giftId, let clientId, let quantity):
                    return kBaseUrl + kAPIVersion + "gift-shipping-price?gift_id=\(giftId)&client_id=\(clientId)&quantity=\(quantity)"
                case .purchaseGift(let giftId,let clientId,let userId,let quantity, let message):
                    return kBaseUrl+kAPIVersion + "purchase-gift?gift_id=\(giftId)&client_id=\(clientId)&user_id=\(userId)&quantity=\(quantity)&message=\(message ?? "")"
                case .getCustomerGiftShippingPrice(giftId: let giftId, zipCode: let zipCode):
                    return kBaseUrl+kAPIVersion + "customer-gift-shipping-price?gift_id=\(giftId)&post_code=\(zipCode)"
                }
                
                
            }
        }
        
        enum Post:String {
            case customerPurchaseGift = "customer-purchase-gift"
            var api:String{
                return kBaseUrl + kAPIVersion + self.rawValue
            }
        }
        
    }
}


//?gift_id=\(giftId)&user_id=\(userId)&quantity=\(1)&message=\(message ?? "")&firstname=\(firstname)&lastname=\(lastname)&email=\(email)&phone=\(phone)&address=\(address)&state=\(state)&city=\(city)&zipcode=\(zipcode)"
