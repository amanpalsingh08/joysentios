//
//  JSFeedback.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

struct JSFeedback:Mappable {
    var feedbackID: Int?
    var clientID: String?
    var userID:String?
    var  clientFirstname:String?
    var  clientLastname:String?
    var clientEmail: String?
    var feedback:String?

    enum CodingKeys: String, CodingKey {
        case feedbackID = "feedback_id"
        case clientID = "client_id"
        case userID = "user_id"
        case clientFirstname = "client_firstname"
        case clientLastname = "client_lastname"
        case clientEmail = "client_email"
        case feedback
    }
}
extension JSFeedback:Equatable{
    static func ==(lhs:JSFeedback, rhs:JSFeedback)->Bool{
        return lhs.feedbackID == rhs.feedbackID && lhs.clientID == rhs.clientID  && lhs.userID == rhs.userID && lhs.clientFirstname == rhs.clientFirstname && lhs.clientLastname == rhs.clientLastname && lhs.clientEmail == rhs.clientEmail  && lhs.feedback == rhs.feedback
    }
}
