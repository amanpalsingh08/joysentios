//
//  JSAnniversaryGift.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
enum JSGitCategoryType:String {
    case HouseWarming = "House-Warming-Gifts"
    case ClientNurture = "Client-Nurture-Gifts"
    case HomeDecor = "Home-Decor"
    case Others = "Others"
}
struct JSGitCategory: Mappable,Hashable {
    var id: Int?
    var parentID: String?
    var typeName, typeSlug, createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case parentID = "parent_id"
        case typeName = "type_name"
        case typeSlug = "type_slug"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    var catType:JSGitCategoryType?{
        guard let vl = typeSlug , let type  = JSGitCategoryType(rawValue: vl) else { return nil }
        return type
    }
}

struct JSGifts:Mappable,Hashable {
    var id: Int?
    var giftName, description, price, height: String?
    var width, length, weight: String?
    var img: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case giftName = "gift_name"
        case description = "description"
        case price, height, width, length, weight, img
    }
}
struct JSGiftItem:Mappable,Hashable {
    var giftID: String?
    var giftName: String?
    var description: String?
    var weight: String?
    var length: String?
    var width: String?
    var height: String?
    var giftsAvailable:Int = 1
    var price: String?
    var images: [String] = []
    var quantity: Int = 1
    var tax: Double?
    var shipping: Double?
    var total: Double?
    
    enum CodingKeys: String, CodingKey {
        case giftID = "gift_id"
        case giftName = "gift_name"
        case description = "description"
        case weight, length, width, height
        case giftsAvailable = "gifts_available"
        case price, images, quantity, tax, shipping, total
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self) // defining our (keyed) container
       
        giftID =  try? container.decodeIfPresent(String.self, forKey: .giftID)
        giftName =  try? container.decodeIfPresent(String.self, forKey: .giftName)
        description = try? container.decodeIfPresent(String.self, forKey: .description)
        weight = try? container.decodeIfPresent(String.self, forKey: .weight)
        length = try? container.decodeIfPresent(String.self, forKey: .length)
        width = try? container.decodeIfPresent(String.self, forKey: .width)
        height = try? container.decodeIfPresent(String.self, forKey: .height)
        images = try container.decodeIfPresent([String].self, forKey: .images) ?? []
        
        if let vl =  try? container.decodeIfPresent(String.self, forKey: .price) {
             price = vl
        }else if let  vl = try? container.decodeIfPresent(Int.self, forKey: .price){
             price = "\(vl)"
        }
        
        if let vl =  try? container.decodeIfPresent(String.self, forKey: .quantity) {
             quantity = Int(vl) ?? 1
        }else if let  vl = try? container.decodeIfPresent(Int.self, forKey: .quantity){
             quantity = vl
        }
        if let vl =  try? container.decodeIfPresent(String.self, forKey: .giftsAvailable) {
             giftsAvailable = Int(vl) ?? 1
        }else if let  vl = try? container.decodeIfPresent(Int.self, forKey: .giftsAvailable){
             giftsAvailable = vl
        }
        if let vl = try? container.decodeIfPresent(Double.self, forKey: .quantity) {
            tax = vl
        }else if let  vl = try? container.decodeIfPresent(String.self, forKey: .quantity){
            tax = Double(vl)
        }
        
        if let vl = try? container.decodeIfPresent(Double.self, forKey: .shipping) {
            shipping = vl
        }else if let  vl = try? container.decodeIfPresent(String.self, forKey: .shipping){
            shipping = Double(vl)
        }
        
        if let vl = try? container.decodeIfPresent(Double.self, forKey: .total) {
            total = vl
        }else if let  vl = try? container.decodeIfPresent(String.self, forKey: .total){
            total = Double(vl)
        }
        
        
        
        
    }
}
