//
//  JSDeliveredClient.swift
//  JoySen
//
//  Created by Jitendra Kumar on 03/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
struct JSDeliveredClient:Mappable,Hashable {
    var id: Int?
    var clientID, clientName, clientFirstname, clientLastname: String?
    var clientStage, type, clientLeadSource, clientAssignedTo: String?
    var clientConnection1, clientConnection2: String?
    var clientReferer: String?
    var clientConn1Name, clientConn2Name, clientGroup: String?
    var trackingID: Int?
    var orderID: Int?
    var orderNumber:String?
    var labelID: Int?
    var status, createdAt, updatedAt, address: String?
    var city, state, email: String?
    var phone: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "client_id"
        case clientName = "client_name"
        case clientFirstname = "client_firstname"
        case clientLastname = "client_lastname"
        case clientStage = "client_stage"
        case type
        case clientLeadSource = "client_lead_source"
        case clientAssignedTo = "client_assigned_to"
        case clientConnection1 = "client_connection1"
        case clientConnection2 = "client_connection2"
        case clientReferer = "client_referer"
        case clientConn1Name = "client_conn1_name"
        case clientConn2Name = "client_conn2_name"
        case clientGroup = "client_group"
        case trackingID = "tracking_id"
        case orderID = "orderId"
        case orderNumber
        case labelID = "label_id"
        case status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case address, city, state, email, phone
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        clientID = try? container.decodeIfPresent(String.self, forKey: .clientID)
        clientName = try? container.decodeIfPresent(String.self, forKey: .clientName)
        clientFirstname = try? container.decodeIfPresent(String.self, forKey: .clientFirstname)
        clientLastname = try? container.decodeIfPresent(String.self, forKey: .clientLastname)
        clientStage = try? container.decodeIfPresent(String.self, forKey: .clientStage)
        type = try? container.decodeIfPresent(String.self, forKey: .type)
        clientLeadSource = try? container.decodeIfPresent(String.self, forKey: .clientLeadSource)
        clientAssignedTo = try? container.decodeIfPresent(String.self, forKey: .clientAssignedTo)
        clientConnection1 = try? container.decodeIfPresent(String.self, forKey: .clientConnection1)
        clientConnection2 = try? container.decodeIfPresent(String.self, forKey: .clientConnection2)
        clientReferer = try? container.decodeIfPresent(String.self, forKey: .clientReferer)
        clientConn1Name = try? container.decodeIfPresent(String.self, forKey: .clientConn1Name)
        clientConn2Name = try? container.decodeIfPresent(String.self, forKey: .clientConn2Name)
        clientGroup = try? container.decodeIfPresent(String.self, forKey: .clientGroup)
        trackingID = try? container.decodeIfPresent(Int.self, forKey: .trackingID)
        orderID = try? container.decodeIfPresent(Int.self, forKey: .orderID)
        orderNumber = try? container.decodeIfPresent(String.self, forKey: .orderNumber)
        labelID = try? container.decodeIfPresent(Int.self, forKey: .labelID)
        status = try? container.decodeIfPresent(String.self, forKey: .status)
        
        createdAt = try? container.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try? container.decodeIfPresent(String.self, forKey: .updatedAt)
        address = try? container.decodeIfPresent(String.self, forKey: .address)
        city = try? container.decodeIfPresent(String.self, forKey: .city)
        state = try? container.decodeIfPresent(String.self, forKey: .state)
        email = try? container.decodeIfPresent(String.self, forKey: .email)
        phone = try? container.decodeIfPresent(String.self, forKey: .phone)
        
   
    }
}
