//
//  JSState.swift
//  JoySen
//
//  Created by Jitendra Kumar on 24/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

struct JSState:Mappable {
    var id:Int?
    var name:String?
    var code:String?

    enum CodingKeys: String, CodingKey {
        case id, code
        case name = "state_name"
    }
    
    
}
extension JSState:Equatable{
    static func ==(lhs:JSState, rhs:JSState)->Bool{
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.code == rhs.code
    }
}
struct JSStage:Mappable {
    let id:Int?
    let name:String?
}
extension JSStage:Equatable{
    static func ==(lhs:JSStage, rhs:JSStage)->Bool{
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
struct JSLeadSource:Mappable {
    let id:Int?
    let name:String?
    
    
}
extension JSLeadSource:Equatable{
    static func ==(lhs:JSLeadSource, rhs:JSLeadSource)->Bool{
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
