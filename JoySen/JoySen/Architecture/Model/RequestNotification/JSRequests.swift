//
//  JSRequests.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
import UIKit
enum JSRequestStatusType:String {
    case pending
    case aggreed
    case accept
    case connected
    var title:String{
        if self == .aggreed {
            return "Accepted"
        }
        return self.rawValue.capitalized
    }
    var color:UIColor{
        switch self {
        case .pending: return #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        case .aggreed: return #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1)
        default:       return #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            
        }
    }
}
enum JSConnectionStatusType:String {
    case pending
    case aggreed
    var title:String{
        if self == .aggreed {
            return "Agreed"
        }
        return self.rawValue.capitalized
    }
    var color:UIColor{
        switch self {
        case .pending: return #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        case .aggreed: return #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1)
            
        }
    }
}
struct JSRequestsSent:Mappable {
    var requestID: Int?
    var clientName, clientEmail, conn1, conn2: String?
    var requestStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case requestID = "request_id"
        case clientName = "client_name"
        case clientEmail = "client_email"
        case conn1, conn2
        case requestStatus = "request_status"
    }
    var requestStatusType:JSRequestStatusType?{
        guard let val = self.requestStatus else {return nil}
        return JSRequestStatusType(rawValue:val)
    }
}
extension JSRequestsSent:Equatable{
    static func ==(lhs:JSRequestsSent, rhs:JSRequestsSent)->Bool{
        return lhs.requestID == rhs.requestID && lhs.clientName == rhs.clientName && lhs.clientEmail == rhs.clientEmail && lhs.conn1 == rhs.conn1 && lhs.conn2 == rhs.conn2 && lhs.requestStatus == rhs.requestStatus
    }
}

// MARK: - DataClass
struct JSRequestsReceivedData: Mappable {
    var card: Int?
    var requests: [JSRequestsReceived] = []
    var subscriptionRequests: [JSSubscriptionRequest] = []
    
    enum CodingKeys: String, CodingKey {
        case card = "card"
        case requests = "requests"
        case subscriptionRequests = "subscription_requests"
    }
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        card = try container.decodeIfPresent(Int.self, forKey: .card)
        if let list = try? container.decodeIfPresent([JSRequestsReceived].self, forKey: .requests) {
            requests = list
        }
        if let list = try? container.decodeIfPresent([JSSubscriptionRequest].self, forKey: .subscriptionRequests) {
            subscriptionRequests = list
        }
        //expiresAt = try container.decodeIfPresent(String.self, forKey: .expiresAt) ?? ""
        
        
    }
}
struct JSRequestsReceived: Mappable ,Hashable{
    var requestID: Int?
    var clientName:String?
    var clientEmail:String?
    var conn1ID:String?
    var conn1: String?
    var conn1Status:String?
    var conn2:String?
    var conn2Status:String?
    var requestStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case requestID = "request_id"
        case clientName = "client_name"
        case clientEmail = "client_email"
        case conn1ID = "conn1_id"
        case conn1 = "conn1"
        case conn1Status = "conn1_status"
        case conn2 = "conn2"
        case conn2Status = "conn2_status"
        case requestStatus = "request_status"
    }
    var requestStatusType:JSRequestStatusType?{
        guard let val = self.requestStatus else {return nil}
        return JSRequestStatusType(rawValue:val)
    }
    var conn1StatusType:JSConnectionStatusType?{
        guard let val = self.conn1Status else {return nil}
        return JSConnectionStatusType(rawValue:val)
    }
    var conn2StatusType:JSConnectionStatusType?{
        guard let val = self.conn2Status else {return nil}
        return JSConnectionStatusType(rawValue:val)
    }
}
extension JSRequestsReceived:Equatable{
    static func ==(lhs:JSRequestsReceived, rhs:JSRequestsReceived)->Bool{
        return lhs.requestID == rhs.requestID && lhs.clientName == rhs.clientName && lhs.clientEmail == rhs.clientEmail && lhs.conn1 == rhs.conn1 && lhs.conn1ID == rhs.conn1ID && lhs.conn2 == rhs.conn2 &&  lhs.conn1Status == rhs.conn1Status  &&  lhs.conn2Status == rhs.conn2Status  && lhs.requestStatus == rhs.requestStatus
    }
}
struct JSSubscriptionRequest: Mappable,Hashable {
    var requestID: Int?
    var type, clientName, clientEmail: String?
    var conn1ID: Int?
    var conn1, conn1Status, conn2, conn2Status: String?
    var requestStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case requestID = "request_id"
        case type
        case clientName = "client_name"
        case clientEmail = "client_email"
        case conn1ID = "conn1_id"
        case conn1
        case conn1Status = "conn1_status"
        case conn2
        case conn2Status = "conn2_status"
        case requestStatus = "request_status"
    }
    var requestStatusType:JSRequestStatusType?{
        guard let val = self.requestStatus else {return nil}
        return JSRequestStatusType(rawValue:val)
    }
    var conn1StatusType:JSConnectionStatusType?{
        guard let val = self.conn1Status else {return nil}
        return JSConnectionStatusType(rawValue:val)
    }
    var conn2StatusType:JSConnectionStatusType?{
        guard let val = self.conn2Status else {return nil}
        return JSConnectionStatusType(rawValue:val)
    }
}
