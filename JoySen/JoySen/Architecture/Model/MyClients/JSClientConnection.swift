//
//  JSClientConnection.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
import UIKit
enum JSClientConnectionStatus:String,Codable {
    case connected = "Connected"
    case request = "Request"
    case alreadyRequested = "Already Requested"
    case requestedYou = "Requested You"
    
    var title:String{
        return self.rawValue
    }
    var color:UIColor{
        switch self {
        case .connected:return #colorLiteral(red: 0.1565970778, green: 0.6567658782, blue: 0.2694083154, alpha: 1)
        case .request: return #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        default: return #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            
        }
    }
}
struct JSClientConnection:Mappable {
    
    var card: Int?
    var connection: [JSConnection]?
    
    enum CodingKeys: String, CodingKey {
        case card
        case connection = "requests"
        
    }
    struct JSConnection:Mappable {
        var clientID: Int?
        var clientName:String?
        var clientEmail: String?
        var connectionID: Int?
        var connectionName: String?
        var connectionRole: String?
        var status: JSClientConnectionStatus?
        
        enum CodingKeys: String, CodingKey {
            case clientID = "client_id"
            case clientName = "client_name"
            case clientEmail = "client_email"
            case connectionID = "connection_id"
            case connectionName = "connection_name"
            case connectionRole = "connection_role"
            case status
        }
        var requestType:JSClientConnectionStatus{
            guard let val = status else { return .request}
            return val
        }
    }
    // MARK: - SearchClientConnection
    struct JSSearchConnection: Codable {
        var connectionID: Int?
        var name, email: String?
        var role: JSUserRole?
        var count: Int?

        enum CodingKeys: String, CodingKey {
            case connectionID = "connection_id"
            case name, email, role, count
        }
    }
    
    
}
extension JSClientConnection.JSSearchConnection:Equatable{
    
    static func == (lhs:JSClientConnection.JSSearchConnection, rhs:JSClientConnection.JSSearchConnection)->Bool{
        return lhs.connectionID == rhs.connectionID && lhs.name == rhs.name && lhs.email == rhs.name && lhs.role == rhs.role && lhs.count == rhs.count
    }
}

