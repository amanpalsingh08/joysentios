//
//  JSUserModel.swift
//  ZGuideZ
//
//  Created by Mandeep Kaur on 20/11/18.
//  Copyright © 2018 Mandeep Kaur. All rights reserved.
//

import UIKit
struct JSReponse<T>:Mappable where T:Mappable{
    enum JSResponseType:String {
        case success = "success"
        case failure = "error"
    }
    var status: String?
    var statusCode: Int?
    var message: String?
    var data: T?
    
    enum CodingKeys: String, CodingKey {
        case status
        case statusCode = "status_code"
        case message, data
    }
    var statusType:JSResponseType?{
        guard let vl = self.status else { return nil}
        return JSResponseType(rawValue: vl)
    }
    var httpStatus:HTTPStatusCodes?{
        guard let vl = self.statusCode else { return nil }
        return HTTPStatusCodes(rawValue:vl)
    }
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try container.decodeIfPresent(Int.self, forKey: .statusCode)
        data = try? container.decodeIfPresent(T.self, forKey: .data)
        message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        //expiresAt = try container.decodeIfPresent(String.self, forKey: .expiresAt) ?? ""
      
        
    }
    
}
struct JSUserModel:Mappable{
    var userID: Int?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var profileImage: String?
    var email: String?
    var accessToken: String?
    var userName: String?
    var name: String?
    var role: JSUserRole?
    var address: String?
    var city: String?
    var state: String?
    var zipCode: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case firstName = "firstname"
        case lastName = "lastname"
        case phone
        case profileImage = "image"
        case email
        case userName = "username"
        case role
        case address
        case state
        case zipCode = "zip_code"
        case city
        case accessToken = "api_token"
    }
    
    
    
}



extension JSUserModel:Equatable{
    static func == (lhs:JSUserModel,rhs:JSUserModel)->Bool{
        return lhs.userID == rhs.userID  && lhs.firstName == rhs.firstName && lhs.lastName == rhs.lastName && lhs.phone == rhs.phone && lhs.profileImage == rhs.profileImage && lhs.email == rhs.email && lhs.userName == rhs.userName && lhs.role == rhs.role && lhs.address == rhs.address && lhs.state == rhs.state && lhs.zipCode == rhs.zipCode && lhs.city == rhs.city && lhs.accessToken == rhs.accessToken
    }
}
