//
//  JSGiftTotal.swift
//  JoySen
//
//  Created by Jitendra Kumar on 04/01/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
struct JSGiftTotal:Mappable {
    var pricePerGift, tax: String?
    var total: Double?

    enum CodingKeys: String, CodingKey {
        case pricePerGift = "price_per_gift"
        case tax, total
    }
}

extension JSGiftTotal:Equatable{
    static func == (lhs:JSGiftTotal, rhs:JSGiftTotal)->Bool{
        return lhs.pricePerGift == rhs.pricePerGift && lhs.tax == rhs.tax && lhs.total == rhs.total
        
    }
}
struct JSMonthsOfGift:Mappable,Equatable {
    var isSelected:Bool = false
    var title:String = ""
    enum CodingKeys: String, CodingKey {
           case title
           case isSelected
       }
    init(_ title:String, isSelected:Bool = false) {
        self.title = title
        self.isSelected = isSelected
        
    }
    static func == (lhs:JSMonthsOfGift, rhs:JSMonthsOfGift)->Bool{
        return lhs.isSelected == rhs.isSelected && lhs.title == rhs.title
        
    }
}
struct JSContribution:Mappable,Equatable {
    var contribute:Int = 0
    var title:String{
        return "\(contribute)%"
    }
    enum CodingKeys: String, CodingKey {
           case contribute
       }
    init(_ contribute:Int) {
        self.contribute = contribute

        
    }
    static func == (lhs:JSContribution, rhs:JSContribution)->Bool{
        return lhs.contribute == rhs.contribute 
        
    }
}
