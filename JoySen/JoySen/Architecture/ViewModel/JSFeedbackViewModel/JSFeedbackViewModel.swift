//
//  JSFeedbackViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
final class JSFeedbackViewModel{
    static let shared  = JSFeedbackViewModel()
    fileprivate var feedbacks:[JSFeedback] = []
    fileprivate var objFeedback:JSFeedback?
    
    func getFeedbacks(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSFeedback]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data { self.feedbacks = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Feedback.Get.feedbacks(userId: userID).api)
    }
    
    func addEditFeedback(_ isUpdate:Bool = false,onCompletion:@escaping()->Void){
        if self.clientID == nil || self.clientID?.isEmpty  == true{
            alertMessage = "Please Select Client"
        }else if self.feedback == nil || self.feedback?.isEmpty == true {
            alertMessage = "Please Enter your feedback."
        }else{
            guard NetworkState.state.isConnected , let  userId = createByUserId, !userId.isEmpty else{ return }
            
            var params:[String:Any] = ["user_id":userId,"client_id":"\(clientID ?? "")","feedback":feedback!]
            if isUpdate, let id = feedbackID {
                params["feedback_id"] = "\(id)"
             
            }
            SMUtility.shared.showHud()
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<[JSFeedback]>.self)
                        guard let statusType = response?.statusType else { return  }
                        switch statusType {
                        case .success:
                            if let message = response?.message { alertMessage = message }
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.Feedback.Post.addEdit.api,params: params)
        }
        
        
    }
    func deleteFeedback(feedbackId:Int, onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected else{ return }
               SMUtility.shared.showHud()
               Server.Request.dataTask(method: .get) { (result) in
                   async {
                       SMUtility.shared.hideHud()
                       switch result{
                       case .success(let data, _):
                           let response  = data.getValue(JSReponse<JSFeedback>.self)
                           guard let statusType = response?.statusType else { return  }
                           switch statusType {
                           case .success:
                               onCompletion()
                           default:
                               guard let message = response?.message else { return  }
                               alertMessage =  message
                           }
                       case .failure(let err):
                           alertMessage = err.localizedDescription
                       }
                   }
               }.request(JSEndpoint.Feedback.Get.delete(feedbackId: feedbackId).api)
    }
    func didSet(item model:JSFeedback?){
        if model == nil {
            self.objFeedback = JSFeedback()
            guard let userId = JSUserViewModel.shared.userId else { return  }
            self.createByUserId = "\(userId)"
        }
        self.objFeedback = model
    }
    
}
extension JSFeedbackViewModel{
    var count:Int{
        return feedbacks.count
    }
    subscript(at index:Int)->JSFeedback{
        return feedbacks[index]
    }
}
extension JSFeedbackViewModel{
    var feedbackID:Int?{
        return objFeedback?.feedbackID
    }
    var clientID:String?{
        
        set{
            guard var model = objFeedback else { return  }
            return model.clientID = newValue
        }
        get{
            return objFeedback?.clientID
        }
        
    }
    var clientName:String?{
        return "\(clientFirstName?.capitalized ?? "") \(clientLastname?.capitalized ?? "")"
    }
    
    var clientFirstName:String?{
        set{
            guard var model = objFeedback else { return  }
            return model.clientFirstname = newValue
        }
        get{
            return objFeedback?.clientFirstname
        }
    }
    var clientLastname:String?{
        set{
            guard var model = objFeedback else { return  }
            return model.clientLastname = newValue
        }
        get{
            return objFeedback?.clientLastname
        }
    }
    var createByUserId:String?{
        set{
            guard var model = objFeedback else { return  }
            return model.clientID = newValue
        }
        get{
            return objFeedback?.userID
        }
        
    }
    var feedback:String?{
        set{
            guard var model = objFeedback else { return  }
            return model.feedback = newValue
        }
        get{
            return objFeedback?.feedback
        }
    }
}
