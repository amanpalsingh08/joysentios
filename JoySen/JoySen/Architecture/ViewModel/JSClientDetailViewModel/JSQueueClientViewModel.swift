//
//  JSQueueClientViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

final class JSQueueClientViewModel{
    static let shared = JSQueueClientViewModel()
    fileprivate var queueClients:[JSQueueClient] = []
    
    
    func getQueueClients( onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
              SMUtility.shared.showHud()
              
              Server.Request.dataTask(method: .get) { (result) in
                  async {
                      SMUtility.shared.hideHud()
                      switch result{
                      case .success(let data, _):
                          let response  = data.getValue(JSReponse<[JSQueueClient]>.self)
                          guard let statusType = response?.statusType else { return  }
                          switch statusType {
                          case .success:
                              if let list = response?.data { self.queueClients = list }
                              onCompletion()
                          default:
                              guard let message = response?.message else { return  }
                              alertMessage =  message
                          }
                      case .failure(let err):
                          alertMessage = err.localizedDescription
                      }
                  }
              }.request(JSEndpoint.Clients.Get.queueClient(userId: userID).api)
    }
    
}
extension JSQueueClientViewModel{
    var count:Int{
        return queueClients.count
    }
    subscript(at index:Int)->JSQueueClient{
        return queueClients[index]
    }
}
