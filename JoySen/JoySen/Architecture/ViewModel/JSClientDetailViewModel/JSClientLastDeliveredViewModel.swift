//
//  JSClientLastDeliveredViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 03/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

final class JSClientLastDeliveredViewModel {
    static let shared  = JSClientLastDeliveredViewModel()
    fileprivate var deliveredClients:[JSDeliveredClient] = []
    
    
    func getDeliveredclients(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSDeliveredClient]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data { self.deliveredClients = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Clients.Get.deliveredClient(userId: userID).api)
    }
}
extension JSClientLastDeliveredViewModel{
    var count:Int{
        return deliveredClients.count
    }
    subscript(at index:Int)->JSDeliveredClient?{
        return self.deliveredClients[index]
    }
    func removeAll(){
        self.deliveredClients.removeAll()
    }
}
