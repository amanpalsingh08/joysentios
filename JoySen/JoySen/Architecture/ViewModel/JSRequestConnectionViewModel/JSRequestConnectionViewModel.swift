//
//  JSRequestConnectionViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 04/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation


enum JSConnectionType{
    case first
    case second
    
}

struct JSUserConnection:Mappable {
    var email, type: String?
    var name: String?
    var lastname: String?
    var id: Int?
    var firstname: String?
  
    var userType:JSUserRole?{
        guard let vl = self.type else { return nil }
        return JSUserRole(rawValue: vl)
    }
    var connectionName:String{
        if name != nil, !name!.isEmpty {
            return "\(name?.capitalized ?? "") (\(userType?.title ?? ""))"
        }else{
            return "\(firstname?.capitalized ?? "") \(lastname?.capitalized ?? "") (\(userType?.title.capitalized ?? ""))"
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case email
        case type = "user_type"
        case name, lastname, id, firstname
    }
}
extension JSUserConnection:Equatable{
    static func == (lhs:JSUserConnection, rhs:JSUserConnection)->Bool{
        return lhs.id == rhs.id && lhs.firstname == rhs.firstname && lhs.lastname == rhs.lastname && lhs.type == rhs.type && lhs.userType == rhs.userType && lhs.name == rhs.name && lhs.email == rhs.email
    }
}

final class JSRequestConnectionViewModel{
    static let shared =  JSRequestConnectionViewModel()
    fileprivate var connections1:[JSUserConnection] = []
    fileprivate var connections2:[JSUserConnection] = []
    fileprivate var objConnection:JSUserConnection?
    
    
    
    func getUserConnections(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected ,let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSUserConnection]>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data  { self.connections1 = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Auth.Get.getUsers(userId: userID).api)
    }
    
    func requestConnection(clientID:Int, conn1ID:Int, conn2ID:Int, onCompletion:@escaping()->Void){
        
        guard NetworkState.state.isConnected ,let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        let params:[String:Any]  = ["user_id":userID,"client_id":clientID, "connection1":conn1ID,"connection2":conn2ID]
        
        Server.Request.dataTask(method: .post) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSClientConnection>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let message = response?.message  { alertMessage = message }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.Request.Post.requestConnections.api, params: params)
    }
    
}

extension JSRequestConnectionViewModel{
    
    func count(_ type:JSConnectionType = .first)->Int{
        
        return type == .first ? connections1.count : connections2.count
    }
    func didSet(firstConnection obj:JSUserConnection?){
        if self.objConnection != nil {
            self.objConnection = nil
        }
        self.objConnection = obj
    }
    subscript(atConnection index:Int, type:JSConnectionType)->JSUserConnection?{
        if type == .first {
            return connections1[index]
        }else{
            return connections2[index]
        }
    }
    func resetConnection(_ type:JSConnectionType = .first){
        if type == .second{
            self.connections2.removeAll()
        }else{
            self.connections1.removeAll()
        }
        
    }
    func getConnection2List(){
        guard let item = objConnection else { return }
        let list  = self.connections1.filter({$0.id != item.id})
        self.connections2 = list.count>0 ? list : []
        
        
    }
    var isEmptyFirstConnection:Bool{
        return objConnection == nil ? true :false
    }
}
