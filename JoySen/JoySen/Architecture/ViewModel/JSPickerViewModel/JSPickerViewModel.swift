//
//  JSPickerViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 24/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation
enum JSPhoneType:String {
    case home
    case mobile
    var title:String{
        switch self {
        case .home: return "Home"
        default: return "Mobile"
            
        }
    }
}
enum JSStatus:String {
    case active = "1"
    case disabled = "0"
    
    var title:String{
        switch self {
        case .active: return "Active"
        default:return "Disabled"
            
        }
    }
}
enum JSFrequency:String {
    case monthly =  "M"
    case quaterly = "Q"
    case semiAnnualy = "S"
    case annualy = "A"
    case custom = "custom"
    
    var title:String{
        switch self {
        case .monthly: return  "Monthly"
        case .quaterly: return  "Quaterly"
        case .semiAnnualy: return  "Semi Annualy"
        case .annualy: return  "Annualy"
        case .custom: return self.rawValue.capitalized
        }
    }
    var groups:[String]{
        switch self {
        case .monthly: return ["M"]
        case .quaterly: return ["Q1", "Q2", "Q3", "Q4"]
        case .semiAnnualy: return ["S1","S2","S3","S4","S5","S6"]
        case .annualy: return ["A"]
        default: return []
            
        }
    }
    
   var numberOfGifts:[Int]{
        switch self {
        
        case .monthly,.custom:
            let list  = Array(2...36)
            return list
        case .quaterly: return [4,8,12]
        case .semiAnnualy: return [2,4,6]
        case .annualy: return [1,2,3]
       
            
    }
    }
}
enum JSUserRole:String, Mappable {
    case lender
    case agent
    case team
    case brokerage
    case company
    case vendor
    case salesRep = "sales rep"
    case customer
    
    var value:String{
        return self.rawValue
    }
    var title:String{
        return self.value.capitalized
    }
    
}


final class JSPickerViewModel{
    static let shared = JSPickerViewModel()
    fileprivate var states:[JSState] = []
    fileprivate  var stages:[JSStage] = []
    fileprivate  var leadSources:[JSLeadSource] = []
    
    fileprivate var groups:[String] = []
    fileprivate var frequencyList:[JSFrequency]{
        return [.monthly, .quaterly, .semiAnnualy,.annualy]
    }
    fileprivate var statusList:[JSStatus]{
        return [.active,.disabled]
    }
    fileprivate var phoneTypeList:[JSPhoneType]{
        return [.home,.mobile]
    }
    fileprivate var userRoles:[JSUserRole]{
        return [.lender,.agent,.team,.brokerage,.company,.vendor,.salesRep,.customer]
    }
    fileprivate var cardExpMonths:[String] = []
    fileprivate var cardExpYears:[String] = []
    func getState(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSState]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data { self.states = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Clients.Get.state.api, params: nil)
        
        
    }
    func getStage(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSStage]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data  {   self.stages = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Clients.Get.stage.api, params: nil)
        
        
    }
    func getLeadSource(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSLeadSource]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data  {   self.leadSources = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Clients.Get.leadSource.api, params: nil)
        
        
    }
    func  getCardMonthAndYears(onCompletion:@escaping()->Void){
          var years:[String] = []
          if years.count == 0 {
              var year = Calendar(identifier: Calendar.Identifier.gregorian).component(.year, from: Date())
              for _ in 1...40 {
                  years.append("\(year)")
                  year += 1
              }
          }
        self.cardExpYears = years
          // population months with localized names
          var months:[String] = []
          var month = 0
          for _ in 1...12 {
              months.append(DateFormatter().monthSymbols[month].capitalized)
              month += 1
          }
         
        self.cardExpMonths = months
        onCompletion()
      }
}
extension JSPickerViewModel{
    var stateCount:Int{
        return states.count
    }
    subscript(atState index:Int)->JSState{
        return self.states[index]
    }
    func removeStateData(){
        self.states.removeAll()
    }
    var stageCount:Int{
        return stages.count
    }
    subscript(atStage index:Int)->JSStage{
        return self.stages[index]
    }
    func removeStageData(){
        self.stages.removeAll()
    }
    var leadSourceCount:Int{
        return leadSources.count
    }
    subscript(atLeadSource index:Int)->JSLeadSource{
        return self.leadSources[index]
    }
    func removeLeadSourceData(){
        self.leadSources.removeAll()
    }
    
    var frequencyCount:Int{
        return frequencyList.count
    }
    subscript(atFrequncy index:Int)->JSFrequency{
        return self.frequencyList[index]
    }
    
    var groupCount:Int{
        return groups.count
    }
    subscript(atGroup index:Int)->String{
        return self.groups[index]
    }
    func didSetGroup(atfrequcy type:JSFrequency){
        if groups.count>0 {
            groups.removeAll()
        }
        self.groups = type.groups
    }
    var statusCount:Int{
        return statusList.count
    }
    subscript(atStatus index:Int)->JSStatus{
        return self.statusList[index]
    }
    var phoneTypeCount:Int{
        return phoneTypeList.count
    }
    subscript(atPhoneType index:Int)->JSPhoneType{
        return self.phoneTypeList[index]
    }
    var expMonthCount:Int{
        return cardExpMonths.count
    }
    
    subscript(atMonth index:Int)->String?{
        return self.cardExpMonths[index]
    }
    
    var expYearCount:Int{
        return cardExpYears.count
    }
    subscript(atYear index:Int)->String?{
        return self.cardExpYears[index]
    }
    var roleCount:Int{
        return self.userRoles.count
    }
    subscript(atRole index:Int)->JSUserRole?{
        return userRoles[index]
    }
}
