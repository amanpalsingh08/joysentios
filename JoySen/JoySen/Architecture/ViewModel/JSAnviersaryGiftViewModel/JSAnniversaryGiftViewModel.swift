//
//  JSAnniversaryGiftViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
final class JSAnniversaryGiftViewModel{
    static let shared = JSAnniversaryGiftViewModel()
    fileprivate var myclient:JSMyClient?
    fileprivate var scheduleDates:[String] = []
    fileprivate var anniversaryGifts:[JSAnniversaryGift] = []
    fileprivate var anniversaryGift:JSAnniversaryGift?
    fileprivate var giftTypeId:Int = 4 // static
    fileprivate var anniversaryGiftData:JSAnniversaryGiftData?
    fileprivate var categories:[JSGitCategory] = []
    fileprivate var scheduleDate:String?
    func getGiftTypes(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSGitCategory]>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data  { self.categories = list.compactMap({$0})}
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.GiftStore.Get.getGiftTypes.api)
        
    }
    
    
    func getGifts(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected else { return  }
        SMUtility.shared.showHud()
             Server.Request.dataTask(method: .get) { (result) in
                 async {
                     SMUtility.shared.hideHud()
                     switch result{
                     case .success(let data, _):
                         let response  = data.getValue(JSReponse<[JSAnniversaryGift]>.self)
                         guard let statusType = response?.httpStatus else { return  }
                         switch statusType {
                         case .OK:
                             if let list = response?.data  { self.anniversaryGifts = list}
                             onCompletion()
                         default:
                             guard let message = response?.message else { return  }
                             alertMessage =  message
                         }
                     case .failure(let err):
                         alertMessage = err.localizedDescription
                     }
                 }
             }.request(JSEndpoint.GiftStore.Get.getGifts(giftTypeId: giftTypeId).api)
        
    }
    func getGiftData(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected, let giftId = self.giftId ,let clientId = self.clientId else { return  }
        SMUtility.shared.showHud()
             Server.Request.dataTask(method: .get) { (result) in
                 async {
                     SMUtility.shared.hideHud()
                     switch result{
                     case .success(let data, _):
                         let response  = data.getValue(JSReponse<JSAnniversaryGiftData>.self)
                         guard let statusType = response?.httpStatus else { return  }
                         switch statusType {
                         case .OK:
                             if let vl = response?.data  { self.anniversaryGiftData = vl}
                             onCompletion()
                         default:
                             guard let message = response?.message else { return  }
                             alertMessage =  message
                         }
                     case .failure(let err):
                         alertMessage = err.localizedDescription
                     }
                 }
             }.request(JSEndpoint.GiftStore.Get.getGiftData(giftId: giftId, clientId: clientId).api)
        
    }
    
    
    
    //MARK:- getScheduleDates
    func getScheduleDates(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[String]>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data  { self.scheduleDates = list.compactMap({$0})}
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.GiftStore.Get.getDates.api)
        
    }
    
    func buyGift(onCompletion:@escaping()->Void){
        
        if clientId == nil {
             alertMessage = "Please Select Client"
        }else if scheduleDate == nil{alertMessage = "Please Select Schedule date"}else{
            
            guard NetworkState.state.isConnected,let giftId = self.giftId ,let clientId = self.clientId, let userId = JSUserViewModel.shared.userId, let date = scheduleDate, let giftTotalPrice  = self.giftTotalPrice else{ return }
            let params:[String : Any] = ["gift_id":giftId,"client_id":clientId,"user_id":userId,"price":giftTotalPrice,"date":date]
            SMUtility.shared.showHud()
            
            Server.Request.dataTask(method: .post) { (result) in
                async {
                    SMUtility.shared.hideHud()
                    switch result{
                    case .success(let data, _):
                        let response  = data.getValue(JSReponse<String>.self)
                        guard let statusType = response?.httpStatus else { return  }
                        switch statusType {
                        case .OK:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                            onCompletion()
                        default:
                            guard let message = response?.message else { return  }
                            alertMessage =  message
                        }
                    case .failure(let err):
                        alertMessage = err.localizedDescription
                    }
                }
            }.request(JSEndpoint.GiftStore.Post.purchaseGift.api,params: params)
        }
    }
}
extension JSAnniversaryGiftViewModel{
    var giftTypeCount:Int{
        return categories.count
    }
    subscript(atGiftType index:Int)->JSGitCategory?{
        return self.categories[index]
    }
    func removeAllGiftTypes(){
        categories.removeAll()
    }
    var scheduleDateCount:Int{
        return self.scheduleDates.count
    }
    subscript(atSchedule index:Int)->String?{
        return self.scheduleDates[index]
    }
    func didSetScheduleDate(at item:String?){
        self.scheduleDate = item
    }
    func removeAllGiftScheduleDates(){
        scheduleDates.removeAll()
    }
    var anniversaryGiftCount:Int{
        return self.anniversaryGifts.count
    }
    subscript(atAnniversaryGift index:Int)->JSAnniversaryGift?{
        return self.anniversaryGifts[index]
    }
    
    func didSetAnniverary(at index:Int) {
        self.anniversaryGift = self[atAnniversaryGift: index]
    }
    func removeAllAnniversaryGifts(){
        anniversaryGifts.removeAll()
    }
    
    func didSetClient(at  item:JSMyClient?){
        self.myclient = item
    }
    var clientName:String{
        return "\(myclient?.firstname?.capitalized ?? "") \(myclient?.lastname?.capitalized ?? "")"
    }
   fileprivate var clientId:Int?{
        return myclient?.id
    }
    fileprivate var giftId:Int?{
        return anniversaryGift?.id
    }
    var height:String{
        return self.anniversaryGift?.height ?? ""
    }
    var width:String{
        return self.anniversaryGift?.width ?? ""
    }
    var length:String{
        return self.anniversaryGift?.length ?? ""
    }
    var gitImageFile:String?{
        return self.anniversaryGift?.img
    }
    var giftName:String{
        return self.anniversaryGift?.giftName ?? ""
    }
    var giftDescription:String{
        return self.anniversaryGift?.description ?? ""
    }
    var giftPrice:String?{
        return anniversaryGiftData?.price
    }
    var giftShippingPrice:Double?{
        return anniversaryGiftData?.shipping
    }
    var giftTotalPrice:Double?{
        return anniversaryGiftData?.total
    }
    
    
    func resetAll(){
        scheduleDate = nil
        myclient = nil
        anniversaryGift = nil
        anniversaryGiftData = nil
       // removeAllGiftTypes()
       // removeAllAnniversaryGifts()
        removeAllGiftScheduleDates()
    }
}
