//
//  JSGiftStoreViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 02/05/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation
final  class JSGiftStoreViewModel{
    static let shared = JSGiftStoreViewModel()
    fileprivate var categories:[JSGitCategory] = []
 
    //MARK:- getGiftTypes
    func getGiftTypes(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSGitCategory]>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data  { self.categories = list.compactMap({$0})}
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.GiftStore.Get.getGiftTypes.api)
        
    }
    
  

}
extension JSGiftStoreViewModel{
    var giftTypeCount:Int{
        return categories.count
    }
    subscript(atGiftType index:Int)->JSGitCategory?{
        return self.categories[index]
    }
    func removeAllGiftTypes(){
        categories.removeAll()
    }
}
