//
//  JSGiftsViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import Foundation



final class JSGiftsViewModel{
    
    static let shared = JSGiftsViewModel()
    fileprivate var giftTypeId:Int?
    fileprivate var gifts:[JSGifts] = []
    
    func getGifts(onCompletion:@escaping()-> Void){
        guard NetworkState.state.isConnected, let giftTypeId = self.giftTypeId else { return  }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSGifts]>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data  { self.gifts = list}
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
        }.request(JSEndpoint.GiftStore.Get.getGifts(giftTypeId: giftTypeId).api)
        
    }
    
    
    
    
}
extension JSGiftsViewModel{
    func didSetGiftType(identity:Int?){
        self.giftTypeId = identity
    }
    var giftCount:Int{
        return self.gifts.count
    }
    subscript(atGift index:Int)->JSGifts?{
        return self.gifts[index]
    }
    
    func removeAllGifts(){
        gifts.removeAll()
    }
    
   
    
}
