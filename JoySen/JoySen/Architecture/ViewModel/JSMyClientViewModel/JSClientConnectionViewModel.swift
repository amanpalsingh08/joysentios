//
//  JSClientConnectionViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation



final class JSClientConnectionViewModel{
    static let shared = JSClientConnectionViewModel()
    fileprivate var clientConnections:[JSClientConnection.JSConnection] = []
    fileprivate var searchClientConnections:[JSClientConnection.JSSearchConnection] = []
    var serviceType:JSServiceType = .normal
    
    func getClientConnections(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected ,let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSClientConnection>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data?.connection  {   self.clientConnections = list }
                        self.serviceType = .normal
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Clients.Get.clientConnections(userId: userID).api, params: nil)
        
    }
    func searchClientConnection(_ query:String,onCompletion:@escaping(Bool)->Void){
        guard !query.isEmpty else {
            alertMessage = "Please Enter Keyword to search Client."
            return
        }
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        let params:[String:Any] = ["user_id":userID,"keyword":query]
        Server.Request.dataTask(method: .post)  { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSClientConnection.JSSearchConnection]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let list = response?.data else {
                            self.serviceType = .normal
                             onCompletion(false)
                            return
                            
                        }
                        self.searchClientConnections = list
                        self.serviceType = .searching
                        onCompletion(true)
                    default:
                         self.serviceType = .normal
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion(false)
                    }
                case .failure(let err):
                     self.serviceType = .normal
                    alertMessage = err.localizedDescription
                    onCompletion(false)
                }
            }
        }.request(JSEndpoint.Clients.Post.searchConnection.api,params: params)
        
    }
    
}

extension JSClientConnectionViewModel{
    func removeAll(){
       clientConnections.removeAll()
    }
    func removelAllSearchData(){
         searchClientConnections.removeAll()
    }
    var count:Int{
        return clientConnections.count
    }
    var searchConnectionCount:Int{
           return searchClientConnections.count
       }
    subscript(at index:Int)->JSClientConnection.JSConnection?{
        return clientConnections[index]
    }
    subscript(atSearch index:Int)->JSClientConnection.JSSearchConnection?{
           return searchClientConnections[index]
       }
    
}

