//
//  JSDashboardViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/08/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSDashboardViewModel: NSObject {
    static let shared = JSDashboardViewModel()
    fileprivate var queueClients:[JSQueueClient] = []
    fileprivate var deliveredClients:[JSDeliveredClient] = []
    
    func getHomeData(completion:@escaping()->Void){
        guard NetworkState.state.isConnected , let userID = JSUserViewModel.shared.userId else{ return }
        
        SMUtility.shared.showHud()
        let dispatchGroup = DispatchGroup()
        var errorMessgae:String? = nil
        
        dispatchGroup.enter()
        Server.Request.dataTask(method: .get) { (result) in
            switch result{
            case .success(let data, _):
                let response  = data.getValue(JSReponse<[JSQueueClient]>.self)
                guard let statusType = response?.statusType else { return  }
                switch statusType {
                case .success:
                    if let list = response?.data {
                        self.queueClients = list
                        
                    }
                    dispatchGroup.leave()
                default:
                    errorMessgae =  response?.message
                    dispatchGroup.leave()
                }
                
            case .failure(let err):
                errorMessgae = err.localizedDescription
                dispatchGroup.leave()
            }
        }.request(JSEndpoint.Clients.Get.queueClient(userId: userID).api)
        
        
        dispatchGroup.enter()
        Server.Request.dataTask(method: .get) { (result) in
            switch result{
            case .success(let data, _):
                let response  = data.getValue(JSReponse<[JSDeliveredClient]>.self)
                guard let statusType = response?.statusType else { return  }
                switch statusType {
                case .success:
                    if let list = response?.data {
                        self.deliveredClients = list
                    }
                    dispatchGroup.leave()
                default:
                    errorMessgae =  response?.message
                    dispatchGroup.leave()
                }
                
            case .failure(let err):
                errorMessgae = err.localizedDescription
                dispatchGroup.leave()
            }
        }.request(JSEndpoint.Clients.Get.deliveredClient(userId: userID).api)
        
        dispatchGroup.notify(queue: .main) {
            SMUtility.shared.hideHud()
            print(errorMessgae ?? "")
                completion()
        }
    }
    
}
extension JSDashboardViewModel{
    var queueClientCount:Int{
        self.queueClients.count>6 ? 6 : self.queueClients.count
    }
    var deliveredClientCount:Int{
        self.deliveredClients.count>6 ? 6 : self.deliveredClients.count
    }
    
    subscript(atDeliveredClient index:Int)->JSDeliveredClient?{
        return self.deliveredClients[index]
    }
    subscript(atQueueClient index:Int)->JSQueueClient?{
        return queueClients[index]
    }
    func removeAll(){
        self.queueClients.removeAll()
        self.deliveredClients.removeAll()
    }
}
