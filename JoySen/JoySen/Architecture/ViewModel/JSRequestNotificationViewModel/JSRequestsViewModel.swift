//
//  JSRequestViewModel.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import Foundation

final class JSRequestsViewModel{
    static let shared = JSRequestsViewModel()
    fileprivate var receivedRequests:[JSRequestsReceived] = []
    fileprivate var sendRequests:[JSRequestsSent] = []
    fileprivate var subscriptionRequests: [JSSubscriptionRequest] = []
    
    func getSendRequestList(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected ,let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<[JSRequestsSent]>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        if let list = response?.data  {   self.sendRequests = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.requestsSend(userId: userID).api, params: nil)
    }
    //MARK:- getRecievedRequestList
    func getRecievedRequestList(onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected ,let userID = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.httpStatus else { return  }
                    switch statusType {
                    case .OK:
                        if let list = response?.data?.requests  {   self.receivedRequests = list }
                        if let list = response?.data?.subscriptionRequests  {   self.subscriptionRequests = list }
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
        }.request(JSEndpoint.Request.Get.requestsReceived(userId: userID).api, params: nil)
    }
    
    //MARK:- sendRequest
    func sendRequest(receiverId:Int,clientId:Int,onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected, let senderId = JSUserViewModel.shared.userId else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.sendRequest(senderId: senderId, receiverId: receiverId, clientId: clientId).api, params: nil)
    }
    //MARK:- acceptRequest
    func acceptRequest(at index:Int,onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected, let userId = JSUserViewModel.shared.userId , let model = self[atReceived: index], let requestId = model.requestID else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.acceptRequest(userId: userId, requestId: requestId).api, params: nil)
    }
    
    //THIS API  USE In Request Recived
    func cancelRequest(at index:Int,onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected, let userId = JSUserViewModel.shared.userId , let model = self[atReceived: index], let requestId = model.requestID else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.cancelRequest(userId: userId, requestId: requestId).api, params: nil)
    }
    func distroyRequest(at index:Int,onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected,  let model = self[atReceived: index], let requestId = model.requestID else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.destroyRequest(requestId: requestId).api, params: nil)
    }
    func payAll(at index:Int,onCompletion:@escaping()->Void){
        guard NetworkState.state.isConnected, let userId = JSUserViewModel.shared.userId , let model = self[atReceived: index], let requestId = model.requestID else{ return }
        SMUtility.shared.showHud()
        Server.Request.dataTask(method: .get) { (result) in
            async {
                SMUtility.shared.hideHud()
                switch result{
                case .success(let data, _):
                    let response  = data.getValue(JSReponse<JSRequestsReceivedData>.self)
                    guard let statusType = response?.statusType else { return  }
                    switch statusType {
                    case .success:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                        onCompletion()
                    default:
                        guard let message = response?.message else { return  }
                        alertMessage =  message
                    }
                case .failure(let err):
                    alertMessage = err.localizedDescription
                }
            }
            
            
            
        }.request(JSEndpoint.Request.Get.payAll(userId:userId,requestId: requestId).api, params: nil)
    }
    
}
extension JSRequestsViewModel{
    func removeAllSendRequests(){
        self.sendRequests.removeAll()
    }
    var sendRequestCount:Int{
        self.sendRequests.count
    }
    subscript(atSend index:Int)->JSRequestsSent?{
        return self.sendRequests[index]
    }
    
    var receivedRequestCount:Int{
        self.receivedRequests.count
    }
    subscript(atReceived index:Int)->JSRequestsReceived?{
        return self.receivedRequests[index]
    }
    func removeAllReceivedRequests(){
        self.receivedRequests.removeAll()
    }
    var subscriptionRequestCount:Int{
        self.subscriptionRequests.count
    }
    subscript(atSubscription index:Int)->JSSubscriptionRequest?{
        return self.subscriptionRequests[index]
    }
    func removeAllSubscriptionRequest(){
        self.subscriptionRequests.removeAll()
    }
    
}
