//
//  JSImportClientsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 08/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSImportClientsVC: UIViewController {
    @IBOutlet weak var csvTF: JKTextField!
    fileprivate var document:UIDocumentPickerData?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction private func opPickDocument(_ sender: UIButton) {
        UIDocumentPickerKit.shared.showDocumentPicker(from: self, source: sender, OnFinalizationBlock: { (result) in
                   async {
                       switch result{
                       case .success(let vl):
                           let fileUrl = vl.fileUrl
                           if fileUrl.pathExtension == "csv"{
                            self.document = vl
                            self.csvTF.text = fileUrl.lastPathComponent
                              
                           }else{
                               alertMessage = "Please Select only CSV File"
                           }
                       case .failure(let err):
                           alertMessage = err.localizedDescription
                       }
                   }
               }, OnCancellationBlock: {
                   
               })
    }
    
    
    @IBAction private func onImportClients(_ sender: Any) {
        guard let doc = document else {
            alertMessage = "Please Select only CSV File"
            return
        }
       JSImportClientViewModel.shared.importClient(doc) {
        async {
            self.document = nil
        }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
