//
//  JSRequestConnectionsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSRequestConnectionsVC: UIViewController {
    
    @IBOutlet fileprivate weak var connection1TF: JKTextField!
    @IBOutlet fileprivate weak var connection2TF: JKTextField!
    fileprivate var viewModel = JSRequestConnectionViewModel.shared
    var clientID:Int?
    fileprivate var conn1ID:Int?
    fileprivate var conn2ID:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction private func onSubmit(_ sender:JKButton){
        view.endEditing(true)
        guard let clientID = self.clientID else {
            alertMessage = "Please select client"
            return
            
        }
        guard let conn1ID = self.conn1ID else {
            alertMessage = "Please select connection 1"
            return
            
        }
        guard let conn2ID = self.conn2ID else {
            alertMessage = "Please select connection 2"
            return
            
        }
        viewModel.requestConnection(clientID: clientID, conn1ID: conn1ID, conn2ID: conn2ID) {
            async {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == SegueIdentity.kConnection2PickerSegue {
            return !viewModel.isEmptyFirstConnection
        }
        return true
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kConnection1PickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .connection(type: .first, connection: nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kConnection2PickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .connection(type: .second, connection: nil), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .connection(let type, let connection):
                if type == .first {
                    self.viewModel.didSet(firstConnection: connection)
                    self.conn1ID = connection?.id
                    self.connection1TF.text = connection?.connectionName ?? ""
                }else{
                    self.conn2ID = connection?.id
                    self.connection2TF.text = connection?.connectionName ?? ""
                }
                
            default:
                break
            }
        }
        
    }
    
}
extension JSRequestConnectionsVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
