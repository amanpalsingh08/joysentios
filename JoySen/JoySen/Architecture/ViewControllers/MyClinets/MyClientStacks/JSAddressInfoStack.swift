//
//  JSAddressInfoStack.swift
//  JoySen
//
//  Created by Jitendra Kumar on 26/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSAddressInfoStack: UIStackView {
    
     //show Client
    @IBOutlet private weak var addresslbl: UILabel!
    @IBOutlet  private weak var statelbl: UILabel!
    @IBOutlet private weak var citylbl: UILabel!
    @IBOutlet private weak var zipCodelbl: UILabel!
    @IBOutlet private weak var countrylbl: UILabel!
    
    //Add Client / Update Client
    @IBOutlet private weak var addressTF: JKTextField!
    @IBOutlet  private weak var stateTF: JKTextField!
    @IBOutlet private weak var cityTF: JKTextField!
    @IBOutlet private weak var zipCodeTF: JKTextField!
    @IBOutlet private weak var countryTF: JKTextField!
    
    var address:String?{
        set{
            if let label = self.addresslbl {
                label.text = newValue ?? ""
            }
            if let field = self.addressTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.addresslbl {
                return label.text
            }
            if let field = self.addressTF {
                return field.text
            }
            return nil
        }
    }
    var state:String?{
        set{
            if let label = self.statelbl {
                label.text = newValue ?? ""
            }
            if let field = self.stateTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.statelbl {
                return label.text
            }
            if let field = self.stateTF {
                return field.text
            }
            return nil
        }
    }
    
    
    
    var city:String?{
        set{
            if let label = self.citylbl {
                label.text = newValue ?? ""
            }
            if let field = self.cityTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.citylbl {
                return label.text
            }
            if let field = self.cityTF {
                return field.text
            }
            return nil
        }
    }
    var zipCode:String?{
        set{
            if let label = self.zipCodelbl {
                label.text = newValue ?? ""
            }
            if let field = self.zipCodeTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.zipCodelbl {
                return label.text
            }
            if let field = self.zipCodeTF {
                return field.text
            }
            return nil
        }
    }
    var country:String?{
        set{
            if let label = self.countrylbl {
                label.text = newValue ?? ""
            }
            if let field = self.countryTF {
                field.text = newValue ?? ""
            }
            
        }
        get{
            if let label = self.countrylbl {
                return label.text
            }
            if let field = self.countryTF {
                return field.text
            }
            return nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if countryTF != nil {
            countryTF.isUserInteractionEnabled = false
        }
        
    }
    
}
