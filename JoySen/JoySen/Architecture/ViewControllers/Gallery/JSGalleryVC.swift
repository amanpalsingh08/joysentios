//
//  JSGalleryVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 23/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSGalleryVC: UIViewController {
    @IBOutlet fileprivate weak var tableView:UITableView!
    fileprivate var viewModel = JSGalleryViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getGallery()
        
    }
    
    fileprivate func getGallery(){
        viewModel.removeAll()
        self.tableView.reloadData()
        viewModel.getGalleryList {
            async {
                self.tableView.reloadData()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension JSGalleryVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kGalleryCell, for: indexPath) as! JSGalleryCell
        cell.item = viewModel[at: indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
