//
//  ForgotPasswordVC.swift
//  ZGuideZ
//
//  Created by Mandeep Kaur on 20/11/18.
//  Copyright © 2018 Mandeep Kaur. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak fileprivate var emailTF: JKTextField!
    
    @IBOutlet weak fileprivate var objUserVM:JSUserViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnClick(_ sender: Any) {
        self.view.endEditing(true)
        _ =  self.navigationController?.popViewController(animated:false)
    }
    
    @IBAction fileprivate func onForgotPassword(_ sender: Any) {
        self.view.endEditing(true)
        objUserVM.forgotPassword(emailTF: emailTF) {
            async {
                self.navigationController?.popViewController(animated: true)
            }
        }
      
            
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
//        if segue.identifier == SegueIdentity.kResetForgotPasswordSegue {
//            let controller = segue.destination as! ResetPasswordVC
//            controller.objUserVM = self.objUserVM
//            controller.objUserVM.set(registerEmail: emailTF.text!)
//        }
    }
    

}
