//
//  JSQueueClientsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSQueueClientsVC: UITableViewController {
    fileprivate var viewModel = JSQueueClientViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()
            
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getQueueClients()
    }
    //MARK:- getQueueClients
    
    fileprivate func getQueueClients(){
        
        viewModel.getQueueClients {
            async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        return viewModel.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kQueueClientCell, for: indexPath) as! JSQueueClientCell
        cell.queueItem = viewModel[at: indexPath.row]

        return cell
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 256
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
