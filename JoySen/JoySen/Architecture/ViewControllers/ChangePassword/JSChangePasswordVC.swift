//
//  JSChangePasswordVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSChangePasswordVC: UIViewController {
   @IBOutlet weak fileprivate var passwordTF: JKTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: JKTextField!
    fileprivate var viewModel = JSUserViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
     @IBAction  fileprivate func onUpdate(_ sender:Any){
         self.view.endEditing(true)
        guard let password = passwordTF.text, let confirmPassword = confirmPasswordTF.text else { return  }
        viewModel.changePassword(password, confirmPassword: confirmPassword) {
            
        }
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
