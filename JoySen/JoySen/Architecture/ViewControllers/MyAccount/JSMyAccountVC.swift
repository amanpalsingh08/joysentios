//
//  JSMyAccountVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 23/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSMyAccountVC: UIViewController {
    @IBOutlet private weak var firstnameTF:JKTextField!
    @IBOutlet private weak var lastnameTF:JKTextField!
    @IBOutlet private weak var emailTF:JKTextField!
    @IBOutlet private weak var phoneTF:JKTextField!
    @IBOutlet private weak var addressTF:JKTextField!
    @IBOutlet private weak var zipcodeTF:JKTextField!
    @IBOutlet private weak var cityTF:JKTextField!
    @IBOutlet private weak var stateTF:JKTextField!
    @IBOutlet private weak var userTypeTF:JKTextField!
    @IBOutlet private weak var profileBtn:JKButton!
    private var uploadImage:UIImage?
    fileprivate var userType:JSUserRole?{
        didSet{
            self.userTypeTF.text = userType?.title
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getUserData()
    }
    fileprivate func getUserData(){
        
        JSUserViewModel.shared.getUserProfile {
            async {
                self.loadData()
            }
        }
    }
    
    fileprivate func loadData(){
        let vm = JSUserViewModel.shared
        firstnameTF.text = vm.firstName
        lastnameTF.text = vm.lastName
        emailTF.text = vm.email
        phoneTF.text = vm.phone
        addressTF.text = vm.address
        zipcodeTF.text = vm.zipCode
        cityTF.text = vm.city
        stateTF.text = vm.state
        userType = vm.userType
        
        if !vm.profileImage.isEmpty {
            profileBtn.loadImage(filePath: vm.profileImage, for: .normal)
        }
        
    }
    @IBAction private func onProfileImage(_ sender:JKButton){
        
        UIBottomSheet.shared.showPicker(pickerOptions: [.photoLibrary,.photoCamera], message: "Please choose any one option for update profile image.", source: sender, allowsEditing: true) { (result) in
            switch result{
            case .image(let image, _):
                async {
                    self.profileBtn.normalImage = image
                    self.uploadImage = image
                    
                }
            default:break
            }
        }
        
    }
    @IBAction private func onSubmit(_ sender:JKButton){
        guard let firstname = firstnameTF.text,let lastname = lastnameTF.text, let email = emailTF.text, let userType = userType, let phone  = phoneTF.text, let address = addressTF.text, let city = cityTF.text,let state = stateTF.text, let zipCode = zipcodeTF.text   else { return  }
        guard let image = uploadImage else {
            alertMessage = "Please select profile image"
            return
            
        }
        JSUserViewModel.shared.updateProfile(firstname: firstname, lastname: lastname, email: email, userType: userType, phone: phone, address: address, city: city, state: state, zipcode: zipCode,image: image) {
            async {
                self.loadData()
            }
        }
        
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kStatePickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .state(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kUserRoleSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .userRole(userType), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .state(let vsl):
                self.stateTF.text = vsl?.name ?? ""
            case .userRole(let role):
                self.userType = role
            default:
                break
            }
        }
        
    }
    
    
}
extension JSMyAccountVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
extension JSMyAccountVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTF   {
            var fullString = textField.text ?? ""
            fullString.append(string)
            if fullString.count>textField.maxLength {
                return false
            }
            let shouldRemoveLastDigit = range.length == 1 ? true : false
            textField.text = fullString.formattedNumber(shouldRemoveLastDigit: shouldRemoveLastDigit)
            
            return false
        }else{
            return true
        }
        
    }
}
