//
//  JSPickerVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 24/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit
enum JSPickerType {
    
    case state(JSState?)
    case stage(JSStage?)
    case leadSource(JSLeadSource?)
    case connection(type:JSConnectionType, connection:JSUserConnection?)
    case phoneType1(JSPhoneType?)
    case phoneType2(JSPhoneType?)
    case shippingState(JSState?)
    case frequency(JSFrequency?)
    case group(frequencyType:JSFrequency,String)
    case status(JSStatus?)
    case cardExpMonth(String?)
    case cardExpYear(String?)
    case client(JSMyClient?) // FeedbackScrren and cSubscribe
    case giftSubscribeFrequency(JSFrequency?)
    case numOfGift(Int)
    case giftQuantity(Int)
    case contribution(type:JSContributionType, contri:JSContribution?)
    case userRole(JSUserRole?)
    
}






class JSPickerVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var pickerType:JSPickerType?
    fileprivate var vm =  JSPickerViewModel.shared
    fileprivate var connectionViewModel = JSRequestConnectionViewModel.shared
    var didSelectPickerItem:((_ item:JSPickerType)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPickerData()
    }
    fileprivate func didUpdatePreferredContentHieght(_ count:Int){
        self.tableView.reloadData()
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.preferredContentSize.height =  55*CGFloat(count)
            self.view.setNeedsDisplay()
        }
        
    }
    private func  getPickerData(){
        guard let type = pickerType else { return  }
        switch type {
        case .state,.shippingState:
            self.vm.getState {
                async {
                    self.didUpdatePreferredContentHieght(self.vm.stateCount)
                }
            }
        case .stage:
            self.vm.getStage {
                async {
                    self.didUpdatePreferredContentHieght(self.vm.stageCount)
                }
            }
            
        case .leadSource:
            self.vm.getLeadSource {
                async {
                    self.didUpdatePreferredContentHieght(self.vm.leadSourceCount)
                }
            }
        case .connection( let type, _):
            
            if type == .first {
                connectionViewModel.getUserConnections {
                    async {
                        
                        self.didUpdatePreferredContentHieght(self.connectionViewModel.count(type))
                    }
                }
            }else{
                async {
                    self.connectionViewModel.getConnection2List()
                    self.didUpdatePreferredContentHieght(self.connectionViewModel.count(type))
                }
                
            }
        case .group(let frequencyType,_):
            async {
                self.vm.didSetGroup(atfrequcy: frequencyType)
                self.didUpdatePreferredContentHieght(self.vm.groupCount)
            }
            
            
            
            
        case .phoneType1, .phoneType2:
            async {
                self.didUpdatePreferredContentHieght(self.vm.phoneTypeCount)
            }
            
        case .frequency:
            async {
                
                self.didUpdatePreferredContentHieght(self.vm.frequencyCount)
            }
        case .status:
            async {
                self.didUpdatePreferredContentHieght(self.vm.statusCount)
            }
        case .cardExpYear:
            self.vm.getCardMonthAndYears {
                async {
                    self.didUpdatePreferredContentHieght(self.vm.expYearCount)
                }
            }
        case .cardExpMonth:
            self.vm.getCardMonthAndYears {
                async {
                    self.didUpdatePreferredContentHieght(self.vm.expMonthCount)
                }
            }
            
        case .client:
            JSMyClientViewModel.shared.getMyClinets {
                
                async {
                    self.didUpdatePreferredContentHieght(JSMyClientViewModel.shared.myClientCount)
                }
            }
            
        case .giftSubscribeFrequency:
            JSClientNutureGiftSubscriptionViewModel.shared.getFrequencyData {
                async {
                    self.didUpdatePreferredContentHieght(JSClientNutureGiftSubscriptionViewModel.shared.frequencyCount)
                }
            }
        case .numOfGift:
            JSClientNutureGiftSubscriptionViewModel.shared.getNumberOfGifts {
                async {
                    self.didUpdatePreferredContentHieght(JSClientNutureGiftSubscriptionViewModel.shared.numberOfGiftsCount)
                }
            }
        case .contribution:
            JSClientNutureGiftSubscriptionViewModel.shared.getContributions {
                async {
                    self.didUpdatePreferredContentHieght(JSClientNutureGiftSubscriptionViewModel.shared.contributionCount)
                }
            }
            
            
        case .userRole:
            async {
                self.didUpdatePreferredContentHieght(self.vm.roleCount)
            }
        case .giftQuantity:
            async {
                self.didUpdatePreferredContentHieght(JSGiftDetailViewModel.shared.giftsQuantityCount)
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension JSPickerVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let type = pickerType else { return 0 }
        
        switch type {
        case .state,.shippingState:return  self.vm.stateCount
        case .stage: return  self.vm.stageCount
        case .leadSource:return  self.vm.leadSourceCount
        case .connection(let type, _):return  connectionViewModel.count(type)
        case .phoneType1, .phoneType2: return self.vm.phoneTypeCount
        case .frequency: return self.vm.frequencyCount
        case .group:  return self.vm.groupCount
        case .status: return self.vm.statusCount
        case .cardExpMonth: return self.vm.expMonthCount
        case .cardExpYear: return self.vm.expYearCount
        case .client: return  JSMyClientViewModel.shared.myClientCount
        case .giftSubscribeFrequency: return  JSClientNutureGiftSubscriptionViewModel.shared.frequencyCount
        case .numOfGift: return JSClientNutureGiftSubscriptionViewModel.shared.numberOfGiftsCount
        case .contribution: return JSClientNutureGiftSubscriptionViewModel.shared.contributionCount
        case .userRole:return vm.roleCount
        case .giftQuantity: return JSGiftDetailViewModel.shared.giftsQuantityCount
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kStateCell, for: indexPath) as! JSStateCell
        cell.state = nil
        if let type = pickerType  {
            switch type {
            case .state,.shippingState:
                cell.state = self.vm[atState: indexPath.row]
            case .stage:
                cell.stage = self.vm[atStage: indexPath.row]
            case .leadSource:
                cell.leadSourece = self.vm[atLeadSource: indexPath.row]
            case .connection(let type, _):
                cell.connection = connectionViewModel[atConnection: indexPath.row, type]
            case .phoneType1,.phoneType2:
                cell.phoneType = self.vm[atPhoneType: indexPath.row]
            case .frequency: cell.frequency = self.vm[atFrequncy: indexPath.row]
            case .group: cell.group = self.vm[atGroup: indexPath.row]
            case .status: cell.status = self.vm[atStatus: indexPath.row]
            case .cardExpMonth: cell.month = self.vm[atMonth: indexPath.row]
            case .cardExpYear: cell.year = self.vm[atYear: indexPath.row]
            case .client: cell.client =  JSMyClientViewModel.shared[atClient: indexPath.row]
            case .giftSubscribeFrequency: cell.frequency = JSClientNutureGiftSubscriptionViewModel.shared[atFrequency: indexPath.row]
            case .numOfGift: cell.numOfGift = JSClientNutureGiftSubscriptionViewModel.shared[atNumOfGit: indexPath.row]
            case .contribution: cell.contribute = JSClientNutureGiftSubscriptionViewModel.shared[atContribute: indexPath.row]
            case .userRole:
                cell.role = self.vm[atRole: indexPath.row]
            case .giftQuantity:
                cell.numOfGift = JSGiftDetailViewModel.shared[atQuantity: indexPath.row]
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55//UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let type = pickerType  {
            switch type {
            case .state:
                let state = self.vm[atState: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.state(state))
                }
            case .shippingState:
                let state = self.vm[atState: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.shippingState(state))
                }
            case .stage:
                let stage = self.vm[atStage: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.stage(stage))
                }
            case .leadSource:
                let leadSourece = self.vm[atLeadSource: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.leadSource(leadSourece))
                }
            case .connection(let type, _):
                let copnnection = connectionViewModel[atConnection: indexPath.row, type]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.connection(type: type, connection: copnnection))
                }
            case .phoneType1:
                let phoneType = self.vm[atPhoneType: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.phoneType1(phoneType))
                }
            case .phoneType2:
                let phoneType = self.vm[atPhoneType: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.phoneType2(phoneType))
                }
            case .frequency:
                let frequency = self.vm[atFrequncy: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.frequency(frequency))
                }
            case .group(let type,_):
                
                let group = self.vm[atGroup: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.group(frequencyType: type, group))
                }
            case .status:
                let status = self.vm[atStatus: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.status(status))
                }
                
            case .cardExpMonth:
                if let  _ = self.vm[atMonth: indexPath.row] {
                    let val = "\(indexPath.row+1)"
                    self.dismiss(animated: true) {
                        self.didSelectPickerItem?(.cardExpMonth(val))
                    }
                }
                
            case .cardExpYear:
                let year = self.vm[atYear: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.cardExpYear(year))
                }
            case .client:
                let client =  JSMyClientViewModel.shared[atClient: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.client(client))
                }
            case .giftSubscribeFrequency:
                let frequency =  JSClientNutureGiftSubscriptionViewModel.shared[atFrequency: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.giftSubscribeFrequency(frequency))
                }
                
            case .numOfGift:
                if let gift =  JSClientNutureGiftSubscriptionViewModel.shared[atNumOfGit: indexPath.row] {
                    self.dismiss(animated: true) {
                        self.didSelectPickerItem?(.numOfGift(gift))
                    }
                }
                
            case .contribution(let type,_):
                let cont =  JSClientNutureGiftSubscriptionViewModel.shared[atContribute: indexPath.row]
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.contribution(type: type, contri: cont))
                }
           
            case .userRole:
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.userRole(self.vm[atRole: indexPath.row]))
                }
            case .giftQuantity:
                self.dismiss(animated: true) {
                    self.didSelectPickerItem?(.giftQuantity(JSGiftDetailViewModel.shared[atQuantity: indexPath.row]))
                }
            }
            
            
        }
        
    }
    
    
}
