//
//  JSFeedbackVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 01/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSFeedbackVC: UITableViewController {

    fileprivate var viewModel = JSFeedbackViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getClientFeedbacks()
    }

    //MARK:- getClientFeedbacks
    
    fileprivate func getClientFeedbacks(){
        viewModel.getFeedbacks {
            async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kFeedbackCell, for: indexPath) as! JSFeedbackCell
        cell.rowIndex = indexPath.row
        cell.feedback = viewModel[at: indexPath.row]
        cell.refresh = {
            self.getClientFeedbacks()
        }

        return cell
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 144
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kAddFeedbackSegue {
           let controller  = segue.destination as! JSAddEditFeedbackVC
            controller.isAddFeedback = true
            viewModel.didSet(item: nil)
        }else if segue.identifier == SegueIdentity.kUpdateFeedbackSegue, let editBtn = sender as? JKButton {
            let controller  = segue.destination as! JSAddEditFeedbackVC
             let index = editBtn.tag
            controller.isAddFeedback = false
            let item  = viewModel[at: index]
            viewModel.didSet(item: item)
        }
    }
    

}
