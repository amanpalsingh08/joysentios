//
//  JSClientNurtureGiftSubscriptionVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 28/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSClientNurtureGiftSubscriptionVC: UIViewController {
    @IBOutlet fileprivate weak var clientTF:JKTextField!
    @IBOutlet fileprivate weak var frequancyTF:JKTextField!
    @IBOutlet fileprivate weak var numOfGiftTF:JKTextField!
    @IBOutlet fileprivate weak var selectMonthForGiftStack:JSSelectMonthForGiftStack!
    @IBOutlet fileprivate weak var paymentStack:JSPaymentStack!
    @IBOutlet fileprivate weak var connectionStack:JSConnectionContainerStack!
    @IBOutlet fileprivate weak var compensationStack:JSCompensationStack!
    fileprivate var giftSubscriptionViewModel = JSClientNutureGiftSubscriptionViewModel.shared
    fileprivate var connectionViewModel = JSRequestConnectionViewModel.shared
    @IBOutlet weak var requestConnectionBtn: JKButton!
    @IBOutlet weak var submitBtnStack: UIStackView!
    fileprivate var payEventType:JSPaymentEventType?
    fileprivate var compensationType:JSCompensationType = .default
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentStack.didSelectPaymentOptionBlock =  { (event) in
            async {
                self.requestConnectionBtn.isHidden = true
                self.payEventType = nil
                self.connectionStack.isHidden = true
                self.connectionStack.reloadData()
                self.compensationStack.isHidden = true
                self.compensationStack.reloadData()
                
                switch event {
                case .fullPayment:
                    self.showAlertAction(title: "Pay 100% Payment Now", message: "Are you sure you want to Pay 100% payment in advsnce for gifts", cancelTitle: "Pay Now", otherTitle: "Subscribe Payment") { (index) in
                        if index == 0{
                            self.giftSubscriptionViewModel.payNow {
                                
                            }
                        }else if index == 2{
                            
                            // subscribe paymnet
                            self.showAlertAction(title: "Subscribe Clients Gifts", message: "The Gift will be send quaterly to the client. Every $12/month will be deducted from your account and $35 will be the subscription startup amount which will be deducted now after accepting the subscription.", otherTitle: "Subscribe") { (index) in
                                if index == 2{
                                    self.giftSubscriptionViewModel.subscribePayment {
                                        
                                    }
                                }
                            }
                            
                            
                        }
                    }
                   
                    
                //Call Api for 100% Payment
                case .oneConnection:
                    self.payEventType = .oneConnection
                    self.connectionStack.isHidden = false
                    self.compensationStack.isHiddenOneConnectionContribution = true
                    self.connectionStack.reloadData()
                case .twoConnection:
                    self.payEventType = .twoConnection
                    self.connectionStack.isHidden = false
                    self.compensationStack.isHiddenOneConnectionContribution = false
                    self.connectionStack.reloadData(false)
                case .searchClients:
                    //search
                    self.performSegue(withIdentifier: SegueIdentity.kSearchConnectionSegue, sender: nil)
                    
                }
                UIView.animate(withDuration: 0.3) {
                    self.view.setNeedsLayout()
                    self.view.setNeedsDisplay()
                }
            }
        }
        connectionStack.didSelectCompensationOption = { (type) in
            async {
                self.requestConnectionBtn.isHidden = false
                self.compensationType = type
                self.compensationStack.reloadData()
                
                switch type {
                case .custom:
                    self.compensationStack.isHidden = false
                    self.compensationStack.isHiddenOneConnectionContribution = self.payEventType == .twoConnection  ? false : true
                    
                default:
                    self.compensationStack.isHidden = true
                    
                    
                }
                UIView.animate(withDuration: 0.3) {
                    self.view.setNeedsLayout()
                    self.view.setNeedsDisplay()
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    fileprivate func resetStacks(){
        self.submitBtnStack.isHidden = true
        self.requestConnectionBtn.isHidden = true
        self.giftSubscriptionViewModel.resetAll()
        self.paymentStack.isHidden = true
        self.paymentStack.reloadPaymentDetail()
        self.selectMonthForGiftStack.isHidden = true
        self.selectMonthForGiftStack.reload()
        self.connectionStack.isHidden = true
        self.connectionStack.reloadData()
        
        self.compensationStack.isHidden = true
        self.compensationStack.reloadData()
        self.clientTF.text = ""
        self.frequancyTF.text = ""
        self.numOfGiftTF.text = ""
        UIView.animate(withDuration: 0.3) {
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
        }
        
        
    }
    
    //MARK:- onRequestConnection
    @IBAction private func onRequestConnection(_ sender:Any){
        if self.payEventType == .oneConnection {
            giftSubscriptionViewModel.requestConnection(compensationType, isRequestConnection1: true) {
                async {
                    self.resetStacks()
                }
            }
        }else if self.payEventType == .twoConnection{
            giftSubscriptionViewModel.requestConnection(compensationType, isRequestConnection1: false) {
                async {
                    self.resetStacks()
                }
            }
        }
        
        
    }
    //MARK:- onGetGiftTotal
    @IBAction private func onGetGiftTotal(_ sender:Any){
        giftSubscriptionViewModel.getGitTotals {
            async {
                if self.paymentStack.isHidden{
                    self.paymentStack.isHidden = false
                }
                self.paymentStack.reloadPaymentDetail()
            }
        }
        
    }
    
    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == SegueIdentity.kConnection2PickerSegue {
            return !connectionViewModel.isEmptyFirstConnection
        }else if identifier == SegueIdentity.kFrequencyPickerSegue{
            return giftSubscriptionViewModel.isShowFrequency
        }else if identifier == SegueIdentity.kNumOfGitsPickerSegue{
            return giftSubscriptionViewModel.isShowFrequency && giftSubscriptionViewModel.isShowNumOfGifts
        }else if identifier == SegueIdentity.kConnection1ContributionPickerSegue{
            return giftSubscriptionViewModel.isSelfContribute
        }
        return true
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kClientPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .client(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kFrequencyPickerSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .giftSubscribeFrequency(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kNumOfGitsPickerSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .numOfGift(0), sender: sender)
        }
        else if segue.identifier == SegueIdentity.kConnection1PickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .connection(type: .first, connection: nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kConnection2PickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .connection(type: .second, connection: nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kSelfContributionPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .contribution(type: .selfContribute, contri: nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kConnection1ContributionPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .contribution(type: .conn1Contribute, contri: nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kSearchConnectionSegue{
            guard segue.destination is JSSearchConnectionVC else{return}
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .client(let client):
                self.giftSubscriptionViewModel.didSetClient(at: client)
                self.clientTF.text = self.giftSubscriptionViewModel.clientName
            case .giftSubscribeFrequency(let feq):
                self.giftSubscriptionViewModel.didSetFrequency(at: feq)
                self.frequancyTF.text = feq?.title ?? ""
                
                if feq == .custom {
                    if self.submitBtnStack.isHidden == false {
                        self.submitBtnStack.isHidden = true
                    }
                    if self.numOfGiftTF.text?.isEmpty == false {
                        self.numOfGiftTF.text = ""
                        self.giftSubscriptionViewModel.didSetGifts(nil)
                    }
                    self.selectMonthForGiftStack.loadGiftMonths()
                }else{
                    self.selectMonthForGiftStack.isHidden = true
                    self.selectMonthForGiftStack.reload()
                }
            case .numOfGift(let gifts):
                
                self.numOfGiftTF.text = "\(gifts)"
                self.giftSubscriptionViewModel.didSetGifts(gifts)
                self.selectMonthForGiftStack.isHidden = self.giftSubscriptionViewModel.isShowMonthOfGift ? false : true
                self.selectMonthForGiftStack.reload(self.selectMonthForGiftStack.isHidden)
                self.submitBtnStack.isHidden = false
                
            case .connection(let type, let connection):
                self.connectionStack.isHiddenCompensationOption = false
                if type == .first {
                    self.connectionViewModel.didSet(firstConnection: connection)
                    self.connectionStack.oneConnection = connection
                }else{
                    self.connectionStack.twoConnection = connection
                }
            case .contribution( let type, let contri):
                self.giftSubscriptionViewModel.didSetContribution(at: contri, type: type)
                if type == .selfContribute {
                    self.compensationStack.compensation1 = ""
                    self.compensationStack.selfCompensation = contri?.title
                }else{
                    self.compensationStack.compensation1  = contri?.title
                }
                self.compensationStack.otherCompensation = self.giftSubscriptionViewModel.conn2ContributeString
                
                
            default:
                break
            }
        }
        
    }
    
}
extension JSClientNurtureGiftSubscriptionVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}


