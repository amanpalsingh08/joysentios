//
//  JSClientNutureGitSubsriptionStacks.swift
//  JoySen
//
//  Created by Jitendra Kumar on 04/01/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit
enum JSCompensationType {
    case `default`
    case custom
}
enum JSPaymentEventType:Int {
    case fullPayment = 100
    case oneConnection
    case twoConnection
    case searchClients
}
class JSSelectMonthForGiftStack: UIStackView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    @IBOutlet private weak var headerLabel:UILabel!
    @IBOutlet private weak var collectionView:UICollectionView!
    @IBOutlet private weak var card:JKCardView!
    @IBOutlet private weak var flowLayout:UICollectionViewFlowLayout!
    private var viewModel = JSClientNutureGiftSubscriptionViewModel.shared
    
    func reload(_ isClear:Bool = true){
        if isClear {
            viewModel.removeAllMonthsOfGift()
        }
        self.collectionView.reloadData()
    }
    func loadGiftMonths(){
        reload()
        if viewModel.isShowMonthOfGift {
            viewModel.getMonthsOfGifts {
                async {
                    self.collectionView.reloadData()
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.monthOfGiftCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JSGiftSelctMonthCell", for: indexPath) as! JSGiftSelctMonthCell
        let item  = viewModel[atMonthOfGift: indexPath.row]
        cell.month = item
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didUpdateCheckAccessoryOfMonth(at: indexPath.row)
        collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.isHidden {
            return .zero
        }else{
            return CGSize(width: 160, height: 60)
        }
        
        // return CGSize(width: 160, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
}
class JSPaymentStack: UIStackView {
    
    @IBOutlet private weak var headerLabel:UILabel!
    @IBOutlet private weak var pricePerGiftStack:JSPaymentValueStack!
    @IBOutlet private weak var taxStack:JSPaymentValueStack!
    @IBOutlet private weak var totalStack:JSPaymentValueStack!
    
    @IBOutlet private weak var fullPaymentBtn:JKButton!
    @IBOutlet private weak var oneConnectionBtn:JKButton!
    @IBOutlet private weak var twoConnectionBtn:JKButton!
    @IBOutlet private weak var searchConnectionBtn:JKButton!
    private var viewModel = JSClientNutureGiftSubscriptionViewModel.shared
    @IBOutlet private weak var coBoardAndFullPayStack:UIStackView!
    @IBOutlet private weak var connectionStack:UIStackView!
    var didSelectPaymentOptionBlock:((_ event:JSPaymentEventType)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pricePerGift = ""
        totalPrice = ""
        tax = ""
    }
    
    var pricePerGift:String = ""{
        didSet{
            pricePerGiftStack.value = pricePerGift
        }
    }
    var totalPrice:String = ""{
        didSet{
            totalStack.value = totalPrice
        }
    }
    var tax:String{
        set(newTax){
            taxStack.value = newTax
        }
        get{
            taxStack.value
        }
    }
    func reloadPaymentDetail(){
        pricePerGift = "$ \(viewModel.pricePerGift)"
        totalPrice = "$ \(viewModel.totalGiftPayAmount)"
        tax = viewModel.gitTax
        
    }
    @IBAction private func onClickPaymentOption(_ sender:JKButton){
        guard let option  = JSPaymentEventType(rawValue: sender.tag) else{return}
        self.didSelectPaymentOptionBlock?(option)
    }
    @IBAction private func onClickOnBoard(_ sender:JKButton){
        self.coBoardAndFullPayStack.isHidden = true
        self.connectionStack.isHidden = false
        self.setNeedsLayout()
    }
    @IBAction private func onClickOnBack(_ sender:JKButton){
        self.connectionStack.isHidden = true
        self.coBoardAndFullPayStack.isHidden = false
        self.setNeedsLayout()
        
    }
    
    
}
class JSPaymentValueStack: UIStackView {
    @IBOutlet private weak var titleLabel:UILabel!
    @IBOutlet private weak var valueLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var value:String = "00.00"{
        didSet{
            valueLabel.text = value
        }
    }
    
    
}


class JSConnectionContainerStack:UIStackView{
    
    @IBOutlet private weak var headerLabel:UILabel!
    @IBOutlet private weak var oneConnectionStack:JSConnectionStack!
    @IBOutlet private weak var twoConnectionStack:JSConnectionStack!
    @IBOutlet private weak var compensationBtnStack:UIStackView!
    @IBOutlet private weak var defaultCompensationBtn:JKButton!
    @IBOutlet private weak var customCompensationBtn:JKButton!
    private var viewModel = JSClientNutureGiftSubscriptionViewModel.shared
    var didSelectCompensationOption:((_ type:JSCompensationType)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        reloadData()
    }
    
    var isHiddenCompensationOption:Bool{
        set{
            compensationBtnStack.isHidden = newValue
        }
        get{
            return compensationBtnStack.isHidden
        }
    }
    func reloadData(_ isHidden:Bool = true){
        isHiddenTwoConnection = isHidden
        isHiddenCompensationOption = true
        oneConnection = nil
        twoConnection = nil
        didUpateHeader()
    }
    
    func didUpateHeader(){
        headerLabel.text = isHiddenTwoConnection == false ? "Two Connections" : "One Connection"
        oneConnectionStack.headerTitle = isHiddenTwoConnection == false ? "Connection 1" : "Choose Connection"
    }
    
    
    var isHiddenTwoConnection:Bool{
        get{
            return  self.twoConnectionStack.isHidden
        }
        set{
            self.twoConnectionStack.isHidden = newValue
        }
    }
    var oneConnection:JSUserConnection?{
        didSet{
            self.oneConnectionStack.connection = oneConnection?.connectionName
            viewModel.conn1Id  = oneConnection?.id
        }
    }
    var twoConnection:JSUserConnection?{
        didSet{
            self.twoConnectionStack.connection = twoConnection?.connectionName
            viewModel.conn2Id = twoConnection?.id
            
        }
    }
    
    @IBAction private func onDefaultCompensation(_ sender:Any){
        didSelectCompensationOption?(.default)
    }
    @IBAction private func onCustomCompensation(_ sender:Any){
        didSelectCompensationOption?(.custom)
    }
}
class JSConnectionStack:UIStackView{
    @IBOutlet private weak var headerLabel:UILabel!
    @IBOutlet private weak var connectionTF:JKTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    var headerTitle:String?{
        set{
            headerLabel.text =  newValue
        }
        get{
            return headerLabel.text
        }
    }
    var connection:String?{
        set{
            connectionTF.text = newValue
        }
        get{
            return connectionTF.text
        }
    }
    
}
class JSCompensationStack:UIStackView{
    
    @IBOutlet private weak var selfContributionStack:JSContributionStack!
    @IBOutlet private weak var oneConnectionContributionStack:JSContributionStack!
    @IBOutlet private weak var twoConnectionContributionnStack:JSContributionStack! // non editable
    override func awakeFromNib() {
        super.awakeFromNib()
        reloadData()
    }
    var selfCompensation:String?{
        set{
            selfContributionStack.compensation = newValue
        }
        get{
            return selfContributionStack.compensation
        }
    }
    var compensation1:String?{
        set{
            oneConnectionContributionStack.compensation = newValue
        }
        get{
            return oneConnectionContributionStack.compensation
        }
    }
    var otherCompensation:String?{
        set{
            twoConnectionContributionnStack.compensation = newValue
        }
        get{
            return twoConnectionContributionnStack.compensation
        }
    }
    func didChangeHeaderTitle(){
        twoConnectionContributionnStack.headerTitle =  isHiddenOneConnectionContribution == false ? "Connection 2 Contribution" : "Connection  Contribution"
    }
    
    var isHiddenOneConnectionContribution:Bool{
        set{
            self.oneConnectionContributionStack.isHidden = newValue
            if newValue == true {
                self.oneConnectionContributionStack.compensation = ""
            }
        }
        get{
            return self.oneConnectionContributionStack.isHidden
        }
    }
    func reloadData(){
        isHiddenOneConnectionContribution = true
        selfCompensation = ""
        compensation1 = ""
        otherCompensation = ""
        didChangeHeaderTitle()
    }
    
    
}
class JSContributionStack:UIStackView{
    
    @IBOutlet private weak var compensationTF:JKTextField!
    @IBOutlet private weak var headerLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var headerTitle:String?{
        set{
            headerLabel.text =  newValue
        }
        get{
            return headerLabel.text
        }
    }
    var compensation:String?{
        set{
            compensationTF.text = newValue
        }
        get{
            return compensationTF.text
        }
    }
    
} 
