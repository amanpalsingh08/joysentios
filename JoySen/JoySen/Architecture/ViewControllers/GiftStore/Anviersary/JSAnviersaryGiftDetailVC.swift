//
//  JSAnviersaryGiftDetailVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSAnviersaryGiftDetailVC: UIViewController {
    
    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var heightlbl: UILabel!
    @IBOutlet weak var widthlbl: UILabel!
    @IBOutlet weak var lengthlbl: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var clientTF: JKTextField!
    @IBOutlet weak var scheduleDateTF: JKTextField!
    
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var subPricelbl: UILabel!
    @IBOutlet weak var totalPricelbl: UILabel!
    var viewModel:JSAnniversaryGiftViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.resetAll()
    }
    
    fileprivate func loadData(){
        self.namelbl.text = viewModel.giftName
        self.heightlbl.text = viewModel.height
        self.widthlbl.text = viewModel.width
        self.lengthlbl.text = viewModel.length
        self.descriptionlbl.text = viewModel.giftDescription
        pricelbl.text  =  self.viewModel.giftPrice != nil ? "$\(self.viewModel.giftPrice!)" : ""
        subPricelbl.text =   self.viewModel.giftShippingPrice != nil ? "$\(self.viewModel.giftShippingPrice!)" : ""
        totalPricelbl.text  =  self.viewModel.giftTotalPrice != nil ? "$\(self.viewModel.giftTotalPrice!)" : ""
        if let file = viewModel.gitImageFile, !file.isEmpty{
            giftImageView.loadImage(filePath: file)
        }
    }
    
    @IBAction func onBuy(_ sender: JKButton) {
        viewModel.buyGift {
            
        }
    }
    
    @IBAction func onCancel(_ sender: JKButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kClientPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .client(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kGiftScheduleDatePickerSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .scheduleDateOfGift(nil), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .client(let client):
                self.viewModel.didSetClient(at: client)
                self.clientTF.text = self.viewModel.clientName
                self.viewModel.getGiftData {
                    async {
                        self.loadData()
                    }
                }
            case .scheduleDateOfGift(let date):
                self.viewModel.didSetScheduleDate(at: date)
                self.scheduleDateTF.text = date ?? ""
            default:
                break
            }
        }
        
    }
    
}
extension JSAnviersaryGiftDetailVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
