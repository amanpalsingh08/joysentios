//
//  JSAnviersaryGiftsVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSAnviersaryGiftsVC: UITableViewController {
   fileprivate var viewModel = JSAnniversaryGiftViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAnniversaryGifts()
    }
    private func getAnniversaryGifts(){
        viewModel.getGifts {
            async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.anniversaryGiftCount
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TBCellIdentity.kAnniversaryGiftCell, for: indexPath) as! JSAnniversaryGiftCell
        cell.rowIndex = indexPath.row
        cell.gift = viewModel[atAnniversaryGift: indexPath.row]

        return cell
    }
    

    
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kAnviersaryGiftDetailSegue, let purchaseBtn = sender as? JKButton {
            guard let controller = segue.destination as? JSAnviersaryGiftDetailVC else { return  }
            controller.viewModel = viewModel
            controller.viewModel.didSetAnniverary(at: purchaseBtn.tag)
        }
    }
    

}
