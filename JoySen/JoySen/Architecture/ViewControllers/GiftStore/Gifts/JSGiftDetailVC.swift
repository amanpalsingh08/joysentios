//
//  JSAnviersaryGiftDetailVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit


class JSClientInfoStack:UIStackView{
    @IBOutlet weak private var clientTF: JKTextField!
    @IBOutlet weak private var numberOfGiftTF: JKTextField!
    var clientName:String?{
        didSet{
            clientTF.text = clientName
        }
    }
    var numberOfGift:String?{
        didSet{
            numberOfGiftTF.text = numberOfGift
        }
    }
}

class JSCustomerInfoStack:UIStackView{
    
    @IBOutlet weak private var fNameTF: JKTextField!
    @IBOutlet weak private var lNameTF: JKTextField!
    @IBOutlet weak private var addressTF: JKTextField!
    @IBOutlet weak private var cityTF: JKTextField!
    @IBOutlet weak private var stateTF: JKTextField!
    @IBOutlet weak private var postCodeTF: JKTextField!
    @IBOutlet weak private var phoneTF: JKTextField!
    @IBOutlet weak private var emailTF: JKTextField!
    @IBOutlet weak private var checkBtn: UIButton!
    var isSelected : Bool {return checkBtn.isSelected}
    fileprivate var userViewModel:JSUserViewModel {return JSUserViewModel.shared}
    
    @IBAction private func onSameAsMyAddress(_ sender: UIButton) {
        checkBtn.isSelected = !checkBtn.isSelected
        customerData()
    }
    func isPhone(_ textField:UITextField)->Bool{
        return phoneTF == textField
    }
    var firstName:String{
        set{
            fNameTF.text = newValue
        }
        get{
            return  fNameTF.text ?? ""
        }
    }
    var lastName:String{
        set{
            lNameTF.text = newValue
        }
        get{
            return  lNameTF.text ?? ""
        }
    }
    var address:String{
        set{
            addressTF.text = newValue
        }
        get{
            return  addressTF.text ?? ""
        }
    }
    
    var city:String{
        set{
            cityTF.text = newValue
        }
        get{
            return  cityTF.text ?? ""
        }
    }
    
    var state:String{
        set{
            stateTF.text = newValue
        }
        get{
            return  stateTF.text ?? ""
        }
    }
    
    var zipCode:String{
        set{
            postCodeTF.text = newValue
        }
        get{
            return  postCodeTF.text ?? ""
        }
    }
    
    var phone:String{
        set{
            phoneTF.text = newValue
        }
        get{
            return  phoneTF.text ?? ""
        }
    }
    var email:String{
        set{
            emailTF.text = newValue
        }
        get{
            return  emailTF.text ?? ""
        }
    }
    
    //MARK:- customerData
    private func customerData(){
        firstName = isSelected == true ? userViewModel.firstName : ""
        lastName = isSelected == true ? userViewModel.lastName : ""
        address = isSelected == true ? userViewModel.address : ""
        city = isSelected == true ? userViewModel.city : ""
        state = isSelected == true ? userViewModel.state : ""
        zipCode = isSelected == true ? userViewModel.zipCode : ""
        phone = isSelected == true ? userViewModel.phone : ""
        email = isSelected == true ? userViewModel.email : ""
        
    }
    
    
}
class JSGiftDetailInfoStack: UIStackView {
    //Product Info
    @IBOutlet weak private var giftNamelbl: UILabel!
    @IBOutlet weak private var descriptionlbl: UILabel!
    @IBOutlet weak private var widthlbl: UILabel!
    @IBOutlet weak private var heightlbl: UILabel!
    @IBOutlet weak private var lengthlbl: UILabel!
    @IBOutlet weak private var pricelbl: UILabel!
    //For Client
    @IBOutlet weak private var clientInfoStack:JSClientInfoStack!
    //For Customer
    @IBOutlet weak fileprivate var customerInfoStack:JSCustomerInfoStack!
    @IBOutlet weak private var messageTV: JKTextView!
    
    var didTapPurchaseNow:(()->Void)?
    var viewModel:JSGiftDetailViewModel?{
        didSet{
            self.giftNamelbl.text = viewModel?.giftName
            self.descriptionlbl.text = viewModel?.giftDescription
            self.widthlbl.text = viewModel?.width
            self.heightlbl.text = viewModel?.height
            self.lengthlbl.text = viewModel?.length
            let giftPrice = viewModel?.giftPrice
            self.pricelbl.text = giftPrice != nil ? "$\(giftPrice!)" : ""
            self.numberOfGift = "\(viewModel?.giftQunatity ?? 1)"
            stackViewHidden()
            
        }
    }
    
    var clientName:String?{
        didSet{
            clientInfoStack.clientName = clientName
        }
    }
    var numberOfGift:String?{
        didSet{
            clientInfoStack.numberOfGift = numberOfGift
        }
    }
    var message:String?{
        return messageTV.text
    }
    
    func stackViewHidden(){
        if isHidden == true {
            
            if customerInfoStack != nil {
                self.customerInfoStack.isHidden = true
            }
            if clientInfoStack != nil {
                self.clientInfoStack.isHidden = true
            }
            
            
        }else{
            if isCustomer {
                if customerInfoStack != nil {
                    self.customerInfoStack.isHidden = false
                }
                if clientInfoStack != nil {
                    self.clientInfoStack.isHidden = true
                }
                
                
            }else{
                if customerInfoStack != nil {
                    self.customerInfoStack.isHidden = true
                }
                if clientInfoStack != nil {
                    self.clientInfoStack.isHidden = false
                }
                
            }
            
        }
    }
    
    
    @IBAction private func onPurchaseNow(_ sender: JKButton) {
        didTapPurchaseNow?()
    }
    
    
}
class JSGiftPaymentInfoStack: UIStackView {
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var quantitylbl: UILabel!
    @IBOutlet weak var taxlbl: UILabel!
    @IBOutlet weak var shippinglbl: UILabel!
    @IBOutlet weak var totalPricelbl: UILabel!
    var didOnTapBuy:((Bool)->Void)?
    var viewModel:JSGiftDetailViewModel?{
        didSet{
            let giftPrice = viewModel?.giftPrice
            self.pricelbl.text = giftPrice != nil ? "$\(giftPrice!)" : ""
            self.quantitylbl.text = "\(viewModel?.giftQunatity ?? 1)"
            let tax = viewModel?.giftTax
            self.taxlbl.text = tax != nil ? "$\(tax!)" : ""
            self.shippinglbl.text = viewModel?.height
            let giftTotalPrice = viewModel?.giftTotalPrice
            self.totalPricelbl.text = giftTotalPrice != nil ? "$\(giftTotalPrice!)" : ""
        }
    }
    
    
    @IBAction private func onBuy(_ sender: JKButton) {
        self.didOnTapBuy?(true)
    }
    
    @IBAction  private func onCancel(_ sender: JKButton) {
        self.didOnTapBuy?(false)
    }
}
class JSGiftDetailVC: UIViewController {
    
    @IBOutlet weak var payOtherCardBtn: JKButton!
    @IBOutlet weak var giftCarouselView: UICarouselView!
    @IBOutlet weak var giftInfoStack: JSGiftDetailInfoStack!
    @IBOutlet weak var giftPaymentInfoStack: JSGiftPaymentInfoStack!
    var viewModel = JSGiftDetailViewModel.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        giftInfoStack.didTapPurchaseNow = {
            self.view.endEditing(true)
            let handler  = {
                async{
                    
                    UIView.animate(withDuration: 0.4, delay: 0.0, options:.curveEaseIn, animations: {
                        self.giftInfoStack.isHidden  = true
                        self.giftPaymentInfoStack.isHidden  = false
                    }) { _ in
                        self.loadData()
                    }
                    
                }
            }
            
            if isCustomer {
                self.viewModel.getCustomerGiftShippingPrice(zipCode: self.giftInfoStack.customerInfoStack.zipCode,onCompletion: handler)
            }else{
                self.viewModel.getGiftShippingPrice(onCompletion: handler)
            }
            
        }
        giftPaymentInfoStack.didOnTapBuy = { isBuy in
            self.view.endEditing(true)
            if isBuy{
                if isCustomer {
                    let handler  = { (_ success:Bool)  in
                        async{
                            if success {
                                self.giftInfoStack.isHidden  = false
                                self.giftPaymentInfoStack.isHidden  = true
                                self.loadData()
                                self.navigationController?.popViewController(animated: true)
                                
                                
                            }else{
                                // user card detail not added yet
                                guard let navicontroller = JSRightMenu.cardDetails.navigationController else { return  }
                                self.slideMenuController()?.changeMainViewController(mainViewController: navicontroller, close: true)
                            }
                            
                        }
                    }
                    self.viewModel.buyCustomerGift(firstname: self.giftInfoStack.customerInfoStack.firstName, lastname: self.giftInfoStack.customerInfoStack.lastName, email: self.giftInfoStack.customerInfoStack.email, phone: self.giftInfoStack.customerInfoStack.phone, address: self.giftInfoStack.customerInfoStack.address, state: self.giftInfoStack.customerInfoStack.state, city: self.giftInfoStack.customerInfoStack.city, zipcode: self.giftInfoStack.customerInfoStack.zipCode, message: self.giftInfoStack.message,onCompletion: handler)
                }else{
                    let handler  = {
                        async{
                            self.giftInfoStack.isHidden  = false
                            self.giftPaymentInfoStack.isHidden  = true
                            self.loadData()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    self.viewModel.buyGift(message: self.giftInfoStack.message,onCompletion: handler)
                }
                
            }else{
                async{
                    
                    UIView.animate(withDuration: 0.4) {
                        self.giftInfoStack.isHidden  = false
                        self.giftPaymentInfoStack.isHidden  = true
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGiftDetail()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        giftCarouselView.stop()
        viewModel.resetAll()
    }
    private func getGiftDetail(){
        viewModel.getGiftData {
            async {
                self.loadData()
            }
        }
    }
    fileprivate func loadData(){
        self.giftCarouselView.setImages(urls:viewModel.gitImages)
        self.giftPaymentInfoStack.viewModel = viewModel
        self.giftInfoStack.viewModel = viewModel
        //self.payOtherCardBtn.isHidden = isCustomer  == false
        
        
        
    }
    
    //MARK:- onShare
    @IBAction private func onShare(_ sender: UIBarButtonItem) {
        
        let objectsToShare = [URL(string: "https://itunes.apple.com/us/app/joysent/id1489100919?ls=1&mt=8")!]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentity.kClientPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .client(nil), sender: sender)
        }else if segue.identifier == SegueIdentity.kGiftNumberPickerSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .giftQuantity(1), sender: sender)
        }else if segue.identifier == SegueIdentity.kCardPaymentSegue{
            guard segue.destination is JSCustomerCardPaymentVC else{return}
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .client(let client):
                self.viewModel.didSetClient(at: client)
                self.giftInfoStack.clientName = self.viewModel.clientName
                self.loadData()
            case .giftQuantity(let quantity):
                self.viewModel.didSetQuantity(at:quantity)
                self.giftInfoStack.numberOfGift = "\(self.viewModel.giftQunatity ?? 1)"
                async {
                    self.loadData()
                }
                
                
            default:
                break
            }
        }
        
    }
    
}
extension JSGiftDetailVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
extension JSGiftDetailVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if giftInfoStack.customerInfoStack.isPhone(textField)   {
            var fullString = textField.text ?? ""
            fullString.append(string)
            if fullString.count>textField.maxLength {
                return false
            }
            let shouldRemoveLastDigit = range.length == 1 ? true : false
            textField.text = fullString.formattedNumber(shouldRemoveLastDigit: shouldRemoveLastDigit)
            
            return false
        }else{
            return true
        }
        
    }
}
