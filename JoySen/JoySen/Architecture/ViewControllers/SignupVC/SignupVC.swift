//
//  SignupVC.swift
//  ZGuideZ
//
//  Created by Mandeep Kaur on 20/11/18.
//  Copyright © 2018 Mandeep Kaur. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak fileprivate var emailTF: JKTextField!
    @IBOutlet weak fileprivate var passwordTF: JKTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: JKTextField!
    @IBOutlet weak fileprivate var firstNameTF: JKTextField!
    @IBOutlet weak fileprivate var lastNameTF: JKTextField!
    @IBOutlet weak fileprivate var objUserVM:JSUserViewModel!
    @IBOutlet weak fileprivate var userRoleTF: JKTextField!
    fileprivate var userType:JSUserRole?{
        didSet{
              self.userRoleTF.text = userType?.title
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTF.right { (accesoryBtn) in
            self.passwordTF.isSecureTextEntry = accesoryBtn.isSelected == true ? false : true
        }
        confirmPasswordTF.right { (accesoryBtn) in
            self.confirmPasswordTF.isSecureTextEntry = accesoryBtn.isSelected == true ? false : true
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnClick(_ sender: Any) {
        self.view.endEditing(true)
        _ =  self.navigationController?.popViewController(animated:false)
    }
    

    
    @IBAction fileprivate func onSignUp(_ sender: Any) {
        self.view.endEditing(true)
        objUserVM.register(firstNameTF: firstNameTF, lastNameTF: lastNameTF, emailTF: emailTF, passwordTF: passwordTF, confirmPasswordTF: confirmPasswordTF,userType:userType , onSuccess: {
            async {
                self.navigationController?.popToRootViewController(animated: true)
               // AppDelegate.shared.showMainController()
                
            }
        })
    }
 

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)
        if segue.identifier == SegueIdentity.kUserRoleSegue{
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .userRole(nil), sender: sender)
        }
    }
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
        
        controller.pickerType = pickerType
        if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
            popoverController.sourceView = sd
            popoverController.sourceRect = sd.bounds
            popoverController.delegate = self
            controller.preferredContentSize.width = sd.bounds.width
            
        }
        controller.didSelectPickerItem = {(item) in
            switch item {
            case .userRole(let role):
                self.userType = role
              
            default:
                break
            }
        }
        
    }

}
extension SignupVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
