//
//  JSHomeVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/08/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSHomeVC: UITableViewController {
    enum Sections :Int{
        case queueClient
        case lastDelivered
        var title:String{
            switch self {
            case .queueClient:
                return "Queue Clients"
            default:
                return "Last Delivered"
            }
        }
    }
    fileprivate var viewModel = JSDashboardViewModel.shared
    fileprivate var sections:[Sections] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getHomeData()
    }
    //MARK:- getQueueClients
    
    fileprivate func getHomeData(){
        viewModel.removeAll()
        self.tableView.reloadData()
        viewModel.getHomeData {
            async {
                self.sections = [.queueClient,.lastDelivered]
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sect = sections[section]
        switch sect {
        case .queueClient:
            return viewModel.queueClientCount
        default:
            return viewModel.deliveredClientCount
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.text? = headerView.textLabel?.text?.capitalized ?? ""
            headerView.textLabel?.textColor = .systemGray
            headerView.contentView.backgroundColor = UIColor.white
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section {
        case .queueClient:
            let cell = tableView.dequeue(JSQueueClientCell.self, for: indexPath)
            cell.queueItem = viewModel[atQueueClient: indexPath.row]
            
            return cell
        default:
            
            let cell =  tableView.dequeue(JSClientLastDeliveredCell.self, for: indexPath)
            cell.model = viewModel[atDeliveredClient: indexPath.row]
            return cell
        }
        
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.sections[indexPath.section] == .lastDelivered ?  300: 256
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
