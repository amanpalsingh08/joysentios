//
//  JSCreditCardVC.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSCreditCardVC: UIViewController {
    @IBOutlet fileprivate weak var cardNumTF:JKTextField!
    @IBOutlet fileprivate weak var cardExpMonthTF:JKTextField!
    @IBOutlet fileprivate weak var cardExpYearTF:JKTextField!
    @IBOutlet fileprivate weak var cardCVCTF:JKTextField!
    @IBOutlet fileprivate weak var submitBtn:JKButton!
    fileprivate var viewModel = JSCardViewModel.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        getCard()
    }
    
    fileprivate func getCard(){
        viewModel.getCard {
            async {
                self.loadCardData()
            }
        }
    }
    fileprivate func loadCardData(){
        cardNumTF.leftIcon = #imageLiteral(resourceName: "ic_crdit_card")
        cardNumTF.text = viewModel.cardNumber ?? ""
        cardExpYearTF.text = viewModel.expYear ?? ""
        cardExpMonthTF.text = viewModel.expMonth ?? ""
    }
    //MARK:- onSubmit
    @IBAction fileprivate func onSubmit(_ sender:JKButton){
        self.view.endEditing(true)
        guard let cardnumber = cardNumTF.text, let expiryMonth = cardExpMonthTF.text, let expiryYear = cardExpYearTF.text, let cvc = cardCVCTF.text else { return}
        viewModel.addEditCard(cardNumber: cardnumber, expiryMonth: expiryMonth, expiryYear: expiryYear, cvc: cvc) {
            async {
                self.getCard()
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SegueIdentity.kCardMonthsPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .cardExpMonth(nil), sender: sender)
        }else  if segue.identifier == SegueIdentity.kCardYearsPickerSegue {
            guard let controller = segue.destination as? JSPickerVC else{return}
            self.showPicker(controller, pickerType: .cardExpYear(nil), sender: sender)
        }
    }
    
    private func showPicker(_ controller:JSPickerVC, pickerType:JSPickerType,sender:Any?){
           
           controller.pickerType = pickerType
           if let popoverController = controller.popoverPresentationController,let sd = sender as? UIButton {
               popoverController.sourceView = sd
               popoverController.sourceRect = sd.bounds
               popoverController.delegate = self
               controller.preferredContentSize.width = sd.bounds.width/2
               
           }
           controller.didSelectPickerItem = {(item) in
               switch item {
               case .cardExpMonth( let month):
                self.cardExpMonthTF.text = month
               case .cardExpYear(let year):
                self.cardExpYearTF.text = year
                   
               default:
                   break
               }
           }
           
       }
}

extension JSCreditCardVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
        
    }
    
}
extension JSCreditCardVC:UITextFieldDelegate{

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
        if currentText.count>textField.maxLength {
            return false
        }
        if textField == cardNumTF  {
            let (type, formatted, _) = currentText.checkCardNumber()
            cardNumTF.text = formatted//grouping(every: 4, with: " ")
            cardNumTF.leftIcon = type.icon
            return false
        }
         return false
    }
}
