//
//  JSRightMenuCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 23/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSRightMenuCell: UITableViewCell {
    @IBOutlet fileprivate var iconView:JKImageView!
    @IBOutlet fileprivate var titlelbl:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var item:JSRightMenu?{
        didSet{
            iconView.image = item?.icon
            titlelbl.text = item?.title ?? ""
        }
    }

}
