//
//  JSClientConnectionCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 03/12/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSClientConnectionCell: UITableViewCell {
    @IBOutlet fileprivate weak var clientNamelbl:UILabel!
    @IBOutlet fileprivate weak var clientEmaillbl:UILabel!
    @IBOutlet fileprivate weak var connectionNamelbl:UILabel!
    @IBOutlet fileprivate weak var connectionRolelbl:UILabel!
    @IBOutlet fileprivate weak var requestBtn:JKButton!
    var refreshData:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        requestBtn.addTarget(self, action: #selector(self.onClickRequest(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var rowIndex:Int = 0{
        didSet{
            requestBtn.tag = rowIndex
        }
    }
    var item:JSClientConnection.JSConnection?{
        didSet{
            clientNamelbl.text  = item?.clientName ?? ""
            clientEmaillbl.text = item?.clientEmail ?? ""
            connectionNamelbl.text = item?.connectionName ?? ""
            connectionRolelbl.text = item?.connectionRole ?? ""
            requestBtn.normalTitle = item?.requestType.title ?? ""
            if let status = item?.requestType {
                requestBtn.backgroundColor  = status.color
                requestBtn.isUserInteractionEnabled = status == .request ? true : false
                
            }
            
        }
    }
    @objc
    fileprivate func onClickRequest(_ sender:JKButton){
        guard let type = self.item?.requestType, let clientID = self.item?.clientID , let connectionID = self.item?.connectionID else { return }
        if type == .request {
            currentController?.showAlertAction(message: "Do you want to send the request?",otherTitle: "Send", onCompletion: { (index) in
                if index == 2{
                    JSRequestsViewModel.shared.sendRequest(receiverId: connectionID, clientId: clientID, onCompletion: {
                        async {
                            self.refreshData?()
                        }
                    })
                }
            })
        }
    }
    
}

class JSSearchConnectionCell: UITableViewCell {
    @IBOutlet fileprivate weak var namelbl:UILabel!
    @IBOutlet fileprivate weak var emaillbl:UILabel!
    @IBOutlet fileprivate weak var clientCountlbl:UILabel!
    @IBOutlet fileprivate weak var rolelbl:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    var searchConnection:JSClientConnection.JSSearchConnection?{
        didSet{
            namelbl.text = searchConnection?.name ?? ""
            emaillbl.text = searchConnection?.email ?? ""
            clientCountlbl.text = "\(searchConnection?.count ?? 0)"
            rolelbl.text = searchConnection?.role?.title ?? ""
        }
    }
}
