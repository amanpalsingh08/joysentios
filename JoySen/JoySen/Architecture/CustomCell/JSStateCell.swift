//
//  JSStateCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 24/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSStateCell: UITableViewCell {
    @IBOutlet fileprivate var titlelbl:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var state:JSState?{
        didSet{
            titlelbl.text = state?.name ?? ""
        }
    }
    var stage:JSStage?{
        didSet{
            titlelbl.text = stage?.name ?? ""
        }
    }
    var leadSourece:JSLeadSource?{
        didSet{
            titlelbl.text = leadSourece?.name ?? ""
        }
    }
    var frequency:JSFrequency?{
        didSet{
            titlelbl.text = frequency?.title ?? ""
        }
    }
    var group:String?{
        didSet{
            titlelbl.text = group ?? ""
        }
    }
    var month:String?{
        didSet{
            titlelbl.text = month ?? ""
        }
    }
    var year:String?{
           didSet{
               titlelbl.text = year ?? ""
           }
       }
    var phoneType:JSPhoneType?{
        didSet{
            titlelbl.text = phoneType?.title ?? ""
        }
    }
    var status:JSStatus?{
        didSet{
            titlelbl.text = status?.title ?? ""
        }
    }
    var connection:JSUserConnection?{
        didSet{
            titlelbl.text = connection?.connectionName ?? ""
        }
    }
    var client:JSMyClient?{
        didSet{
            titlelbl.text = "\(client?.firstname?.capitalized ?? "") \(client?.lastname?.capitalized ?? "")"
        }
    }
    var numOfGift:Int?{
        didSet{
            
            titlelbl.text =  numOfGift == nil ? "" : "\(numOfGift!)"
        }
    }
    var contribute:JSContribution?{
        didSet{
            
            titlelbl.text =  contribute?.title ?? ""
        }
    }
    var giftScheduleDate:String?{
        didSet{
            titlelbl.text = giftScheduleDate ?? ""
        }
    }
    var role:JSUserRole?{
        didSet{
            titlelbl.text = role?.title
        }
    }
}
