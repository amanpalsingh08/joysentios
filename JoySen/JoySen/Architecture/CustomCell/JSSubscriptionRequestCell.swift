//
//  JSSubscriptionRequestCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/08/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSSubscriptionRequestCell: UITableViewCell {
    @IBOutlet fileprivate weak var namelbl:UILabel!
    @IBOutlet fileprivate weak var emaillbl:UILabel!
    @IBOutlet fileprivate weak var connection1lbl:UILabel!
    @IBOutlet fileprivate weak var connection2lbl:UILabel!
    @IBOutlet fileprivate weak var requestStatusBtn:JKButton!
    @IBOutlet fileprivate weak var conn1StatusBtn:JKButton!
    @IBOutlet fileprivate weak var conn2StatusBtn:JKButton!
    @IBOutlet fileprivate weak var typelbl:UILabel!
    fileprivate var viewModel = JSRequestsViewModel.shared
    
    var refreshData:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        requestStatusBtn.addTarget(self, action: #selector(self.onClickRequest(_:)), for: .touchUpInside)
        
    }
    var userID:String?{
        guard let id = JSUserViewModel.shared.userId else { return nil }
        return String(id)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var rowIndex:Int = 0{
        didSet{
            conn1StatusBtn.tag = rowIndex
            conn2StatusBtn.tag = rowIndex
            requestStatusBtn.tag = rowIndex
        }
    }
    var subscribeItem:JSSubscriptionRequest?{
        didSet{
            typelbl.text = subscribeItem?.type ?? ""
            namelbl.text = subscribeItem?.clientName ?? ""
            emaillbl.text = subscribeItem?.clientEmail ?? ""
            connection1lbl.text = subscribeItem?.conn1 ?? ""
            connection2lbl.text = subscribeItem?.conn2 ?? ""
            requestStatusBtn.normalTitle = subscribeItem?.requestStatusType?.title ?? ""
            conn1StatusBtn.normalTitle = subscribeItem?.conn1StatusType?.title ?? ""
            conn2StatusBtn.normalTitle = subscribeItem?.conn2StatusType?.title ?? ""
            if let type = subscribeItem?.conn1StatusType{
                if let connId = subscribeItem?.conn1ID, let userID  = userID, userID == "\(connId)" , type == .aggreed{
                    conn1StatusBtn.isEnabled = true
                }else{
                    conn1StatusBtn.isEnabled = false
                }
                
                conn1StatusBtn.backgroundColor = type.color
            }
            if let type = subscribeItem?.conn2StatusType{
                conn2StatusBtn.isEnabled = type == .aggreed ? true : false
                conn2StatusBtn.backgroundColor = type.color
            }
            if let type = subscribeItem?.requestStatusType{
                requestStatusBtn.isEnabled = type == .accept ? true : false
                requestStatusBtn.backgroundColor = type.color
            }
            
        }
    }
    @objc
    fileprivate func onClickRequest(_ sender:JKButton){
        guard let type = self.subscribeItem?.requestStatusType else { return }
        if type == .accept {
            currentController?.showAlertAction(message: "Are you sure do you want to accept of \(self.namelbl.text ?? "") subscription request", onCompletion: { (index) in
                if index == 2{
                    self.viewModel.acceptRequest(at: self.rowIndex) {
                        self.refreshData?()
                    }
                }
            })
        }
    }
    
    
}

