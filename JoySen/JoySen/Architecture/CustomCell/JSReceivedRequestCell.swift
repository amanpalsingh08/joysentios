//
//  JSReceivedRequestCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 30/11/19.
//  Copyright © 2019 joy. All rights reserved.
//

import UIKit

class JSReceivedRequestCell: UITableViewCell {
    @IBOutlet fileprivate weak var namelbl:UILabel!
    @IBOutlet fileprivate weak var emaillbl:UILabel!
    @IBOutlet fileprivate weak var connection1lbl:UILabel!
    @IBOutlet fileprivate weak var connection2lbl:UILabel!
    @IBOutlet fileprivate weak var requestStatusBtn:JKButton!
    @IBOutlet fileprivate weak var conn1StatusBtn:JKButton!
    @IBOutlet fileprivate weak var conn2StatusBtn:JKButton!
    fileprivate var viewModel = JSRequestsViewModel.shared
    
    var refreshData:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        requestStatusBtn.addTarget(self, action: #selector(self.onClickRequest(_:)), for: .touchUpInside)
        //conn1StatusBtn.addTarget(self, action: #selector(self.onClickConn1Status(_:)), for: .touchUpInside)
        // conn2StatusBtn.addTarget(self, action: #selector(self.onClickConn2Status(_:)), for: .touchUpInside)
    }
    var userID:String?{
        guard let id = JSUserViewModel.shared.userId else { return nil }
        return String(id)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var rowIndex:Int = 0{
        didSet{
            conn1StatusBtn.tag = rowIndex
            conn2StatusBtn.tag = rowIndex
            requestStatusBtn.tag = rowIndex
        }
    }
    var receivedItem:JSRequestsReceived?{
        didSet{
            namelbl.text = receivedItem?.clientName ?? ""
            emaillbl.text = receivedItem?.clientEmail ?? ""
            connection1lbl.text = receivedItem?.conn1 ?? ""
            connection2lbl.text = receivedItem?.conn2 ?? ""
            requestStatusBtn.normalTitle = receivedItem?.requestStatusType?.title ?? ""
            conn1StatusBtn.normalTitle = receivedItem?.conn1StatusType?.title ?? ""
            conn2StatusBtn.normalTitle = receivedItem?.conn2StatusType?.title ?? ""
            if let type = receivedItem?.conn1StatusType{
                if let connId = receivedItem?.conn1ID, let userID  = userID, userID == connId , type == .aggreed{
                    conn1StatusBtn.isEnabled = true
                }else{
                    conn1StatusBtn.isEnabled = false
                }
                
                conn1StatusBtn.backgroundColor = type.color
            }
            if let type = receivedItem?.conn2StatusType{
                conn2StatusBtn.isEnabled = type == .aggreed ? true : false
                conn2StatusBtn.backgroundColor = type.color
            }
            if let type = receivedItem?.requestStatusType{
                requestStatusBtn.isEnabled = type == .accept ? true : false
                requestStatusBtn.backgroundColor = type.color
            }
            
        }
    }
    @objc
    fileprivate func onClickRequest(_ sender:JKButton){
        guard let type = self.receivedItem?.requestStatusType else { return }
        if type == .accept {
            currentController?.showAlertAction(message: "Are you sure do you want to accept of \(self.namelbl.text ?? "") request", onCompletion: { (index) in
                if index == 2{
                    self.viewModel.acceptRequest(at: self.rowIndex) {
                        self.refreshData?()
                    }
                }
            })
        }
    }
    @objc
    fileprivate func onClickConn1Status(_ sender:JKButton){
        
        
    }
    @objc
    fileprivate func onClickConn2Status(_ sender:JKButton){
        guard let type = self.receivedItem?.conn2StatusType else { return }
        if type == .aggreed {
            currentController?.showAlertAction(message: "Do you really want to Cancel this request", onCompletion: { (index) in
                if index == 2{
                    self.viewModel.cancelRequest(at: self.rowIndex) {
                        self.refreshData?()
                    }
                }
            })
        }
    }
    
}

