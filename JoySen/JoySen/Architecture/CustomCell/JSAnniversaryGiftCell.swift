//
//  JSAnniversaryGiftCell.swift
//  JoySen
//
//  Created by Jitendra Kumar on 29/02/20.
//  Copyright © 2020 joy. All rights reserved.
//

import UIKit

class JSAnniversaryGiftCell: UITableViewCell {
    @IBOutlet private var giftImageView:UIImageView!
    @IBOutlet private var titlelbl:UILabel!
    @IBOutlet private var detaillbl:UILabel!
    @IBOutlet private var pricelbl:UILabel!
    @IBOutlet private var purchaseBtn:JKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var rowIndex:Int = 0 {
        didSet{
            self.purchaseBtn.tag = rowIndex
        }
    }
    var gift:JSAnniversaryGift?{
        didSet{
            titlelbl.text = gift?.giftName ?? ""
            detaillbl.text = gift?.description ?? ""
             let price = gift?.price ?? ""
             pricelbl.text = price.isEmpty == false ? "$\(price)" : ""
            if let file = gift?.img, !file.isEmpty {
                giftImageView.loadImage(filePath: file)
            }
        }
    }

}
